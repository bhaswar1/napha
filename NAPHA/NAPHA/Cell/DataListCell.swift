//
//  DataListCell.swift
//  NAPHA
//
//  Created by admin on 16/02/18.
//  Copyright © 2018 Esolz. All rights reserved.
//

import UIKit

class DataListCell: UITableViewCell {
    
    // Show List Cell
    
    @IBOutlet weak var showName: UILabel!
    @IBOutlet weak var divisionName: UILabel!
    @IBOutlet weak var judgeName: UILabel!
    @IBOutlet weak var judgeDropDown: UIImageView!
    
    @IBOutlet weak var classView: UIView!
    @IBOutlet weak var classField: UITextField!
    @IBOutlet weak var classSep: UIView!
    
    
    @IBOutlet weak var classNameView: UIView!
    @IBOutlet weak var classNameField: UITextField!
    @IBOutlet weak var classNameSep: UIView!
    
    
    @IBOutlet weak var divisionView: UIView!
    @IBOutlet weak var divisionField: UITextField!
    @IBOutlet weak var divisionSep: UIView!
    @IBOutlet weak var divisionBtn: UIButton!
    
    
    @IBOutlet weak var judgeView: UIView!
    @IBOutlet weak var judgeField: UITextField!
    @IBOutlet weak var judgeSep: UIView!
    @IBOutlet weak var judgeBtn: UIButton!
    
    
    @IBOutlet weak var saveView: UIView!
    @IBOutlet weak var saveBtn: UIButton!
    
    
    
    // Horse Entry
    @IBOutlet weak var txtHorseEntry: UITextField!
    @IBOutlet weak var txtHorseName: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    
    //Show listing - File
    @IBOutlet weak var lblShowName: UILabel!
    @IBOutlet weak var lblJudge1: UILabel!
    @IBOutlet weak var lblJudge2: UILabel!
    @IBOutlet weak var btnClassList: UIButton!
    @IBOutlet weak var btnEcard: UIButton!
    @IBOutlet weak var showDeleteBtn: UIButton!
    @IBOutlet weak var showCopyBtn: UIButton!
    
    // Category by division - preferences
    @IBOutlet weak var lblDivisionNo: UILabel!
    @IBOutlet weak var txtDivision: UITextField!
    @IBOutlet weak var txtCategory: UITextField!
    @IBOutlet weak var txtWeight: UITextField!
    @IBOutlet weak var txtSpread: UITextField!
    @IBOutlet weak var numberView: UIView!
    @IBOutlet weak var preferenceDivisionView: UIView!
    @IBOutlet weak var categoryView: UIView!
    
    
    // Judge's Card
    
    @IBOutlet weak var serialNumber: UITextField!
    @IBOutlet weak var entryView: UIView!
    @IBOutlet weak var entryField: UITextField!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var functionView: UIView!
    @IBOutlet weak var functionField: UITextField!
    @IBOutlet weak var smoothView: UIView!
    @IBOutlet weak var smoothField: UITextField!
    @IBOutlet weak var threadView: UIView!
    @IBOutlet weak var threadField: UITextField!
    @IBOutlet weak var terminoView: UIView!
    @IBOutlet weak var terminoField: UITextField!
    @IBOutlet weak var calculateView: UIView!
    @IBOutlet weak var calculateField: UITextField!
    @IBOutlet weak var judgePlaceView: UIView!
    @IBOutlet weak var jusgePlaceField: UITextField!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
