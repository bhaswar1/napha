//
//  NPShowSetupViewController.swift
//  NAPHA
//
//  Created by Priyanka on 15/02/18.
//  Copyright © 2018 Esolz. All rights reserved.
//

import UIKit

class NPShowSetupViewController: GlobalViewController, UITextFieldDelegate {
    
    @IBOutlet weak var mainscroll: UIScrollView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var txtShowName: UITextField!
    @IBOutlet weak var txtJudge1: UITextField!
    
    @IBOutlet weak var txtJudge2: UITextField!
    
    @IBOutlet weak var Switch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        SetDesign()
    }
    // MARK: - set design
    func SetDesign()
    {
        btnNext.backgroundColor = GlobalViewController.ThemeColor
        btnNext.layer.cornerRadius = SetButtonRadius(size: 24)
        btnNext.layer.shadowOffset = CGSize(width:0,height:5)
        // set the radius
        btnNext.layer.shadowRadius = 10
        // change the color of the shadow (has to be CGColor)
        btnNext.layer.shadowColor = GlobalViewController.ThemeColor.cgColor
        // display the shadow
        btnNext.layer.shadowOpacity = 0.8
        btnNext.layer.shadowPath = UIBezierPath(roundedRect:CGRect(x: 20, y: 5, width: btnNext.frame.size.width - 40, height:btnNext.frame.size.height), cornerRadius: 12.0).cgPath
        btnNext.setTitleColor(UIColor.white, for: .normal)
        btnNext.titleLabel!.font = UIFont(name:GlobalViewController.RobotoBold, size: 18)
        
        txtShowName.layer.cornerRadius = 4.0
        txtShowName.layer.masksToBounds = true
        txtShowName.layer.borderColor = UIColor.black.cgColor
        txtShowName.layer.borderWidth = 0.6
        
        txtJudge1.layer.cornerRadius = 4.0
        txtJudge1.layer.masksToBounds = true
        txtJudge1.layer.borderColor = UIColor.black.cgColor
        txtJudge1.layer.borderWidth = 0.6
        
        txtJudge2.layer.cornerRadius = 4.0
        txtJudge2.layer.masksToBounds = true
        txtJudge2.layer.borderColor = UIColor.black.cgColor
        txtJudge2.layer.borderWidth = 0.6
        
        txtPadding(textfield: txtShowName)
        txtPadding(textfield: txtJudge1)
        txtPadding(textfield: txtJudge2)
        
                  
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - next button tapped
    @IBAction func NextButtonTapped(_ sender: Any) {
        
        txtShowName.placeholder = nil
        txtJudge1.placeholder = nil
        txtJudge2.placeholder = nil
        
        txtShowName.resignFirstResponder()
        txtJudge1.resignFirstResponder()
        txtJudge2.resignFirstResponder()
        
        mainscroll.contentOffset = CGPoint(x:0, y:0);
        
        if (BlankOrNot(txtShowName.text!).characters.count==0)
        {
            
            txtShowName.attributedPlaceholder = NSAttributedString(string: "Enter Show Name", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
//            let alert = UIAlertController(title: "Alert", message:"Enter Show Name", preferredStyle: UIAlertControllerStyle.alert)
//            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//            OperationQueue.main.addOperation
//                {
//
//                    self.present(alert, animated: true, completion: nil)
//            }
        }
        else if (BlankOrNot(txtJudge1.text!).characters.count==0)
        {
            
            txtJudge1.attributedPlaceholder = NSAttributedString(string: "Enter Judge Name #1", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
//            let alert = UIAlertController(title: "Alert", message:"Enter Judge Name", preferredStyle: UIAlertControllerStyle.alert)
//            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//            OperationQueue.main.addOperation
//                {
//
//                    self.present(alert, animated: true, completion: nil)
//            }
        }
        else
        {
            ShowSetupAPI()
        }
    }
    // MARK: - Show Setup Api Execute
    func ShowSetupAPI()
    {


        DispatchQueue.main.async {
            self.CheckLoader()
        }
        var defaultJudge1 : NSInteger = 0
        if Switch.isOn
        {
            defaultJudge1 = 1
        }
        else
        {
            defaultJudge1 = 0
        }

        let  url_to_request = GlobalViewController.GLOBALAPI + "show/createshow"

        print("Full URL:",url_to_request)

        let urlString:NSURL = NSURL(string: url_to_request)!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: urlString as URL)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData

        let parameter = "name=\(txtShowName.text!)&judgeone=\(txtJudge1.text!)&judgetwo=\(txtJudge2.text!)&default=\(defaultJudge1)"

        print("parameter:",parameter)
        request.httpMethod = "POST"
        request.httpBody = parameter.data(using: String.Encoding.utf8)


        let task = session.dataTask(with: request as URLRequest, completionHandler: {


            (data, response, error) in


            guard let _:Data = data, let _:URLResponse = response, error == nil

                else
            {
                print("error")
                let alert = UIAlertController(title: "Error", message:error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                OperationQueue.main.addOperation
                    {
                        self.CheckLoader()
                        self.present(alert, animated: true, completion: nil)
                }
                return
            }
            //    self.statusCode = [(NSHTTPURLResponse*)response statusCode];
            if let httpResponse = response as? HTTPURLResponse
            {


                print("statusCode \(httpResponse.statusCode)")
                if (httpResponse.statusCode == 200)
                {



                    let JSONData = data
                    do {
                        let JSON = try JSONSerialization.jsonObject(with: JSONData!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                        guard let JSONDictionary: NSDictionary = JSON as? NSDictionary else {
                            print("Not a Dictionary")
                            // put in function

                            print("JSONDictionary \(JSON)")
                            DispatchQueue.main.async {
                                self.CheckLoader()
                            }
                            return
                        }

                        print("JSONDictionary \(JSONDictionary)")


                        let status : Bool = JSONDictionary.object(forKey: "status") as! Bool

                        if (status == true)
                        {

                            DispatchQueue.main.async {
                                self.CheckLoader()


                               let details:NSDictionary = JSONDictionary.object(forKey: "showdetails") as! NSDictionary



                                let ClassListPageView = self.storyboard?.instantiateViewController(withIdentifier: "NPClassListViewController") as! NPClassListViewController
                                ClassListPageView.ShowId = String(details.object(forKey: "id") as! NSInteger)
                                ClassListPageView.Judge1 = details.object(forKey: "judgeonename") as! String
                                ClassListPageView.Judge2 = details.object(forKey: "judgetwoname") as! String
                                ClassListPageView.ShowName = details.object(forKey: "showname") as! String
                                ClassListPageView.Judge1Id = String(details.object(forKey: "judgeoneid") as! NSInteger)
                                ClassListPageView.Judge2Id = String(details.object(forKey: "judgetwoid") as! NSInteger)
                                ClassListPageView.J1DefaultJudge = NSInteger(details.object(forKey: "default_judge") as! String)!
                               
                                
                                var tempDict1 = NSMutableDictionary()
                                var tempDict2 = NSMutableDictionary()
                                let judgeList = NSMutableArray()
                                tempDict1.setValue(String(details.object(forKey: "judgeoneid") as! NSInteger), forKey: "judgeid")
                                tempDict1.setValue(details.object(forKey: "judgeonename") as! String, forKey: "judgename")
                                judgeList.add(tempDict1)
                               
                               
                if (self.BlankOrNot(self.txtJudge2.text!).characters.count > 0)
                 {
                        tempDict2.setValue(String(details.object(forKey: "judgetwoid") as! NSInteger), forKey: "judgeid")
                        tempDict2.setValue(details.object(forKey: "judgetwoname") as! String, forKey: "judgename")
                        judgeList.add(tempDict2)
                        
                }
                        
                            UserDefaults.standard.set(details.object(forKey: "showname") as! String, forKey: "ShowName")
                            UserDefaults.standard.set(String(details.object(forKey: "id") as! NSInteger), forKey: "ShowID")
                                
                               
                                
                                ClassListPageView.judgeList = judgeList.mutableCopy() as! NSMutableArray
                                
                                
                                UserDefaults.standard.set(NSInteger(details.object(forKey: "default_judge") as! String)!, forKey: "defaultJudge")
                                UserDefaults.standard.set(judgeList.mutableCopy() as! NSArray, forKey: "JudgeArray")
                                self.navigationController?.pushViewController(ClassListPageView, animated: false)


                            }


                        }
                        else
                        {
                            let alert = UIAlertController(title: "Error", message: JSONDictionary.value(forKey: "message") as? String, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))

                            DispatchQueue.main.async {
                                self.present(alert, animated: true, completion: nil)
                                self.CheckLoader()


                            }
                        }


                    }
                    catch let JSONError as NSError {
                        print("\(JSONError)")
                        let alert = UIAlertController(title: "Error", message: JSONError.localizedDescription as String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        OperationQueue.main.addOperation
                            {
                                self.CheckLoader()
                                self.present(alert, animated: true, completion: nil)
                        }
                    }


                }
                else if(httpResponse.statusCode == 400)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else if(httpResponse.statusCode == 500)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        self.CheckLoader()
                    }
                }


            }


        })


        task.resume()


    }
    // MARK: - judge1 switched
    @IBAction func Judge1Swiched(_ sender: Any) {
    }
    // MARK: - TextField Delegates
    public func textFieldDidBeginEditing(_ textField: UITextField)
    {
        textField.placeholder = nil
        
        if GlobalViewController.IsipadAir
        {
            if textField == txtShowName
            {
                mainscroll.setContentOffset(CGPoint (x: 0, y: 150), animated: true)
            }
            else if textField == txtJudge1 || textField == txtJudge2
            {
                mainscroll.setContentOffset(CGPoint (x: 0, y: 300), animated: true)
            }
            else
            {
                mainscroll.setContentOffset(CGPoint (x: 0, y: 0), animated: true)
            }
        }
        else
        {
            if textField == txtShowName
            {
                mainscroll.setContentOffset(CGPoint (x: 0, y: 120), animated: true)
            }
            else if textField == txtJudge1 || textField == txtJudge2
            {
                mainscroll.setContentOffset(CGPoint (x: 0, y: 200), animated: true)
            }
            else
            {
                mainscroll.setContentOffset(CGPoint (x: 0, y: 0), animated: true)
            }
        }
       
    }
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool
    {
        return true
    }
    public func textFieldDidEndEditing(_ textField: UITextField)
    {
        
    }
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        return true
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        mainscroll.setContentOffset(CGPoint (x: 0, y: 0), animated: true)
        textField.endEditing(true)
        return false
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

