//
//  NPCategoryByDivisionEditViewController.swift
//  NAPHA
//
//  Created by Priyanka on 20/02/18.
//  Copyright © 2018 Esolz. All rights reserved.
//

import UIKit

class NPCategoryByDivisionEditViewController: GlobalViewController, UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {

    
    @IBOutlet weak var mainscroll: UIScrollView!
    @IBOutlet weak var DivisionCategoryTable: UITableView!
    @IBOutlet weak var txtShowName: UITextField!
    @IBOutlet weak var btnShow: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var dropDownIcon: UIImageView!
    
    
    var DivisionByCategoryArray = NSMutableArray()
    var ShowListArray = NSMutableArray()
    var wholeData = NSMutableDictionary(), totalWeightDictionary = NSMutableDictionary()
    
    
    var comingFrom = ""
    var ShowName : String = ""
    var ShowId : String = ""
    var ShowView = UIView()
    var SectionIndex : NSInteger = 0, showIndex : NSInteger = 0
    var RowIndex : NSInteger = 0
    var prefCantChange = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        saveBtn.layer.cornerRadius = self.SetButtonRadius(size: 15)
      //  txtShowName.text = "Default Show"

        
        if comingFrom == "General Preference"
        {
            dropDownIcon.isHidden = true
            btnShow.isUserInteractionEnabled = false
            txtShowName.text = "Default Show"
            txtShowName.isUserInteractionEnabled = false
            ShowListAPI()
            DivisionByCategoryAPI()
        }
        else
        {
            if (UserDefaults.standard.object(forKey: "ShowName") != nil && UserDefaults.standard.object(forKey: "ShowName") as! String != "")
            {
                HeaderView.showNameHeader.text = UserDefaults.standard.object(forKey: "ShowName") as! String
                ShowId = UserDefaults.standard.object(forKey: "ShowID") as! String
                txtShowName.text = UserDefaults.standard.object(forKey: "ShowName") as! String
                ShowListAPI()
                DivisionByCategoryAPI()
            }
            else
            {
            
                txtShowName.text = "Choose Show Name"
                ShowListAPI()
            }
            txtShowName.isUserInteractionEnabled = true
            btnShow.isUserInteractionEnabled = true
            dropDownIcon.isHidden = false
            
        }
        // Do any additional setup after loading the view.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - Show List Api Execute
    func ShowListAPI()
    {

        DispatchQueue.main.async {
            self.CheckLoader()
        }

        let  url_to_request = GlobalViewController.GLOBALAPI + "show/fetchshow"

        print("Full URL:",url_to_request)

        let urlString:NSURL = NSURL(string: url_to_request)!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: urlString as URL)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData

        var parameter : String = ""

        parameter = "searchvalue="

        print("parameter:",parameter)
        request.httpMethod = "POST"
        request.httpBody = parameter.data(using: String.Encoding.utf8)


        let task = session.dataTask(with: request as URLRequest, completionHandler: {


            (data, response, error) in


            guard let _:Data = data, let _:URLResponse = response, error == nil

                else
            {
                print("error")
                let alert = UIAlertController(title: "Error", message:error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                OperationQueue.main.addOperation
                    {
                        self.CheckLoader()
                        self.present(alert, animated: true, completion: nil)
                }
                return
            }
            //    self.statusCode = [(NSHTTPURLResponse*)response statusCode];
            if let httpResponse = response as? HTTPURLResponse
            {


                print("statusCode \(httpResponse.statusCode)")
                if (httpResponse.statusCode == 200)
                {

                    let JSONData = data
                    do {
                        let JSON = try JSONSerialization.jsonObject(with: JSONData!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                        guard let JSONDictionary: NSDictionary = JSON as? NSDictionary else {
                            print("Not a Dictionary")
                            // put in function

                            print("JSONDictionary \(JSON)")
                            DispatchQueue.main.async {
                                self.CheckLoader()


                            }
                            return
                        }

                        print("JSONDictionary \(JSONDictionary)")


                        let status : Bool = JSONDictionary.object(forKey: "status") as! Bool
                        
                        


                        if (status)
                        {
                            DispatchQueue.main.async {
                                self.CheckLoader()

                                let temparray = JSONDictionary.object(forKey: "details") as! NSArray

                                self.ShowListArray = temparray.mutableCopy() as! NSMutableArray
                                
                                
                                
                              //  let tempDict = NSMutableDictionary()
                                
                              //  tempDict.setValue("", forKey: "id")
                              //  tempDict.setValue("Default Show", forKey: "name")
                                
                              //  self.ShowListArray.add(tempDict)


                                if self.ShowListArray.count > 0
                                {
                                   
                                    
                                    
                              
                                }
                                else
                                {
                                   
                                    self.ShowListArray.removeAllObjects()
                                  
                                    //  self.ShowTable.isHidden = true
                                }


                            }



                        }
                        else
                        {


                            DispatchQueue.main.async {


                             //   let tempDict = NSMutableDictionary()
                                
                             //   tempDict.setValue("", forKey: "id")
                             //   tempDict.setValue("Default Show", forKey: "name")
                                
                              //  self.ShowListArray.add(tempDict)
                               
                                //  self.ShowTable.isHidden = true

                                self.CheckLoader()


                            }
                        }


                    }
                    catch let JSONError as NSError {
                        let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                        print("Error---\(String(describing: datastring))")
                        print("Error---\(String(describing: data))")
                        print("Error mesg---\(JSONError)")

                        let alert = UIAlertController(title: "Error", message: JSONError.localizedDescription as String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        OperationQueue.main.addOperation
                            {
                               
                                self.ShowListArray.removeAllObjects()
                               
                               
                                self.CheckLoader()
                                self.present(alert, animated: true, completion: nil)
                        }
                    }


                }
                else if(httpResponse.statusCode == 400)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            // self.ShowTable.isHidden = true
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else if(httpResponse.statusCode == 500)
                {
                    let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print("Error---\(String(describing: datastring))")


                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            //  self.ShowTable.isHidden = true
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        //  self.ShowTable.isHidden = true
                        self.CheckLoader()
                    }
                }
                

            }


        })


        task.resume()


    }
    // MARK: - Division by category Api Execute
    func DivisionByCategoryAPI()
    {
        
        DispatchQueue.main.async {
            self.CheckLoader()
        }
        
        let  url_to_request = GlobalViewController.GLOBALAPI + "show/preferences"
        
        print("Full URL:",url_to_request)
        
        let urlString:NSURL = NSURL(string: url_to_request)!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: urlString as URL)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        var parameter : String = ""
        
        parameter = "showid=\(ShowId)"
        
        print("parameter:",parameter)
        request.httpMethod = "POST"
        request.httpBody = parameter.data(using: String.Encoding.utf8)
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            
            
            (data, response, error) in
            
            
            guard let _:Data = data, let _:URLResponse = response, error == nil
                
                else
            {
                print("error")
                let alert = UIAlertController(title: "Error", message:error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                DispatchQueue.main.async
                    {
                        self.CheckLoader()
                        self.present(alert, animated: true, completion: nil)
                }
                return
            }
            //    self.statusCode = [(NSHTTPURLResponse*)response statusCode];
            if let httpResponse = response as? HTTPURLResponse
            {
                
                
                print("statusCode \(httpResponse.statusCode)")
                if (httpResponse.statusCode == 200)
                {
                    
                    let JSONData = data
                    do {
                        let JSON = try JSONSerialization.jsonObject(with: JSONData!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                        guard let JSONDictionary: NSDictionary = JSON as? NSDictionary else {
                            print("Not a Dictionary")
                            // put in function
                            
                            print("JSONDictionary \(JSON)")
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                            }
                            return
                        }
                        
                        print("JSONDictionary \(JSONDictionary)")
                        
                        
                        let status : Bool = JSONDictionary.object(forKey: "status") as! Bool
                        
                        
                        if (status)
                        {
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                                let temparray = JSONDictionary.object(forKey: "preference") as! NSArray
                                
                                if(JSONDictionary.object(forKey: "alreadyCalculated") as! Bool)
                                {
                                    self.prefCantChange = true
                                }
                                else
                                {
                                    self.prefCantChange = false
                                }
                                
                                self.DivisionByCategoryArray = temparray.mutableCopy() as! NSMutableArray
                                
//                                for i in 0 ... self.DivisionByCategoryArray.count - 1
//                                {
//                                    let dict : NSDictionary = self.DivisionByCategoryArray.object(at: i) as! NSDictionary
//                                    let tempdict : NSMutableDictionary = NSMutableDictionary(dictionary: dict)
//                                    tempdict.setValue(100, forKey:"TotalWeight")
//                                    self.DivisionByCategoryArray.replaceObject(at: i, with: tempdict)
//                                }
                                
                             //   print("Updated Array \(self.DivisionByCategoryArray)")
                                
                                if self.DivisionByCategoryArray.count > 0
                                {
                                    
                                        let preference = NSMutableArray()
                                        
                                        
                                        for sectionIndex in 0...5
                                        {
                                            let object : NSDictionary = self.DivisionByCategoryArray.object(at: sectionIndex) as! NSDictionary
                                            
                                            print ("Object:",object)
                                            
                                            let keys : NSMutableArray = NSMutableArray(array : object.allKeys as NSArray)
                                            
                                            var divisionId = ""
                                            
                                            for i in 0...1
                                            {
                                                if keys.object(at: i) as! String == "divisionid"
                                                {
                                                    divisionId = object.object(forKey: "divisionid") as! String
                                                }
                                            }
                                            
                                            keys.remove("divisionid")
                                            
                                            let divisionArray : NSMutableArray = NSMutableArray(array: object.object(forKey: keys.object(at: 0) as! String) as! NSArray)
                                            

                                            let tempDict = ["divisiondetails" : divisionArray, "divisionid" : divisionId] as [String : Any]
                                            
                                            preference.add(tempDict)
                                            
                                           
                                            var weight = 0
                                            
                                            for object in divisionArray
                                            {
                                                let tempDivDict : NSDictionary = object as! NSDictionary
                                                
                                                weight = weight + Int(tempDivDict.object(forKey: "weight") as! String)!
                                                
                                            }
                                            self.totalWeightDictionary.setValue(String(weight), forKey: String(sectionIndex))
                                            
                                        }
                                        
                                       // let tempDict = ["preference" : preference , "showid" : "0"] as [String : Any]
                                    self.wholeData = ["preference" : preference , "showid" : "\(self.ShowId as! String)"]
                                        print("Final Data : ",self.wholeData)
                                        
                                   //     SavePreference(AllData: tempDict as NSDictionary)
                                        
                                    
                                    
                                    
                                    
                                    
                                    
                                    self.DivisionCategoryTable.isHidden = false
                                    self.DivisionCategoryTable.reloadData()
                                    
                                    
                                    
                                    
                                }
                                else
                                {
                                    
                                    self.DivisionByCategoryArray.removeAllObjects()
                                    self.DivisionCategoryTable.reloadData()
                                    //  self.ShowTable.isHidden = true
                                }
                                
                                
                            }
                            
                            
                            
                        }
                        else
                        {
                            
                            
                            DispatchQueue.main.async {
                                
                                
                                self.CheckLoader()
                               
                                self.DivisionByCategoryArray.removeAllObjects()
                                self.DivisionCategoryTable.reloadData()
                                //  self.ShowTable.isHidden = true
                                
                                
                                
                                
                            }
                        }
                        
                        
                    }
                    catch let JSONError as NSError {
                        let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                        print("Error---\(String(describing: datastring))")
                        print("Error---\(String(describing: data))")
                        print("Error mesg---\(JSONError)")
                        
                        let alert = UIAlertController(title: "Error", message: JSONError.localizedDescription as String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async
                            {
                               self.CheckLoader()
                                self.DivisionByCategoryArray.removeAllObjects()
                                self.DivisionCategoryTable.reloadData()
                                // self.ShowTable.isHidden = true
                                
                                self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                    
                }
                else if(httpResponse.statusCode == 400)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async
                        {
                            // self.ShowTable.isHidden = true
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else if(httpResponse.statusCode == 500)
                {
                    let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print("Error---\(String(describing: datastring))")
                    
                    
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async
                        {
                            //  self.ShowTable.isHidden = true
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        //  self.ShowTable.isHidden = true
                        self.CheckLoader()
                    }
                }
                
                
            }
            
            
        })
        
        
        task.resume()
        
        
    }
    @IBAction func ShowClick(_ sender: Any)
    {
//        txtRadius.resignFirstResponder()
//        txtMileage.resignFirstResponder()
//        txtModel.resignFirstResponder()
//        mainscroll.contentOffset = CGPoint(x:0, y:0);
//
//        btnMaker.isSelected = true
        
        ShowView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        ShowView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        ShowView.center = self.view.center
        self.view.addSubview(ShowView)
        
        let typeView = UIView(frame: CGRect(x: 0, y: ShowView.frame.height-250, width: ShowView.frame.width, height: 250))
        typeView.backgroundColor = UIColor.white
        ShowView.addSubview(typeView)
        
        // Create a Picker
        let pickerView = UIPickerView()
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        
        pickerView.frame = CGRect(x: 10, y: 50, width: ShowView.frame.width-20, height: 200)
        pickerView.backgroundColor = UIColor.white
        
        
        // Add DataPicker to the view
        typeView.addSubview(pickerView)
        
        let savebtn:UIButton = UIButton(frame: CGRect(x: typeView.frame.size.width-60, y: pickerView.frame.origin.y-50, width: 50, height: 50))
        savebtn.setTitleColor(GlobalViewController.ThemeColor, for: .normal)
        savebtn.setTitle("Save", for: .normal)
        savebtn.addTarget(self, action:#selector(NPCategoryByDivisionEditViewController.ShowSaveClick), for: .touchUpInside)
        typeView.addSubview(savebtn)
        
        let cancelbtn:UIButton = UIButton(frame: CGRect(x: 10, y: pickerView.frame.origin.y-50, width: 80, height: 50))
        cancelbtn.setTitleColor(GlobalViewController.ThemeColor, for: .normal)
        cancelbtn.setTitle("Cancel", for: .normal)
        cancelbtn.addTarget(self, action:#selector(NPCategoryByDivisionEditViewController.ShowCancelClick), for: .touchUpInside)
        typeView.addSubview(cancelbtn)
    }
    // MARK: - company save click
    @objc func ShowSaveClick()
    {
       
            if(ShowName.characters.count > 0)
            {
                UserDefaults.standard.set(ShowName, forKey: "ShowName")
                UserDefaults.standard.set(ShowId, forKey: "ShowID")
                HeaderView.showNameHeader.text = ShowName
                txtShowName.text = ShowName
                txtShowName.textColor = UIColor.black
                ShowName = ""
                
                let object:NSDictionary = ShowListArray.object(at: showIndex) as! NSDictionary
                
                
//                let temp:NSArray = object.object(forKey: "judge") as! NSArray
//                UserDefaults.standard.set(temp, forKey: "JudgeArray")
                
            }
            else
            {
                let object = ShowListArray.object(at: 0) as! NSDictionary
                txtShowName.text = object.object(forKey: "name") as? String
                txtShowName.textColor = UIColor.black
                ShowId = (object.object(forKey: "id") as? String)!
                
            }
            
        
            
            ShowView.removeFromSuperview()
            
           DivisionByCategoryAPI()
        
    }
    
    // MARK: - cancel click
    @objc func ShowCancelClick()
    {
        if (UserDefaults.standard.object(forKey: "ShowName") != nil && UserDefaults.standard.object(forKey: "ShowName") as! String != "")
        {
            HeaderView.showNameHeader.text = UserDefaults.standard.object(forKey: "ShowName") as! String
            ShowId = UserDefaults.standard.object(forKey: "ShowID") as! String
            ShowName = UserDefaults.standard.object(forKey: "ShowName") as! String
        }
        else
        {
            HeaderView.showNameHeader.text = ""
            ShowName = ""
            //showLbl.text = "Choose Show Name"
            
        }
       
        
            
            ShowView.removeFromSuperview()
        
        
    }
    // MARK: - UIpicker Delegates
    // The number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
            return ShowListArray.count
     
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
       
            let object = ShowListArray.object(at: row) as! NSDictionary
            return object.object(forKey: "name") as? String
       
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
       
            if row>0
            {
                showIndex = row
                let object = ShowListArray.object(at: row) as! NSDictionary
                ShowName = (object.object(forKey: "name") as? String)!
                ShowId = (object.object(forKey: "id") as? String)!
                
            }
       
        
        
        
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel: UILabel? = (view as? UILabel)
        if pickerLabel == nil {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name: "Roboto-Regular", size: 30)
            pickerLabel?.textAlignment = .center
        }
        
        let object = ShowListArray.object(at: row) as! NSDictionary
        
            pickerLabel?.text = object.object(forKey: "name") as? String
        
        pickerLabel?.textColor = UIColor.black
        
        return pickerLabel!
    }
    
    
    // MARK: - table view delegate
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return DivisionByCategoryArray.count   // DivisionByCategoryArray.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // create a new cell if needed or reuse an old one
        
        let cell:DataListCell = DivisionCategoryTable.dequeueReusableCell(withIdentifier: "NPEntryCell") as! DataListCell!
        
        cell.selectionStyle = .none
        
        
        return cell
        
    }
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if let cell = cell as? DataListCell
        {

//            cell.btnSave.layer.cornerRadius = self.SetButtonRadius(size: 15)
//
//
//            cell.btnSave.tag = indexPath.section * 1000 + indexPath.row
//            cell.txtDivision.tag = indexPath.section * 1000 + indexPath.row
//            cell.txtCategory.tag = indexPath.section * 1000 + indexPath.row
            
//            if (indexPath.row % 2) > 0
//            {
                cell.txtWeight.tag = indexPath.section * 1000 + indexPath.row
                cell.txtSpread.tag = indexPath.section * 1000 + indexPath.row
//            }
//            else
//            {
//
//            }
            
            

          //  cell.btnSave.addTarget(self, action:#selector(NPCategoryByDivisionEditViewController.CategorySaveClick), for: .touchUpInside)

            let object : NSDictionary = DivisionByCategoryArray.object(at: indexPath.section) as! NSDictionary
            
            let keys : NSMutableArray = NSMutableArray(array : object.allKeys as NSArray)
            keys.remove("divisionid")
            
            if indexPath.row == 0
            {
                cell.lblDivisionNo.text = String(indexPath.section + 1)
                cell.lblDivisionNo.isHidden = false
                cell.numberView.backgroundColor = UIColor.init(red: (212.0/255.0), green: (187.0/255.0), blue: (159.0/255.0), alpha: 1.0)
                print("Keys:",keys)
                
                cell.txtDivision.isHidden = false
                cell.preferenceDivisionView.backgroundColor = UIColor.init(red: (212.0/255.0), green: (187.0/255.0), blue: (159.0/255.0), alpha: 1.0)
                cell.txtDivision.text = (keys.object(at: 0) as! String)
                
                let divisionArray : NSArray = object.object(forKey: keys.object(at: 0) as! String) as! NSArray
                
                print("Div Array:",divisionArray)
                let divisionObj : NSDictionary = divisionArray.object(at: indexPath.row) as! NSDictionary
                cell.txtWeight.text = (divisionObj.object(forKey: "weight") as! String)
                cell.txtSpread.text = (divisionObj.object(forKey: "spread") as! String)
                cell.txtCategory.text = (divisionObj.object(forKey: "categoryname") as! String)
                
                cell.txtWeight.isUserInteractionEnabled = true
                cell.txtSpread.isUserInteractionEnabled = true
                
                
                cell.txtCategory.isHidden = false
                cell.txtCategory.textColor = UIColor.white
                 cell.categoryView.backgroundColor = UIColor.init(red: (212.0/255.0), green: (187.0/255.0), blue: (159.0/255.0), alpha: 1.0)
                cell.txtSpread.isHidden = false
                cell.txtWeight.isHidden = false
            }
            else if indexPath.row == 4
            {
                cell.txtDivision.isHidden = true
                cell.lblDivisionNo.isHidden = true
                cell.txtSpread.isHidden = true
                cell.txtCategory.isHidden = false
                cell.txtWeight.isHidden = false
                cell.txtCategory.text = "Total"
                cell.txtCategory.textColor = UIColor.black
                cell.txtWeight.text = totalWeightDictionary.object(forKey: String(indexPath.section)) as! String
                
                cell.txtWeight.isUserInteractionEnabled = false
                
             cell.preferenceDivisionView.backgroundColor = UIColor.clear
             cell.categoryView.backgroundColor = UIColor.clear
             cell.numberView.backgroundColor = UIColor.clear
                
            }
            else if indexPath.row == 5
            {
                cell.txtDivision.isHidden = true
                cell.lblDivisionNo.isHidden = true
                cell.txtSpread.isHidden = true
                cell.txtCategory.isHidden = true
                cell.txtWeight.isHidden = true
                
                cell.txtWeight.isUserInteractionEnabled = false
                cell.txtSpread.isUserInteractionEnabled = false
                
                cell.preferenceDivisionView.backgroundColor = UIColor.clear
                cell.categoryView.backgroundColor = UIColor.clear
                cell.numberView.backgroundColor = UIColor.clear
            }
            else
            {
                cell.preferenceDivisionView.backgroundColor = UIColor.clear
                cell.numberView.backgroundColor = UIColor.clear
                
                cell.categoryView.backgroundColor = UIColor.init(red: (212.0/255.0), green: (187.0/255.0), blue: (159.0/255.0), alpha: 1.0)
                
                cell.txtDivision.isHidden = true
                cell.lblDivisionNo.isHidden = true
                
                cell.txtCategory.isHidden = false
                cell.txtCategory.textColor = UIColor.white
                cell.txtWeight.isHidden = false
                cell.txtSpread.isHidden = false
                
                print("Else Obj:",object,keys,object.object(forKey: keys.object(at: 0) as! String) as! NSArray)
                
                let divisionArray : NSArray = object.object(forKey: keys.object(at: 0) as! String) as! NSArray
                
                let divisionObj : NSDictionary = divisionArray.object(at: indexPath.row) as! NSDictionary
                cell.txtWeight.text = (divisionObj.object(forKey: "weight") as! String)
                cell.txtSpread.text = (divisionObj.object(forKey: "spread") as! String)
                cell.txtCategory.text = (divisionObj.object(forKey: "categoryname") as! String)
                
                cell.txtWeight.isUserInteractionEnabled = true
                cell.txtSpread.isUserInteractionEnabled = true
            }
            
            
            
//            if DivisionByCategoryArray.count > 0
//            {
//
//                let tempDict:NSDictionary = DivisionByCategoryArray.object(at: indexPath.section) as! NSDictionary
//
//                if indexPath.row == 0
//                {
//                    cell.lblDivisionNo.text = String(indexPath.section + 1)
//                    cell.lblDivisionNo.isHidden = false
//
//                    cell.txtDivision.text = (tempDict.object(forKey: "divisionname") as! String)
//                    cell.txtDivision.isHidden = false
//
//                  //  cell.btnSave.isHidden = false
//                }
//                else
//                {
//                    cell.lblDivisionNo.isHidden = true
//                    cell.txtDivision.isHidden = true
//
//                   // cell.btnSave.isHidden = true
//                }
//
//                if indexPath.row == 4
//                {
//
//                }
//                else
//                {
//
//
//                  //  self.DivisionByCategoryArray = temparray.mutableCopy() as! NSMutableArray
//                }
//
//                if indexPath.row < 4
//                {
//                    cell.txtWeight.isUserInteractionEnabled = true
//                    cell.txtCategory.isUserInteractionEnabled = true
//
//                   // cell.btnSave.isHidden = false
//
//                    let categoryarray = tempDict.object(forKey: "category") as! NSArray
//                    cell.txtCategory.text = categoryarray.object(at: indexPath.row) as? String
//
//                    let weightarray = tempDict.object(forKey: "weight") as! NSArray
//                    cell.txtWeight.text = weightarray.object(at: indexPath.row) as? String
//
//                    let spreadarray = tempDict.object(forKey: "spread") as! NSArray
//                    cell.txtSpread.text = spreadarray.object(at: indexPath.row) as? String
//
//                    cell.txtWeight.isUserInteractionEnabled = true
//                    cell.txtCategory.isUserInteractionEnabled = true
//                    cell.txtSpread.isUserInteractionEnabled = true
//
//                    cell.txtCategory.font = UIFont(name:GlobalViewController.RobotoRegular, size: 17)
//                    cell.txtWeight.font = UIFont(name:GlobalViewController.RobotoRegular, size: 17)
//
//                    cell.txtWeight.textColor = UIColor.black
//
////                    if cell.txtWeight.text?.characters.count == 0
////                    {
////                        cell.txtWeight.textColor = UIColor.black
////                    }
////                    else if cell.txtWeight.text == "?"
////                    {
////                        cell.txtWeight.textColor = UIColor.red
////                    }
////                    else if Float(cell.txtWeight.text!) as! Float > 100.0
////                    {
////                        cell.txtWeight.textColor = UIColor.red
////                    }
//                }
//                else if indexPath.row == 4
//                {
//                    cell.txtCategory.text = "Total"
//                    cell.txtWeight.text = String(tempDict.object(forKey: "TotalWeight") as! NSInteger)
//                    cell.txtSpread.text = ""
//
//                    cell.txtWeight.isUserInteractionEnabled = false
//                    cell.txtCategory.isUserInteractionEnabled = false
//                    cell.txtSpread.isUserInteractionEnabled = false
//
//                  //  cell.btnSave.isHidden = true
//
//                    cell.txtCategory.font = UIFont(name:GlobalViewController.RobotoBold, size: 17)
//                    cell.txtWeight.font = UIFont(name:GlobalViewController.RobotoBold, size: 17)
//
//                    if Float(cell.txtWeight.text!) == 100.0
//                    {
//                        cell.txtWeight.textColor = UIColor.black
//                    }
//                    else
//                    {
//                        cell.txtWeight.textColor = UIColor.red
//                    }
//                }
//                else
//                {
//                    cell.txtCategory.text = ""
//                    cell.txtWeight.text = ""
//                    cell.txtSpread.text = ""
//
//                    cell.txtWeight.isUserInteractionEnabled = false
//                    cell.txtCategory.isUserInteractionEnabled = false
//                    cell.txtSpread.isUserInteractionEnabled = false
//
//                    cell.txtCategory.font = UIFont(name:GlobalViewController.RobotoRegular, size: 17)
//                    cell.txtWeight.font = UIFont(name:GlobalViewController.RobotoRegular, size: 17)
//
//                    cell.txtWeight.textColor = UIColor.black
//
//                  //  cell.btnSave.isHidden = true
//                }
//
////                cell.lblShowName.text = (tempDict.object(forKey: "name") as! String)
////                cell.lblJudge1.text = (tempDict.object(forKey: "judgeonename") as! String)
////                cell.lblJudge2.text = (tempDict.object(forKey: "judgetwoname") as! String)
//
//
//
//                // now kept division and category user interaction false
//                cell.txtDivision.isUserInteractionEnabled = false
//                cell.txtCategory.isUserInteractionEnabled = false
//
//            }

        }
    }
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1.0
    }
    
    
    
    // MARK: - Category Save click
  //  @objc func CategorySaveClick(sender: UIButton)
    @IBAction func SaveClick(_ sender: Any)
    {
        if comingFrom != "General Preference"
        {
            if(txtShowName.text as! String == "Choose Show Name")
            {
                txtShowName.text = "Choose Show Name"
                txtShowName.textColor = UIColor.red
            }
            else
            {
        
        if(prefCantChange)
        {
            let alert = UIAlertController(title: nil, message:"Show in Progress, Preferences cannot be changed", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))

                    self.present(alert, animated: true, completion: nil)
        }
        else
        {
        var weightCount = 0
        for sectionWise in 0...5
        {
            
            if totalWeightDictionary.object(forKey: String(sectionWise)) as! String != "100"
            {
                weightCount += 1
                break
            }
        }
        
        if weightCount>0
        {
            let alert = UIAlertController(title: "Error", message:"Total Weight always to be 100", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            OperationQueue.main.addOperation
            {
                self.present(alert, animated: true, completion: nil)
            }
        }
        else
        {
            SavePreference(AllData: wholeData as NSDictionary)
        }
        
        
        }
            
        }
            
        }
        else if(totalWeightDictionary.count > 0)
        {
            var weightCount = 0
            for sectionWise in 0...5
            {
                
                if totalWeightDictionary.object(forKey: String(sectionWise)) as! String != "100"
                {
                    weightCount += 1
                    break
                }
            }
            
            if weightCount>0
            {
                let alert = UIAlertController(title: "Error", message:"Total Weight always to be 100", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                OperationQueue.main.addOperation
                    {
                        self.present(alert, animated: true, completion: nil)
                }
            }
            else
            {
                SavePreference(AllData: wholeData as NSDictionary)
            }
            
            
        }
        else
        {
            
        }
            
        
        
    }
    
    

    
    
    
    func SavePreference(AllData : NSDictionary)
    {
        
        
        DispatchQueue.main.async {
            self.CheckLoader()
        }
        
        
        let  url_to_request = GlobalViewController.GLOBALAPI + "show/updatepreferences"
        
        print("Full URL:",url_to_request)
        
        let urlString:NSURL = NSURL(string: url_to_request)!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: urlString as URL)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        

        let preferenceToSend = NSArray.init(object: AllData)
        var paramString : String = ""
        request.httpMethod = "POST"
        
        do {
            
            //Convert to Data
            let jsonData = try JSONSerialization.data(withJSONObject: preferenceToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            //Convert back to string. Usually only do this for debugging
            if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                print(JSONString)

                let allowedCharacterSet = CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[] ").inverted
                
                if let escapedString = JSONString.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) {
                    print(escapedString)
                    
                    paramString = "updatepreferences=\(escapedString)"
                }
                
               
            }
            

            print("param string \(paramString)")
            
        } catch {
            
        }
        
        request.httpBody = paramString.data(using: String.Encoding.utf8)
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            
            (data, response, error) in
            
            
            guard let _:Data = data, let _:URLResponse = response, error == nil
                
            else
            {
                print("error")
                let alert = UIAlertController(title: "Error", message:error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                OperationQueue.main.addOperation
                    {
                        self.CheckLoader()
                        self.present(alert, animated: true, completion: nil)
                }
                return
            }
            //    self.statusCode = [(NSHTTPURLResponse*)response statusCode];
            if let httpResponse = response as? HTTPURLResponse
            {
                
                
                print("statusCode \(httpResponse.statusCode)")
                if (httpResponse.statusCode == 200)
                {
                    
                    
                    
                    let JSONData = data
                    do {
                        let datastring = NSString(data: JSONData!, encoding: String.Encoding.utf8.rawValue)
                        print("Error---\( datastring as! String)")
                        
                        
                        let JSON = try JSONSerialization.jsonObject(with: JSONData!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                        guard let JSONDictionary: NSDictionary = JSON as? NSDictionary else {
                            print("Not a Dictionary")
                            // put in function
                            
                            print("JSONDictionary \(JSON)")
                            DispatchQueue.main.async {
                                self.CheckLoader()
                            }
                            return
                        }
                        
                        print("JSONDictionary \(JSONDictionary)")
                        
                       
                        
                        let status : Bool = JSONDictionary.object(forKey: "status") as! Bool
                        
                        if (status == true)
                        {
                            let alert = UIAlertController(title: nil, message: JSONDictionary.value(forKey: "message") as? String, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            
                            DispatchQueue.main.async {
                                self.present(alert, animated: true, completion: nil)
                                self.CheckLoader()
                                
                                
                            }
                        }
                        else
                        {
                            let alert = UIAlertController(title: "Error", message: JSONDictionary.value(forKey: "message") as? String, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            
                            DispatchQueue.main.async {
                                self.present(alert, animated: true, completion: nil)
                                self.CheckLoader()
                                
                                
                            }
                        }
                        
                        
                    }
                    catch let JSONError as NSError {
                        print("\(JSONError)")
                        let alert = UIAlertController(title: "Error", message: JSONError.localizedDescription as String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        OperationQueue.main.addOperation
                            {
                                self.CheckLoader()
                                self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                    
                }
                else if(httpResponse.statusCode == 400)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else if(httpResponse.statusCode == 500)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        self.CheckLoader()
                    }
                }
                
                
            }
            
            
        })
        
        
        task.resume()
        
        
    }
    
    
    
    // MARK: - TextField Delegates
    public func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if(prefCantChange)
        {
            let alert = UIAlertController(title: nil, message:"Show in Progress, Preferences cannot be changed", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
        
        textField.placeholder = nil
        
        let row = textField.tag % 1000
        let section = NSInteger(textField.tag / 1000)
        
        
        let indexPath = IndexPath(row: row, section: section)
        let cell = DivisionCategoryTable.cellForRow(at: indexPath) as! DataListCell
        
        SectionIndex = section
        
        RowIndex = row
        
        if textField.tag == 1
        {
            print ("Weight:",cell.txtWeight.text as! String)
        }
        else if textField.tag == 2
        {
            print ("Spread:",cell.txtSpread.text as! String)
        }
        
        }
        
    }
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool
    {
        return true
    }
    public func textFieldDidEndEditing(_ textField: UITextField)
    {
        var prefMutArray = NSMutableArray.init(array: wholeData.object(forKey: "preference") as! NSArray)
        
        let row = textField.tag % 1000
        let section = NSInteger(textField.tag / 1000)
        
        
        let indexPath = IndexPath(row: row, section: section)
        let cell = DivisionCategoryTable.cellForRow(at: indexPath) as! DataListCell
        
        var prefSubDict = NSMutableDictionary.init()
        prefSubDict = NSMutableDictionary(dictionary:prefMutArray.object(at: section) as! NSDictionary)
        
        
        let keys : NSMutableArray = NSMutableArray(array : prefSubDict.allKeys as NSArray)
        keys.remove("divisionid")
        
        
        var tempArray = NSMutableArray()
        tempArray = NSMutableArray(array : prefSubDict.object(forKey: keys.object(at: 0) as! String) as! NSArray)
        
        print("Presubdict:",prefSubDict.object(forKey: keys.object(at: 0) as! String))
        
        var tempDict = NSMutableDictionary()
        
        tempDict = NSMutableDictionary(dictionary : tempArray.object(at: row) as! NSDictionary)
        
        SectionIndex = section
        
        if textField == cell.txtWeight
        {
            tempDict.setValue(cell.txtWeight.text as! String, forKey: "weight")
            print ("Weight:",cell.txtWeight.text as! String)
        }
        else if textField == cell.txtSpread
        {
            tempDict.setValue(cell.txtWeight.text as! String, forKey: "spread")
            print ("Spread:",cell.txtSpread.text as! String)
        }
        
        tempArray.replaceObject(at: row, with: tempDict)
        prefSubDict.setValue(tempArray, forKey: keys.object(at: 0) as! String)
        
        prefMutArray.replaceObject(at: section, with: prefSubDict)
        
        wholeData.setValue(prefMutArray, forKey: "preference")
        
        print("After Edit:",wholeData)
        
    }
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        print("Text Value:",textField.text)
        
        let row = textField.tag % 1000
        let section = NSInteger(textField.tag / 1000)
        
        
        let indexPath = IndexPath(row: row, section: section)
        let cell = DivisionCategoryTable.cellForRow(at: indexPath) as! DataListCell
        
        let scalars = string.unicodeScalars
        
        if ((scalars[scalars.startIndex].value > 46 && scalars[scalars.startIndex].value < 58) || scalars[scalars.startIndex].value == 65533)
        {
            if textField == cell.txtWeight
            {
            
           var startString = ""
            
            if (textField.text != nil)
            {
                startString += textField.text!
            }
            
            startString += string
            
         //   cell.txtWeight.text = startString
                
                let limitNumber = Int(startString)
            
                var totalWeight = limitNumber as! Int
            
            for cellCount in 0...3
            {
                if cellCount != row
                {
                    let indexPath = IndexPath(row: cellCount, section: section)
                    let cell = DivisionCategoryTable.cellForRow(at: indexPath) as! DataListCell
                
                    totalWeight = totalWeight + Int(cell.txtWeight.text!)!
                    
                }
                
                
                
            }
                
                
                if totalWeight > 100
                {
                    return false
                }
                else
                {
                    
                    let indexPath = IndexPath(row: 4, section: section)
                    let cell = DivisionCategoryTable.cellForRow(at: indexPath) as! DataListCell
                    
                    cell.txtWeight.text = String(totalWeight)
                    
                    print("Total Weight:",cell.txtWeight.text as! String)
                    
                    totalWeightDictionary.setValue(String(totalWeight), forKey: String(section))
                    return true
                }
                
            }
            else
            {
            
                return true
                
            }
            
        }
         else
        {
            return false
        }
        

    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        //  HorseEntryTable.setContentOffset(CGPoint (x: 0, y: 0), animated: true)
        
        let row = textField.tag % 1000
        let section = NSInteger(textField.tag / 1000)
        
        var totalWeight = 0
        
        for cellCount in 0...3
        {
            
                let indexPath = IndexPath(row: cellCount, section: section)
                let cell = DivisionCategoryTable.cellForRow(at: indexPath) as! DataListCell
                if(cell.txtWeight.text as! String != "")
                {
                    totalWeight = totalWeight + Int(cell.txtWeight.text!)!
                }
            
            
        }
        
        let indexPath = IndexPath(row: 4, section: section)
        let cell = DivisionCategoryTable.cellForRow(at: indexPath) as! DataListCell
        
        cell.txtWeight.text = String(totalWeight)
        
        print("Total Weight:",cell.txtWeight.text as! String)
        
        totalWeightDictionary.setValue(String(totalWeight), forKey: String(section))
        
        
        
        textField.endEditing(true)
        return false
    }
    
    // MARK: - keyboard Will Show
    @objc func keyboardWillShow(notification: NSNotification)
    {
        let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        
        print(" section index \(SectionIndex)")
        //  var y = (HorseEntryTable.frame.origin.y + HorseEntryTable.frame.size.height) - (keyboardSize?.height)!
        
        //   print("height brfore \(SendMessageView.frame.size.height)")
        
        DivisionCategoryTable.frame = CGRect(x: DivisionCategoryTable.frame.origin.x,y: DivisionCategoryTable.frame.origin.y , width: DivisionCategoryTable.frame.size.width,height: (mainscroll.frame.size.height - DivisionCategoryTable.frame.origin.y) - (keyboardSize?.height)!);
        
        if DivisionByCategoryArray.count != 0
        {
            let index = IndexPath(row:RowIndex, section: SectionIndex) // use your index number or Indexpath
            DivisionCategoryTable.scrollToRow(at: index,at: .bottom, animated: false)
            DivisionCategoryTable.isScrollEnabled = false
        }
        
    }
    // MARK: - keyboard Will hide
    @objc func keyboardWillHide(notification: NSNotification)
    {
        let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        
        DivisionCategoryTable.frame = CGRect(x: DivisionCategoryTable.frame.origin.x,y: DivisionCategoryTable.frame.origin.y , width: DivisionCategoryTable.frame.size.width,height: (mainscroll.frame.size.height - DivisionCategoryTable.frame.origin.y));
        
        DivisionCategoryTable.isScrollEnabled = true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
