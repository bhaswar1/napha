//
//  NPJudgeCardViewController.swift
//  NAPHA
//
//  Created by admin on 16/02/18.
//  Copyright © 2018 Esolz. All rights reserved.
//

import UIKit
import MobileCoreServices

class NPJudgeCardViewController: GlobalViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIDragInteractionDelegate, UIDropInteractionDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var judgeCardTable: UITableView!
    @IBOutlet weak var numberView: UIView!
    @IBOutlet weak var placingView: UIView!
    @IBOutlet weak var oneView: UIView!
    @IBOutlet weak var twoView: UIView!
    @IBOutlet weak var threeView: UIView!
    @IBOutlet weak var fourView: UIView!
    @IBOutlet weak var fiveView: UIView!
    @IBOutlet weak var sixView: UIView!
    @IBOutlet weak var sevenView: UIView!
    @IBOutlet weak var eightView: UIView!
    @IBOutlet weak var nineView: UIView!
    @IBOutlet weak var tenView: UIView!
    @IBOutlet weak var dropDownImgVw: UIImageView!
    @IBOutlet weak var judge1View: UIView!
    @IBOutlet weak var oneLabel: UILabel!
    @IBOutlet weak var twoLabel: UILabel!
    @IBOutlet weak var threeLabel: UILabel!
    @IBOutlet weak var fourLabel: UILabel!
    @IBOutlet weak var fiveLabel: UILabel!
    @IBOutlet weak var sixLabel: UILabel!
    @IBOutlet weak var sevenLabel: UILabel!
    @IBOutlet weak var eightLabel: UILabel!
    @IBOutlet weak var nineLabel: UILabel!
    @IBOutlet weak var tenLabel: UILabel!
    @IBOutlet weak var horseNameEdit: UILabel!
    @IBOutlet weak var horseNameEditBtn: UIButton!
    @IBOutlet weak var functionEdit: UILabel!
    @IBOutlet weak var functionEditBtn: UIButton!
    @IBOutlet weak var smoothEdit: UILabel!
    @IBOutlet weak var smoothEditBtn: UIButton!
    @IBOutlet weak var threadEdit: UILabel!
    @IBOutlet weak var threadEditBtn: UIButton!
    @IBOutlet weak var terminoEdit: UILabel!
    @IBOutlet weak var terminoEditBtn: UIButton!
    @IBOutlet weak var calculate: UILabel!
    @IBOutlet weak var calculateBtn: UIButton!
    @IBOutlet weak var saveLbl: UILabel!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var showBtn: UIButton!
    @IBOutlet weak var showLbl: UILabel!
    @IBOutlet weak var judge1Lbl: UILabel!
    @IBOutlet weak var judge2Lbl: UILabel!
    @IBOutlet weak var classLbl: UILabel!
    @IBOutlet weak var classBtn: UIButton!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var divisionLbl: UILabel!
    @IBOutlet weak var defaultJudgeLbl: UILabel!
    @IBOutlet weak var tableHeaderView: UIView!
    @IBOutlet weak var mainScroll: UIScrollView!
    
    @IBOutlet weak var category1: UILabel!
    @IBOutlet weak var category2: UILabel!
    @IBOutlet weak var category3: UILabel!
    @IBOutlet weak var category4: UILabel!
    @IBOutlet weak var category1Weight: UILabel!
    @IBOutlet weak var category2Weight: UILabel!
    @IBOutlet weak var category3Weight: UILabel!
    @IBOutlet weak var category4Weight: UILabel!
    
    
    @IBOutlet weak var placeNumberView: UIView!
    @IBOutlet weak var number1ImgVw: UIImageView!
    @IBOutlet weak var numberImg2Vw: UIImageView!
    @IBOutlet weak var number3ImgVw: UIImageView!
    @IBOutlet weak var number4ImgVw: UIImageView!
    @IBOutlet weak var number5ImgVw: UIImageView!
    @IBOutlet weak var number6ImgVw: UIImageView!
    @IBOutlet weak var number7ImgVw: UIImageView!
    @IBOutlet weak var number8ImgVw: UIImageView!
    @IBOutlet weak var number9ImgVw: UIImageView!
    @IBOutlet weak var number10ImgVw: UIImageView!
    
    @IBOutlet weak var PrintLbl: UILabel!
    @IBOutlet weak var printBtn: UIButton!
    
    @IBOutlet weak var placeHeader: UIView!
    @IBOutlet weak var entryHeader: UIView!
    @IBOutlet weak var functionHeader: UIView!
    @IBOutlet weak var smoothHeader: UIView!
    @IBOutlet weak var threadHeader: UIView!
    @IBOutlet weak var terminoHeader: UIView!
    @IBOutlet weak var calculateHeader: UIView!
    @IBOutlet weak var saveHeader: UIView!
    
    @IBOutlet weak var clearBtn: UIButton!
    @IBOutlet weak var clearLbl: UILabel!
    
    var Error = false
    
  
    
    var ShowId : String = "", classId = "", divisionId = "", judgeId = "", category1Id = "", category2Id = "", category3Id = "", category4Id = "", cellIndex = ""
    var ShowView = UIView()
    
    var showList = NSMutableArray(), judgeList = NSMutableArray(), classList = NSMutableArray(), judgesCardList = NSMutableArray(), categoryList = NSMutableArray(), horseIdArray = NSMutableArray(), serialNoArray = NSMutableArray(), placeValueArray = NSMutableArray(), judgePlaceArray = NSMutableArray()
    var show = false, className = false, saveEdit = false, isCalculate = false
    var showIndex : NSInteger = 0, editTag = 0, selectedCell = -1, draggedImageTag = 0, invisibleCount = 0, classIndex = 0, textFieldTag = 0, selectTextTag = -1
    
    var category1Not = false, category2Not = false, category3Not = false, category4Not = false
    
    let appDel = UIApplication.shared.delegate as! AppDelegate
    
    
    
    
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        // Add a notification for keyboard change events
//        //   NotificationCenter.default.addObserver(self, selector: Selector("keyboardWillChange:"), name: .UIKeyboardWillChangeFrame, object: nil)
//
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
//    }
    
    
    // MARK: - keyboard Will Show
    @objc func keyboardWillShow(notification: NSNotification)
    {
        let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        
   
        
    }
    // MARK: - keyboard Will hide
    @objc func keyboardWillHide(notification: NSNotification)
    {
        let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        
       
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.transitionCurlUp, animations: {
            self.mainScroll.contentOffset.y = 0
        }, completion: nil)
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        if #available(iOS 11.0, *) {

            // Drag Interaction
            
//            let dragInteraction = UIDragInteraction.init(delegate: self as UIDragInteractionDelegate)
//            placeNumberView.addInteraction(dragInteraction)
            
            

            number1ImgVw.isUserInteractionEnabled = true
            numberImg2Vw.isUserInteractionEnabled = true
            number3ImgVw.isUserInteractionEnabled = true
            number4ImgVw.isUserInteractionEnabled = true
            number5ImgVw.isUserInteractionEnabled = true
            number6ImgVw.isUserInteractionEnabled = true
            number7ImgVw.isUserInteractionEnabled = true
            number8ImgVw.isUserInteractionEnabled = true
            number9ImgVw.isUserInteractionEnabled = true
            number10ImgVw.isUserInteractionEnabled = true

//            let dragInteraction = UIDragInteraction.init(delegate: self as UIDragInteractionDelegate)

            number1ImgVw.addInteraction(UIDragInteraction.init(delegate: self as UIDragInteractionDelegate))
            numberImg2Vw.addInteraction(UIDragInteraction.init(delegate: self as UIDragInteractionDelegate))
            number3ImgVw.addInteraction(UIDragInteraction.init(delegate: self as UIDragInteractionDelegate))
            number4ImgVw.addInteraction(UIDragInteraction.init(delegate: self as UIDragInteractionDelegate))
            number5ImgVw.addInteraction(UIDragInteraction.init(delegate: self as UIDragInteractionDelegate))
            number6ImgVw.addInteraction(UIDragInteraction.init(delegate: self as UIDragInteractionDelegate))
            number7ImgVw.addInteraction(UIDragInteraction.init(delegate: self as UIDragInteractionDelegate))
            number8ImgVw.addInteraction(UIDragInteraction.init(delegate: self as UIDragInteractionDelegate))
            number9ImgVw.addInteraction(UIDragInteraction.init(delegate: self as UIDragInteractionDelegate))
            number10ImgVw.addInteraction(UIDragInteraction.init(delegate: self as UIDragInteractionDelegate))


            // Drop Interaction

            let dropInteraction = UIDropInteraction.init(delegate: self as! UIDropInteractionDelegate)
            judgeCardTable.addInteraction(dropInteraction)


         //    judgeCardTable.dropDelegate = self as! UITableViewDropDelegate

        } else {
            // Fallback on earlier versions
        }
        
       
        
        self.setData()
        
    }
    
    func setData()
    {
        placingView.layer.borderWidth = 1.0
        placingView.layer.borderColor = UIColor.init(red:198.0/255, green: 150.0/255.0, blue: 92.0/255.0, alpha: 1.0).cgColor
        
        oneView.layer.borderWidth = 1.0
        oneView.layer.borderColor = UIColor.lightGray.cgColor
        
        twoView.layer.borderWidth = 1.0
        twoView.layer.borderColor = UIColor.lightGray.cgColor
        
        threeView.layer.borderWidth = 1.0
        threeView.layer.borderColor = UIColor.lightGray.cgColor
        
        fourView.layer.borderWidth = 1.0
        fourView.layer.borderColor = UIColor.lightGray.cgColor
        
        fiveView.layer.borderWidth = 1.0
        fiveView.layer.borderColor = UIColor.lightGray.cgColor
        
        sixView.layer.borderWidth = 1.0
        sixView.layer.borderColor = UIColor.lightGray.cgColor
        
        sevenView.layer.borderWidth = 1.0
        sevenView.layer.borderColor = UIColor.lightGray.cgColor
        
        eightView.layer.borderWidth = 1.0
        eightView.layer.borderColor = UIColor.lightGray.cgColor
        
        nineView.layer.borderWidth = 1.0
        nineView.layer.borderColor = UIColor.lightGray.cgColor
        
        tenView.layer.borderWidth = 1.0
        tenView.layer.borderColor = UIColor.lightGray.cgColor
        
        
        PrintLbl.layer.cornerRadius = 20.0
        horseNameEdit.layer.cornerRadius = 20.0
        functionEdit.layer.cornerRadius = 20.0
        smoothEdit.layer.cornerRadius = 20.0
        threadEdit.layer.cornerRadius = 20.0
        terminoEdit.layer.cornerRadius = 20.0
        calculate.layer.cornerRadius = 20.0
        saveLbl.layer.cornerRadius = 20.0
        clearLbl.layer.cornerRadius = 20.0
        
        
        saveBtn.addTarget(self, action: #selector(NPJudgeCardViewController.editClick), for: .touchUpInside)
        calculateBtn.addTarget(self, action: #selector(NPJudgeCardViewController.editClick), for: .touchUpInside)
        terminoEditBtn.addTarget(self, action: #selector(NPJudgeCardViewController.editClick), for: .touchUpInside)
        threadEditBtn.addTarget(self, action: #selector(NPJudgeCardViewController.editClick), for: .touchUpInside)
        smoothEditBtn.addTarget(self, action: #selector(NPJudgeCardViewController.editClick), for: .touchUpInside)
        functionEditBtn.addTarget(self, action: #selector(NPJudgeCardViewController.editClick), for: .touchUpInside)
        horseNameEditBtn.addTarget(self, action: #selector(NPJudgeCardViewController.editClick), for: .touchUpInside)
        
        
        if (UserDefaults.standard.object(forKey: "ShowName") != nil && UserDefaults.standard.object(forKey: "ShowName") as! String != "")
        {
            HeaderView.showNameHeader.text = UserDefaults.standard.object(forKey: "ShowName") as! String
            
          showLbl.text = UserDefaults.standard.object(forKey: "ShowName") as! String
            ShowId = UserDefaults.standard.object(forKey: "ShowID") as! String
            
            showBtn.isUserInteractionEnabled = false
            dropDownImgVw.isHidden = true
        }
        
    }
    
    
    // MARK: - Edit Tapped
    @objc func editClick(sender : UIButton)
    {
        if showLbl.text == "Choose Show Name"
        {
            showLbl.textColor = UIColor.red
        }
        else if classLbl.text == "CLASS #:"
        {
            classLbl.textColor = UIColor.red
        }
         else
        {
        if sender.tag == 1
        {
            
            if (functionEdit.text == "ENTER" && smoothEdit.text == "ENTER" && threadEdit.text == "ENTER" && terminoEdit.text == "ENTER")
            {
                placeNumberView.isHidden = true
            
            if horseNameEdit.text == "ENTER"
            {
                horseNameEdit.text = "DONE"
                horseNameEdit.backgroundColor = UIColor.orange
                
                for index in 0...9
                {
                    let indexPath = NSIndexPath.init(row: index, section: 0)
                    
                    let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                    cell.entryView.layer.borderWidth = 1.0
                    cell.entryView.layer.borderColor = UIColor.darkGray.cgColor
                    cell.isUserInteractionEnabled = true
                    cell.entryView.isUserInteractionEnabled = true
                    cell.entryField.isUserInteractionEnabled = true
                    
                    
                    functionEdit.text = "ENTER"
                    functionEdit.backgroundColor = UIColor.white
                    
                    cell.functionView.layer.borderWidth = 0.0
                    cell.functionView.layer.borderColor = UIColor.clear.cgColor
                    cell.functionField.isUserInteractionEnabled = false
                    
                    
                    smoothEdit.text = "ENTER"
                    smoothEdit.backgroundColor = UIColor.white
                    
                    cell.smoothView.layer.borderWidth = 0.0
                    cell.smoothView.layer.borderColor = UIColor.clear.cgColor
                    cell.smoothField.isUserInteractionEnabled = false
                    
                    
                    threadEdit.text = "ENTER"
                    threadEdit.backgroundColor = UIColor.white
                    
                    cell.threadView.layer.borderWidth = 0.0
                    cell.threadView.layer.borderColor = UIColor.clear.cgColor
                    cell.threadField.isUserInteractionEnabled = false
                    
                    
                    terminoEdit.text = "ENTER"
                    terminoEdit.backgroundColor = UIColor.white
                    
                    cell.terminoView.layer.borderWidth = 0.0
                    cell.terminoView.layer.borderColor = UIColor.clear.cgColor
                    cell.terminoField.isUserInteractionEnabled = false
                    
                }
                
            }
            else
            {
                Error = false
                
                if (self.mainScroll.contentOffset.y > 0)
                {
                    UIView.animate(withDuration: 1.5, delay: 0, options: UIViewAnimationOptions.transitionCurlUp, animations: {
                        self.mainScroll.contentOffset.y = 0
                    }, completion: nil)
                    
                }
                
                horseIdArray.removeAllObjects()
                serialNoArray.removeAllObjects()
                
//                horseNameEdit.text = "ENTER"
//                horseNameEdit.backgroundColor = UIColor.white
                
                
//                for index in 0...9
//                {
//                    let indexPath = NSIndexPath.init(row: index, section: 0)
//
//                    let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
//                    cell.entryView.layer.borderWidth = 0.0
//                    cell.entryView.layer.borderColor = UIColor.clear.cgColor
//                    cell.entryField.isUserInteractionEnabled = false
//
//                }
                
                
                
                for index in 0...9 
                {
                    let indexPath = NSIndexPath.init(row: index, section: 0)
                    let horseCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                    
                    
                    print("Horse Check:",horseCell.entryField.text as! String)
                    
                    if (horseCell.entryField.text as! String != "")
                    {
                        
                        // Duplicate Horse Entry Checking
                        var dupHorseNumbers = NSMutableString()
                        
                        var dupCheck = 0
                        for dupIndex in  0...9
                        {
                            let dupIndexPath = NSIndexPath.init(row: dupIndex, section: 0)
                            let dupHorseCell = judgeCardTable.cellForRow(at: dupIndexPath as IndexPath) as! DataListCell
                            
                            if(index != dupIndex && dupHorseCell.entryField.text as! String != "" && horseCell.entryField.text as! String == dupHorseCell.entryField.text as! String)
                            {
                                dupCheck += 1
                               
                            dupHorseNumbers.append(dupHorseCell.entryField.text as! String)
                            }
                            
                            
                        }
                        
                        if (dupCheck > 0)
                        {
                            Error = true
                            
                            let alert = UIAlertController(title: "Error", message: "Horse Entry # \(dupHorseNumbers) repeated", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            break
                        }
                        else     // If Duplicate not found
                        {
                            var judgeExist = 0, existIndex = 0
                            var existObject = NSMutableDictionary()
                            
                            if (judgesCardList.count > 0)
                            {
                            
                                for judgeCard in 0...(judgesCardList.count - 1)
                                {
                                    let judgeObject = judgesCardList.object(at: judgeCard) as! NSDictionary
                            
                                    if (judgeObject.object(forKey: "entrynumber") as! String == horseCell.entryField.text as! String)
                                    {
                                        judgeExist += 1
                                        
                                        existIndex = judgeCard
                                        
                                        existObject = NSMutableDictionary(dictionary:judgeObject as! NSDictionary)
                                    }
                                    
                                }
                            
                            }
                            
                            
                            if(judgeExist == 0)
                            {
                            
                                let newDict = NSMutableDictionary()
                                            newDict.setValue("0", forKey: "calculate")
                                newDict.setValue(horseCell.entryField.text as! String, forKey: "entrynumber")
                                newDict.setValue("", forKey: "horsename")
                                newDict.setValue("", forKey: "jgscrdid")
                                newDict.setValue("0", forKey: "judgeplace")
                                newDict.setValue(String(textFieldTag + 1), forKey: "sl_no")
                                            newDict.setValue("0.00", forKey: "total")
                            
                            
                                let newCategory = NSMutableArray()
                                for catCount in 0...3
                                {
                                    let catDict = NSMutableDictionary()
                                    if (catCount == 0)
                                    {
                                        catDict.setValue(category1Id, forKey: "categoryid")
                                    }
                                    else if (catCount == 2)
                                    {
                                        catDict.setValue(category2Id, forKey: "categoryid")
                                    }
                                    else if (catCount == 2)
                                    {
                                        catDict.setValue(category3Id, forKey: "categoryid")
                                    }
                                    else if (catCount == 3)
                                    {
                                        catDict.setValue(category4Id, forKey: "categoryid")
                                    }
                            
                                        catDict.setValue("", forKey: "jgscrdtlsid")
                                        catDict.setValue("0", forKey: "place")
                                        catDict.setValue("0.00", forKey: "place_vale")
                            
                            
                            
                                        newCategory.add(catDict)
                                    
                                }
                            
                                newDict.setValue(newCategory, forKey: "category_details")
                                

                                if (index <= (judgesCardList.count - 1))
                                {
                                    judgesCardList.replaceObject(at: index, with:newDict )
                                }
                                else
                                {
                                    judgesCardList.add(newDict)
                                    
                                }
                                
                                print("Judge Blank Add:",judgesCardList)
                            
                            }
                            else      // If Entry exist in Array
                            {
                                judgesCardList.remove(existObject)
                                
                                judgesCardList.insert(existObject, at: index)
                                
                                print("Judge Replace:",judgesCardList)
                                
                            }
                            
                            
                            
                            
                            
                        }
                        
                    }
                    else
                    {
                        
                        let newDict = NSMutableDictionary()
                        newDict.setValue("0", forKey: "calculate")
                        newDict.setValue(horseCell.entryField.text as! String, forKey: "entrynumber")
                        newDict.setValue("", forKey: "horsename")
                        newDict.setValue("", forKey: "jgscrdid")
                        newDict.setValue("0", forKey: "judgeplace")
                        newDict.setValue(String(textFieldTag + 1), forKey: "sl_no")
                        newDict.setValue("0.00", forKey: "total")
                        
                        
                        var newCategory = NSMutableArray()
                        for catCount in 0...3
                        {
                            let catDict = NSMutableDictionary()
                            if (catCount == 0)
                            {
                                catDict.setValue(category1Id, forKey: "categoryid")
                            }
                            else if (catCount == 2)
                            {
                                catDict.setValue(category2Id, forKey: "categoryid")
                            }
                            else if (catCount == 2)
                            {
                                catDict.setValue(category3Id, forKey: "categoryid")
                            }
                            else if (catCount == 3)
                            {
                                catDict.setValue(category4Id, forKey: "categoryid")
                            }
                            
                            catDict.setValue("", forKey: "jgscrdtlsid")
                            catDict.setValue("0", forKey: "place")
                            catDict.setValue("0.00", forKey: "place_vale")
                            
                            
                            
                            newCategory.add(catDict)
                            
                        }
                        
                        newDict.setValue(newCategory, forKey: "category_details")
                        

                            
                            if (index <= (judgesCardList.count - 1))
                            {
                                judgesCardList.replaceObject(at: index, with:newDict )
                            }
                            else
                            {
                                judgesCardList.add(newDict)
                                
                            }
                        
                        print("When Blank Field:",judgesCardList)
                        
                    }
                    
                    
                }
                
        

                // To Remove all blank Entry from Array
                
                var indexes = NSMutableSet()
                let removeObjects = NSMutableArray()
                
                
                if(judgesCardList.count > 0)
                {
                    
                for removeIndex in 0...(judgesCardList.count - 1)
                {
                    let removeIndexPath = NSIndexPath.init(row: removeIndex, section: 0)
                    let removeCell = judgeCardTable.cellForRow(at: removeIndexPath as IndexPath) as! DataListCell
                    
                    if (removeCell.entryField.text as! String == "")
                    {
                        removeObjects.add(removeIndex)
                        
                        
                    }
                    
                }
                
                
                }
                
                
             //   let indexAnimals = [0, 3, 4]
                let arrayRemainingArray = judgesCardList
                    .enumerated()
                    .filter { !removeObjects.contains($0.offset) }
                    .map { $0.element }
                
                print(arrayRemainingArray)
                
                judgesCardList = NSMutableArray(array:arrayRemainingArray)
                
                
                print("Current JudgesCard:",judgesCardList)
                
                for horseIndex in 0...(judgesCardList.count - 1)
                {
                    let addIndexPath = NSIndexPath.init(row: horseIndex, section: 0)
                    let addCell = judgeCardTable.cellForRow(at: addIndexPath as IndexPath) as! DataListCell
                    
                    let judgeObject = judgesCardList.object(at: horseIndex) as! NSDictionary
                    
                    horseIdArray.add(judgeObject.object(forKey: "entrynumber") as! String)
                    serialNoArray.add(String(horseIndex + 1))
                    
                }
                
                if(horseIdArray.count > 0 && !Error)
                {
                    horseNameEdit.text = "ENTER"
                    horseNameEdit.backgroundColor = UIColor.white
                    
                    
                    for index in 0...9
                    {
                        let indexPath = NSIndexPath.init(row: index, section: 0)
                        
                        let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                        cell.entryView.layer.borderWidth = 0.0
                        cell.entryView.layer.borderColor = UIColor.clear.cgColor
                        cell.entryField.isUserInteractionEnabled = false
                        
                    }
                    
                    // Add Judges Card
                  //  JudgesCardAddEdit(horseId: horseIdArray)
                    
                    checkHorseExist(horseId: horseIdArray)
                    
                    
                }
                

            }
                
            }
            else
            {
                let alert = UIAlertController(title: "Error", message: "Please Complete Your Edit", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
            
        }
        else if sender.tag == 2
        {
            if (horseNameEdit.text == "ENTER" && smoothEdit.text == "ENTER" && threadEdit.text == "ENTER" && terminoEdit.text == "ENTER")
            {
                placeNumberView.isHidden = false
            
            if functionEdit.text == "ENTER"
            {
                editTag = sender.tag
                
                let indexPath = NSIndexPath.init(row: 0, section: 0)
                
                let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                
                if (cell.entryField.text != "")
                {

                    // To make the number hidden
                    invisibleCount = 0
                    self.number1ImgVw.isHidden = false
                    self.numberImg2Vw.isHidden = false
                    self.number3ImgVw.isHidden = false
                    self.number4ImgVw.isHidden = false
                    self.number5ImgVw.isHidden = false
                    self.number6ImgVw.isHidden = false
                    self.number7ImgVw.isHidden = false
                    self.number8ImgVw.isHidden = false
                    self.number9ImgVw.isHidden = false
                    self.number10ImgVw.isHidden = false
                    
                    
                    for placeCheck in 0...9
                    {
                        let indexPath = NSIndexPath.init(row: placeCheck, section: 0)
                        
                        let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                        
                        if (cell.functionField.text == "1")
                        {
                            self.number1ImgVw.isHidden = true
                            invisibleCount += 1
                        }
                        else if (cell.functionField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = true
                            invisibleCount += 1
                        }
                        else if (cell.functionField.text == "3")
                        {
                            self.number3ImgVw.isHidden = true
                            invisibleCount += 1
                        }
                        else if (cell.functionField.text == "4")
                        {
                            self.number4ImgVw.isHidden = true
                            invisibleCount += 1
                        }
                        else if (cell.functionField.text == "5")
                        {
                            self.number5ImgVw.isHidden = true
                            invisibleCount += 1
                        }
                        else if (cell.functionField.text == "6")
                        {
                            self.number6ImgVw.isHidden = true
                            invisibleCount += 1
                        }
                        else if (cell.functionField.text == "7")
                        {
                            self.number7ImgVw.isHidden = true
                            invisibleCount += 1
                        }
                        else if (cell.functionField.text == "8")
                        {
                            self.number8ImgVw.isHidden = true
                            invisibleCount += 1
                        }
                        else if (cell.functionField.text == "9")
                        {
                            self.number9ImgVw.isHidden = true
                            invisibleCount += 1
                        }
                        else if (cell.functionField.text == "10")
                        {
                            self.number10ImgVw.isHidden = true
                            invisibleCount += 1
                        }
                        
                    }
                    
                    
                    if (self.mainScroll.contentOffset.y == 0)
                    {
                        UIView.animate(withDuration: 1.5, delay: 0, options: UIViewAnimationOptions.transitionCurlUp, animations: {
                            self.mainScroll.contentOffset.y = 70
                        }, completion: nil)
                        
                    }
                    
                
                    if (horseNameEdit.text == "ENTER")
                    {
//                        if (invisibleCount == 10)
//                        {
//                            functionEdit.text = "CLEAR"
//                            functionEdit.backgroundColor = UIColor.red
//                        }
//                        else
//                        {
                        functionEdit.text = "DONE"
                        functionEdit.backgroundColor = UIColor.orange
//                        }
                
                        for index in 0...9
                        {
                            let indexPath = NSIndexPath.init(row: index, section: 0)
                    
                            let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                            cell.functionView.layer.borderWidth = 1.0
                            cell.functionView.layer.borderColor = UIColor.darkGray.cgColor
                           // cell.functionField.isUserInteractionEnabled = true
                    
                    
                            horseNameEdit.text = "ENTER"
                            horseNameEdit.backgroundColor = UIColor.white
                    
                            cell.nameView.layer.borderWidth = 0.0
                            cell.nameView.layer.borderColor = UIColor.clear.cgColor
                            cell.nameField.isUserInteractionEnabled = false
                    
                    
                            smoothEdit.text = "ENTER"
                            smoothEdit.backgroundColor = UIColor.white
                    
                            cell.smoothView.layer.borderWidth = 0.0
                            cell.smoothView.layer.borderColor = UIColor.clear.cgColor
                            cell.smoothField.isUserInteractionEnabled = false
                    
                    
                            threadEdit.text = "ENTER"
                            threadEdit.backgroundColor = UIColor.white
                    
                            cell.threadView.layer.borderWidth = 0.0
                            cell.threadView.layer.borderColor = UIColor.clear.cgColor
                            cell.threadField.isUserInteractionEnabled = false
                    
                    
                            terminoEdit.text = "ENTER"
                            terminoEdit.backgroundColor = UIColor.white
                    
                            cell.terminoView.layer.borderWidth = 0.0
                            cell.terminoView.layer.borderColor = UIColor.clear.cgColor
                            cell.terminoField.isUserInteractionEnabled = false
                    
                        }
                    
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Error", message: "Please Complete Entry Column First", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                
                }
                else
                {
                    let alert = UIAlertController(title: "Error", message: "Please Enter Entry Number", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                
            }
            else if (functionEdit.text == "DONE")
            {
                
                
                
//                if (self.mainScroll.contentOffset.y > 0)
//                {
//                    UIView.animate(withDuration: 1.5, delay: 0, options: UIViewAnimationOptions.transitionCurlUp, animations: {
//                        self.mainScroll.contentOffset.y = 0
//                    }, completion: nil)
//
//                }
//
//
                    placeValueArray.removeAllObjects()
                    serialNoArray.removeAllObjects()
//
//                functionEdit.text = "ENTER"
//                functionEdit.backgroundColor = UIColor.white
                
                for index in 0...9
                {
                    let indexPath = NSIndexPath.init(row: index, section: 0)
                    
                    let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
//                    cell.functionView.layer.borderWidth = 1.0
//                    cell.functionView.layer.borderColor = UIColor.clear.cgColor
//                    cell.functionField.isUserInteractionEnabled = false
                    
                    
                    if (cell.entryField.text != "" && cell.functionField.text != "")
                    {
                        placeValueArray.add(cell.functionField.text as! String)
                        serialNoArray.add(String(index + 1))
                    }
                    
                }
                

                
                var totalRepeat = 0
                for place in 1...10
                {
                    var repeatCount = 0
                    for indexCount in 0...9
                    {
                        let indexPath = NSIndexPath.init(row: indexCount, section: 0)
                        
                       let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                        
                        print("Place:",String(place),cell.functionField.text as! String)
                        
                        if cell.functionField.text as! String == String(place)
                        {
                            repeatCount = repeatCount + 1
                            
                            if repeatCount > 1
                            {
                                totalRepeat = totalRepeat + 1
                            }
                        }
                    }
                }
                
                if totalRepeat > 0
                {
                    let alert = UIAlertController(title: "Error", message: "You can't repeat the Rating", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {

                    
                    
                    
                    if (placeValueArray.count > 0 && judgesCardList.count == placeValueArray.count)
                    {
                        editTag = 0
                        
                        if (self.mainScroll.contentOffset.y > 0)                {                    UIView.animate(withDuration: 1.5, delay: 0, options: UIViewAnimationOptions.transitionCurlUp, animations: {
                            self.mainScroll.contentOffset.y = 0                    }, completion: nil)
                            
                        }
                        
                        functionEdit.text = "ENTER"
                        
                        functionEdit.backgroundColor = UIColor.white
                        
                        
                        for index in 0...9
                        {
                            let indexPath = NSIndexPath.init(row: index, section: 0)
                            
                            let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                            cell.functionView.layer.borderWidth = 1.0
                            cell.functionView.layer.borderColor = UIColor.clear.cgColor
                            cell.functionField.isUserInteractionEnabled = false
                            
                        }
                        
                        
                        CategoryAddEdit(categoryId: category1Id, placeValue : placeValueArray, editTag: sender.tag)
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Error", message: "Please Place All Entries", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    
                    
                }
                
                
                    
 //               }
            }
//            else if (functionEdit.text == "CLEAR")
//            {
//                functionEdit.text = "DONE"
//                functionEdit.backgroundColor = UIColor.orange
//
//                invisibleCount = 0
//                self.number1ImgVw.isHidden = false
//                self.numberImg2Vw.isHidden = false
//                self.number3ImgVw.isHidden = false
//                self.number4ImgVw.isHidden = false
//                self.number5ImgVw.isHidden = false
//                self.number6ImgVw.isHidden = false
//                self.number7ImgVw.isHidden = false
//                self.number8ImgVw.isHidden = false
//                self.number9ImgVw.isHidden = false
//                self.number10ImgVw.isHidden = false
//
//
//                for placeCheck in 0...9
//                {
//                    let indexPath = NSIndexPath.init(row: placeCheck, section: 0)
//
//                    let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
//
//                    cell.functionField.text = ""
//                    cell.functionView.backgroundColor = UIColor.clear
//
//                }
//            }
            
            }
            else
            {
                let alert = UIAlertController(title: "Error", message: "Please Complete Your Edit", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
        }
        else if sender.tag == 3
        {
            if (horseNameEdit.text == "ENTER" && functionEdit.text == "ENTER" && threadEdit.text == "ENTER" && terminoEdit.text == "ENTER")
            {
                placeNumberView.isHidden = false
            
            if smoothEdit.text == "ENTER"
            {
                editTag = sender.tag
                
                let indexPath = NSIndexPath.init(row: 0, section: 0)
                
                let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                
                if (cell.entryField.text != "")
                {
                    // To make the number hidden
                    invisibleCount = 0
                    
                    self.number1ImgVw.isHidden = false
                    self.numberImg2Vw.isHidden = false
                    self.number3ImgVw.isHidden = false
                    self.number4ImgVw.isHidden = false
                    self.number5ImgVw.isHidden = false
                    self.number6ImgVw.isHidden = false
                    self.number7ImgVw.isHidden = false
                    self.number8ImgVw.isHidden = false
                    self.number9ImgVw.isHidden = false
                    self.number10ImgVw.isHidden = false
                    
                    
                    for placeCheck in 0...9
                    {
                        let indexPath = NSIndexPath.init(row: placeCheck, section: 0)
                        
                        let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                        
                        if (cell.smoothField.text == "1")
                        {
                            invisibleCount += 1
                            self.number1ImgVw.isHidden = true
                        }
                        else if (cell.smoothField.text == "2")
                        {
                            invisibleCount += 1
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (cell.smoothField.text == "3")
                        {
                            invisibleCount += 1
                            self.number3ImgVw.isHidden = true
                        }
                        else if (cell.smoothField.text == "4")
                        {
                            invisibleCount += 1
                            self.number4ImgVw.isHidden = true
                        }
                        else if (cell.smoothField.text == "5")
                        {
                            invisibleCount += 1
                            self.number5ImgVw.isHidden = true
                        }
                        else if (cell.smoothField.text == "6")
                        {
                            invisibleCount += 1
                            self.number6ImgVw.isHidden = true
                        }
                        else if (cell.smoothField.text == "7")
                        {
                            invisibleCount += 1
                            self.number7ImgVw.isHidden = true
                        }
                        else if (cell.smoothField.text == "8")
                        {
                            invisibleCount += 1
                            self.number8ImgVw.isHidden = true
                        }
                        else if (cell.smoothField.text == "9")
                        {
                            invisibleCount += 1
                            self.number9ImgVw.isHidden = true
                        }
                        else if (cell.smoothField.text == "10")
                        {
                            invisibleCount += 1
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    
                    if (self.mainScroll.contentOffset.y == 0)
                    {
                        UIView.animate(withDuration: 1.5, delay: 0, options: UIViewAnimationOptions.transitionCurlUp, animations: {
                            self.mainScroll.contentOffset.y = 70
                        }, completion: nil)
                        
                    }
                    
                    if (horseNameEdit.text == "ENTER")
                    {
//                        if (invisibleCount == 10)
//                        {
//                            smoothEdit.text = "CLEAR"
//                            smoothEdit.backgroundColor = UIColor.red
//                        }
//                        else
//                        {
                            smoothEdit.text = "DONE"
                            smoothEdit.backgroundColor = UIColor.orange
//                        }
                        
                
                for index in 0...9
                {
                    let indexPath = NSIndexPath.init(row: index, section: 0)
                    
                    let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                    cell.smoothView.layer.borderWidth = 1.0
                    cell.smoothView.layer.borderColor = UIColor.darkGray.cgColor
                  //  cell.smoothField.isUserInteractionEnabled = true
                    
                    
                    functionEdit.text = "ENTER"
                    functionEdit.backgroundColor = UIColor.white
                    
                    cell.functionView.layer.borderWidth = 0.0
                    cell.functionView.layer.borderColor = UIColor.clear.cgColor
                    cell.functionField.isUserInteractionEnabled = false
                    
                    
                    horseNameEdit.text = "ENTER"
                    horseNameEdit.backgroundColor = UIColor.white
                    
                    cell.nameView.layer.borderWidth = 0.0
                    cell.nameView.layer.borderColor = UIColor.clear.cgColor
                    cell.nameField.isUserInteractionEnabled = false
                    
                    
                    horseNameEdit.text = "ENTER"
                    horseNameEdit.backgroundColor = UIColor.white
                    
                    cell.threadView.layer.borderWidth = 0.0
                    cell.threadView.layer.borderColor = UIColor.clear.cgColor
                    cell.threadField.isUserInteractionEnabled = false
                    
                    
                    terminoEdit.text = "ENTER"
                    terminoEdit.backgroundColor = UIColor.white
                    
                    cell.terminoView.layer.borderWidth = 0.0
                    cell.terminoView.layer.borderColor = UIColor.clear.cgColor
                    cell.terminoField.isUserInteractionEnabled = false
                    
                    
                }
                        
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Error", message: "Please Complete Entry Column First", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
                else
                {
                    let alert = UIAlertController(title: "Error", message: "Please Enter Entry Number", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                
            }
            else if (smoothEdit.text == "DONE")
            {
                
//                if (self.mainScroll.contentOffset.y > 0)
//                {
//                    UIView.animate(withDuration: 1.5, delay: 0, options: UIViewAnimationOptions.transitionCurlUp, animations: {
//                        self.mainScroll.contentOffset.y = 0
//                    }, completion: nil)
//
//                }
//
//
                    placeValueArray.removeAllObjects()
                    serialNoArray.removeAllObjects()
//
//                smoothEdit.text = "ENTER"
//                smoothEdit.backgroundColor = UIColor.white
                
                for index in 0...9
                {
                    let indexPath = NSIndexPath.init(row: index, section: 0)
                    
                    let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
//                    cell.smoothView.layer.borderWidth = 1.0
//                    cell.smoothView.layer.borderColor = UIColor.clear.cgColor
//                    cell.smoothField.isUserInteractionEnabled = false
                    
                    if (cell.entryField.text != "" && cell.smoothField.text != "")
                    {
                        placeValueArray.add(cell.smoothField.text as! String)
                        serialNoArray.add(String(index + 1))
                    }
                    
                }
                

                
                
                var totalRepeat = 0
                for place in 1...10
                {
                    var repeatCount = 0
                    for indexCount in 0...9
                    {
                        let indexPath = NSIndexPath.init(row: indexCount, section: 0)
                        
                        let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                        
                        print("Place:",String(place),cell.functionField.text as! String)
                        
                        if cell.smoothField.text as! String == String(place)
                        {
                            repeatCount = repeatCount + 1
                            
                            if repeatCount > 1
                            {
                                totalRepeat = totalRepeat + 1
                            }
                        }
                    }
                }
                
                if totalRepeat > 0
                {
                    let alert = UIAlertController(title: "Error", message: "You can't repeat the Rating", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                
                   
                    
                    if (placeValueArray.count > 0 && judgesCardList.count == placeValueArray.count)
                    {
                        
                        editTag = 0
                        if (self.mainScroll.contentOffset.y > 0)                {
                            UIView.animate(withDuration: 1.5, delay: 0, options: UIViewAnimationOptions.transitionCurlUp, animations: {
                                self.mainScroll.contentOffset.y = 0                    }, completion: nil)
                            
                        }
                        
                        smoothEdit.text = "ENTER"
                        
                        smoothEdit.backgroundColor = UIColor.white
                        
                        for index in 0...9
                        {
                            let indexPath = NSIndexPath.init(row: index, section: 0)
                            
                            let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                            cell.smoothView.layer.borderWidth = 1.0
                            cell.smoothView.layer.borderColor = UIColor.clear.cgColor
                            cell.smoothField.isUserInteractionEnabled = false
                            
                        }
                        
                        
                        CategoryAddEdit(categoryId: category2Id, placeValue : placeValueArray, editTag: sender.tag)
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Error", message: "Please Place All Entries", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    
                    
                }
                    
                
            }

                
            }
            else
            {
                let alert = UIAlertController(title: "Error", message: "Please Complete Your Edit", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
                
        }
        else if sender.tag == 4
        {
            if (horseNameEdit.text == "ENTER" && functionEdit.text == "ENTER" && smoothEdit.text == "ENTER" && terminoEdit.text == "ENTER")
            {
            
                placeNumberView.isHidden = false
            if threadEdit.text == "ENTER"
            {
               editTag = sender.tag
                
                let indexPath = NSIndexPath.init(row: 0, section: 0)
                
                let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                
                if (cell.entryField.text != "")
                {
                    
                    // To make the number hidden
                    invisibleCount = 0
                    
                    self.number1ImgVw.isHidden = false
                    self.numberImg2Vw.isHidden = false
                    self.number3ImgVw.isHidden = false
                    self.number4ImgVw.isHidden = false
                    self.number5ImgVw.isHidden = false
                    self.number6ImgVw.isHidden = false
                    self.number7ImgVw.isHidden = false
                    self.number8ImgVw.isHidden = false
                    self.number9ImgVw.isHidden = false
                    self.number10ImgVw.isHidden = false
                    
                    
                    for placeCheck in 0...9
                    {
                        let indexPath = NSIndexPath.init(row: placeCheck, section: 0)
                        
                        let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                        
                        if (cell.threadField.text == "1")
                        {
                            invisibleCount += 1
                            self.number1ImgVw.isHidden = true
                        }
                        else if (cell.threadField.text == "2")
                        {
                            invisibleCount += 1
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (cell.threadField.text == "3")
                        {
                            invisibleCount += 1
                            self.number3ImgVw.isHidden = true
                        }
                        else if (cell.threadField.text == "4")
                        {
                            invisibleCount += 1
                            self.number4ImgVw.isHidden = true
                        }
                        else if (cell.threadField.text == "5")
                        {
                            invisibleCount += 1
                            self.number5ImgVw.isHidden = true
                        }
                        else if (cell.threadField.text == "6")
                        {
                            invisibleCount += 1
                            self.number6ImgVw.isHidden = true
                        }
                        else if (cell.threadField.text == "7")
                        {
                            invisibleCount += 1
                            self.number7ImgVw.isHidden = true
                        }
                        else if (cell.threadField.text == "8")
                        {
                            invisibleCount += 1
                            self.number8ImgVw.isHidden = true
                        }
                        else if (cell.threadField.text == "9")
                        {
                            invisibleCount += 1
                            self.number9ImgVw.isHidden = true
                        }
                        else if (cell.threadField.text == "10")
                        {
                            invisibleCount += 1
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    
                    if (self.mainScroll.contentOffset.y == 0)
                    {
                        UIView.animate(withDuration: 1.5, delay: 0, options: UIViewAnimationOptions.transitionCurlUp, animations: {
                            self.mainScroll.contentOffset.y = 70
                        }, completion: nil)
                        
                    }
                    
                    if (horseNameEdit.text == "ENTER")
                    {
//                        if(invisibleCount == 10)
//                        {
//                            threadEdit.text = "CLEAR"
//                            threadEdit.backgroundColor = UIColor.red
//                        }
//                        else
//                        {
                            threadEdit.text = "DONE"
                            threadEdit.backgroundColor = UIColor.orange
//                        }
                
                for index in 0...9
                {
                    let indexPath = NSIndexPath.init(row: index, section: 0)
                    
                    let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                    cell.threadView.layer.borderWidth = 1.0
                    cell.threadView.layer.borderColor = UIColor.darkGray.cgColor
                  //  cell.threadField.isUserInteractionEnabled = true
                    
                    
                    functionEdit.text = "ENTER"
                    functionEdit.backgroundColor = UIColor.white
                    
                    cell.functionView.layer.borderWidth = 0.0
                    cell.functionView.layer.borderColor = UIColor.clear.cgColor
                    cell.functionField.isUserInteractionEnabled = false
                    
                    
                    smoothEdit.text = "ENTER"
                    smoothEdit.backgroundColor = UIColor.white
                    
                    
                    cell.smoothView.layer.borderWidth = 0.0
                    cell.smoothView.layer.borderColor = UIColor.clear.cgColor
                    cell.smoothField.isUserInteractionEnabled = false
                    
                    
                    horseNameEdit.text = "ENTER"
                    horseNameEdit.backgroundColor = UIColor.white
                    
                    cell.nameView.layer.borderWidth = 0.0
                    cell.nameView.layer.borderColor = UIColor.clear.cgColor
                    cell.nameField.isUserInteractionEnabled = false
                    
                    
                    terminoEdit.text = "ENTER"
                    terminoEdit.backgroundColor = UIColor.white
                    
                    cell.terminoView.layer.borderWidth = 0.0
                    cell.terminoView.layer.borderColor = UIColor.clear.cgColor
                    cell.terminoField.isUserInteractionEnabled = false
                    
                }
                        
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Error", message: "Please Complete Entry Column First", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
                else
                {
                    let alert = UIAlertController(title: "Error", message: "Please Enter Entry Number", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                
            }
            else if(threadEdit.text == "DONE")
            {
                
                //                if (self.mainScroll.contentOffset.y > 0)
//                {
//                    UIView.animate(withDuration: 1.5, delay: 0, options: UIViewAnimationOptions.transitionCurlUp, animations: {
//                        self.mainScroll.contentOffset.y = 0
//                    }, completion: nil)
//
//                }
//
//
                placeValueArray.removeAllObjects()
                serialNoArray.removeAllObjects()
//
//                threadEdit.text = "ENTER"
//                threadEdit.backgroundColor = UIColor.white
                
                for index in 0...9
                {
                    let indexPath = NSIndexPath.init(row: index, section: 0)
                    
                    let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
//                    cell.threadView.layer.borderWidth = 1.0
//                    cell.threadView.layer.borderColor = UIColor.clear.cgColor
//                    cell.threadField.isUserInteractionEnabled = false
                    
                    
                    if (cell.entryField.text != "" && cell.threadField.text != "")
                    {
                        placeValueArray.add(cell.threadField.text as! String)
                        serialNoArray.add(String(index + 1))
                    }
                    
                }
                

                
                
                var totalRepeat = 0
                for place in 1...10
                {
                    var repeatCount = 0
                    for indexCount in 0...9
                    {
                        let indexPath = NSIndexPath.init(row: indexCount, section: 0)
                        
                        let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                        
                        print("Place:",String(place),cell.functionField.text as! String)
                        
                        if cell.threadField.text as! String == String(place)
                        {
                            repeatCount = repeatCount + 1
                            
                            if repeatCount > 1
                            {
                                totalRepeat = totalRepeat + 1
                            }
                        }
                    }
                }
                
                if totalRepeat > 0
                {
                    let alert = UIAlertController(title: "Error", message: "You can't repeat the Rating", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                

                    
                    if (placeValueArray.count > 0 && judgesCardList.count == placeValueArray.count)
                    {
                        editTag = 0

                        
                        if (self.mainScroll.contentOffset.y > 0)                {
                            UIView.animate(withDuration: 1.5, delay: 0, options: UIViewAnimationOptions.transitionCurlUp, animations: {
                                self.mainScroll.contentOffset.y = 0                    }, completion: nil)
                            
                        }
                        
                        threadEdit.text = "ENTER"
                        
                    threadEdit.backgroundColor = UIColor.white
                        
                        
                        for index in 0...9
                        {
                            let indexPath = NSIndexPath.init(row: index, section: 0)
                            
                            let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                            cell.threadView.layer.borderWidth = 0.0
                            cell.threadView.layer.borderColor = UIColor.clear.cgColor
                            cell.threadField.isUserInteractionEnabled = false
                            
                        }
                        
                        
                        CategoryAddEdit(categoryId: category3Id, placeValue : placeValueArray, editTag: sender.tag)
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Error", message: "Please Place All Entries", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    
                    
                    
                }
                    
            }

            
            }
            else
            {
                let alert = UIAlertController(title: "Error", message: "Please Complete Your Edit", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
        }
        else if sender.tag == 5
        {
            if (horseNameEdit.text == "ENTER" && functionEdit.text == "ENTER" && smoothEdit.text == "ENTER" && threadEdit.text == "ENTER")
            {
                placeNumberView.isHidden = false
            
            if terminoEdit.text == "ENTER"
            {
                
                editTag = sender.tag
                
                let indexPath = NSIndexPath.init(row: 0, section: 0)
                
                let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                
                if (cell.entryField.text != "")
                {
                    
                    // To make the number hidden
                    invisibleCount = 0
                    self.number1ImgVw.isHidden = false
                    self.numberImg2Vw.isHidden = false
                    self.number3ImgVw.isHidden = false
                    self.number4ImgVw.isHidden = false
                    self.number5ImgVw.isHidden = false
                    self.number6ImgVw.isHidden = false
                    self.number7ImgVw.isHidden = false
                    self.number8ImgVw.isHidden = false
                    self.number9ImgVw.isHidden = false
                    self.number10ImgVw.isHidden = false
                    
                    
                    for placeCheck in 0...9
                    {
                        let indexPath = NSIndexPath.init(row: placeCheck, section: 0)
                        
                        let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                        
                        if (cell.terminoField.text == "1")
                        {
                            invisibleCount += 1
                            self.number1ImgVw.isHidden = true
                        }
                        else if (cell.terminoField.text == "2")
                        {
                            invisibleCount += 1
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (cell.terminoField.text == "3")
                        {
                            invisibleCount += 1
                            self.number3ImgVw.isHidden = true
                        }
                        else if (cell.terminoField.text == "4")
                        {
                            invisibleCount += 1
                            self.number4ImgVw.isHidden = true
                        }
                        else if (cell.terminoField.text == "5")
                        {
                            invisibleCount += 1
                            self.number5ImgVw.isHidden = true
                        }
                        else if (cell.terminoField.text == "6")
                        {
                            invisibleCount += 1
                            self.number6ImgVw.isHidden = true
                        }
                        else if (cell.terminoField.text == "7")
                        {
                            invisibleCount += 1
                            self.number7ImgVw.isHidden = true
                        }
                        else if (cell.terminoField.text == "8")
                        {
                            invisibleCount += 1
                            self.number8ImgVw.isHidden = true
                        }
                        else if (cell.terminoField.text == "9")
                        {
                            invisibleCount += 1
                            self.number9ImgVw.isHidden = true
                        }
                        else if (cell.terminoField.text == "10")
                        {
                            invisibleCount += 1
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    
                    if (self.mainScroll.contentOffset.y == 0)
                    {
                        UIView.animate(withDuration: 1.5, delay: 0, options: UIViewAnimationOptions.transitionCurlUp, animations: {
                            self.mainScroll.contentOffset.y = 70
                        }, completion: nil)
                        
                    }
                    
                    if (horseNameEdit.text == "ENTER")
                    {
                        
//                        if (invisibleCount == 10)
//                        {
//                            terminoEdit.text = "CLEAR"
//                            terminoEdit.backgroundColor = UIColor.red
//                        }
//                        else
//                        {
                            terminoEdit.text = "DONE"
                            terminoEdit.backgroundColor = UIColor.orange
//                        }
                
                for index in 0...9
                {
                    let indexPath = NSIndexPath.init(row: index, section: 0)
                    
                    let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                    cell.terminoView.layer.borderWidth = 1.0
                    cell.terminoView.layer.borderColor = UIColor.darkGray.cgColor
                   // cell.terminoField.isUserInteractionEnabled = true
                    
                    
                    functionEdit.text = "ENTER"
                    functionEdit.backgroundColor = UIColor.white
                    
                    cell.functionView.layer.borderWidth = 0.0
                    cell.functionView.layer.borderColor = UIColor.clear.cgColor
                    cell.functionField.isUserInteractionEnabled = false
                    
                    smoothEdit.text = "ENTER"
                    smoothEdit.backgroundColor = UIColor.white
                    
                    cell.smoothView.layer.borderWidth = 0.0
                    cell.smoothView.layer.borderColor = UIColor.clear.cgColor
                    cell.smoothField.isUserInteractionEnabled = false
                    
                    
                    threadEdit.text = "ENTER"
                    threadEdit.backgroundColor = UIColor.white
                    
                    cell.threadView.layer.borderWidth = 0.0
                    cell.threadView.layer.borderColor = UIColor.clear.cgColor
                    cell.threadField.isUserInteractionEnabled = false
                    
                    horseNameEdit.text = "ENTER"
                    horseNameEdit.backgroundColor = UIColor.white
                    
                    cell.nameView.layer.borderWidth = 0.0
                    cell.nameView.layer.borderColor = UIColor.clear.cgColor
                    cell.nameField.isUserInteractionEnabled = false
                    
                }
                        
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Error", message: "Please Complete Entry Column First", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
                else
                {
                    let alert = UIAlertController(title: "Error", message: "Please Enter Entry Number", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                
            }
            else if terminoEdit.text == "DONE"
            {
                
//                if (self.mainScroll.contentOffset.y > 0)
//                {
//                    UIView.animate(withDuration: 1.5, delay: 0, options: UIViewAnimationOptions.transitionCurlUp, animations: {
//                        self.mainScroll.contentOffset.y = 0
//                    }, completion: nil)
//
//                }
//
//
                placeValueArray.removeAllObjects()
                serialNoArray.removeAllObjects()
//
//                terminoEdit.text = "ENTER"
//                terminoEdit.backgroundColor = UIColor.white
                
                for index in 0...9
                {
                    let indexPath = NSIndexPath.init(row: index, section: 0)
                    
                    let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
//                    cell.terminoView.layer.borderWidth = 1.0
//                    cell.terminoView.layer.borderColor = UIColor.clear.cgColor
//                    cell.terminoField.isUserInteractionEnabled = false
                    
                    if (cell.entryField.text != "" && cell.terminoField.text != "")
                    {
                        placeValueArray.add(cell.terminoField.text as! String)
                        serialNoArray.add(String(index + 1))
                    }
                    
                }
                
                

                
                
                var totalRepeat = 0
                for place in 1...10
                {
                    var repeatCount = 0
                    for indexCount in 0...9
                    {
                        let indexPath = NSIndexPath.init(row: indexCount, section: 0)
                        
                        let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                        
                        print("Place:",String(place),cell.functionField.text as! String)
                        
                        if cell.terminoField.text as! String == String(place)
                        {
                            repeatCount = repeatCount + 1
                            
                            if repeatCount > 1
                            {
                                totalRepeat = totalRepeat + 1
                            }
                        }
                    }
                }
                
                if totalRepeat > 0
                {
                    let alert = UIAlertController(title: "Error", message: "You can't repeat the Rating", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                

                    
                    if (placeValueArray.count > 0 && judgesCardList.count == placeValueArray.count)
                    {
                        
                        editTag = 0
                        
                        if (self.mainScroll.contentOffset.y > 0)                {
                            UIView.animate(withDuration: 1.5, delay: 0, options: UIViewAnimationOptions.transitionCurlUp, animations: {
                                self.mainScroll.contentOffset.y = 0                    }, completion: nil)
                            
                        }
                        
                        terminoEdit.text = "ENTER"
                        terminoEdit.backgroundColor = UIColor.white
                        
                        for index in 0...9
                        {
                            let indexPath = NSIndexPath.init(row: index, section: 0)
                            
                            let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                            cell.terminoView.layer.borderWidth = 0.0
                            cell.terminoView.layer.borderColor = UIColor.clear.cgColor
                            cell.terminoField.isUserInteractionEnabled = false
                            
                        }
                        
                        
                        
                        CategoryAddEdit(categoryId: category4Id, placeValue : placeValueArray, editTag: sender.tag)
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Error", message: "Please Place All Entries", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    
                    
                }
                    
                
            }

            
            
            }
            else
            {
                let alert = UIAlertController(title: "Error", message: "Please Complete Your Edit", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
        }
        else if sender.tag == 6
        {
            if (horseNameEdit.text == "ENTER" && functionEdit.text == "ENTER" && smoothEdit.text == "ENTER" && threadEdit.text == "ENTER" && terminoEdit.text == "ENTER")
            {
            
            if (ShowId != nil && classId != nil)
            {
                var filledCellCount = 0, filledFunction = 0, filledSmooth = 0, filledThread = 0, filledTermino = 0
                
                for index in 0...9
                {
                    let indexPath = NSIndexPath.init(row: index, section: 0)
                    
                    let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                   
                    
//                    if (cell.entryField.text != "" && cell.functionField.text != "" && cell.smoothField.text != "" && cell.threadField.text != "" && cell.terminoField.text != "")
//                    {
//                       filledCellCount = filledCellCount + 1
//                    }
                    
                    
                    
                    if (cell.entryField.text != "")
                    {
                        filledCellCount = filledCellCount + 1
                        if (category1Not && cell.functionField.text != "")
                        {
                            filledFunction = filledFunction + 1
                        }
                        
                        if (category2Not && cell.smoothField.text != "")
                        {
                            filledSmooth = filledSmooth + 1
                        }
                        
                        if (category3Not && cell.threadField.text != "")
                        {
                            filledThread = filledThread + 1
                        }
                        
                        if (category4Not && cell.terminoField.text != "")
                        {
                            filledTermino = filledTermino + 1
                        }
                        
                        
                    }
                    
                    
                    
//                    if (cell.entryField.text != "")
//                    {
//                        if (category1Not && category2Not && category3Not && category4Not)
//                        {
//                            filledCellCount = filledCellCount + 1
//                        }
//                        else if ((category1Not && category2Not && category3Not) || (category1Not && category3Not && category4Not) || (category1Not && category2Not && category4Not) || (category2Not && category3Not && category4Not) || (category2Not && category3Not && category4Not) || (category1Not && category3Not && category4Not))
//                        {
//                            filledCellCount = filledCellCount + 1
//                        }
//                        else if ((category3Not && category4Not) ||  (category1Not && category4Not) || (category1Not && category2Not) || (category1Not && category3Not) || (category2Not && category3Not) || (category2Not && category4Not))
//                        {
//                            filledCellCount = filledCellCount + 1
//                        }
//                        else if (category1Not || category2Not || category3Not || category4Not)
//                        {
//                            filledCellCount = filledCellCount + 1
//                        }
//                    }
                    
                    
                    
                    
                }
                
                
                if (category1Not && category2Not && category3Not && category4Not)
                {
                    if (filledFunction == filledCellCount && filledSmooth == filledCellCount && filledThread == filledCellCount && filledTermino == filledCellCount)
                    {
                        self.calculateTotal()
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Error", message: "Please Enter All Information.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else  if (category1Not && category2Not && category3Not)
                {
                    if (filledFunction == filledCellCount && filledSmooth == filledCellCount && filledThread == filledCellCount)
                    {
                        self.calculateTotal()
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Error", message: "Please Enter All Information.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else  if (category1Not && category3Not && category4Not)
                {
                    if (filledFunction == filledCellCount && filledTermino == filledCellCount && filledThread == filledCellCount)
                    {
                        self.calculateTotal()
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Error", message: "Please Enter All Information.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else  if (category1Not && category2Not && category4Not)
                {
                    if (filledFunction == filledCellCount && filledSmooth == filledCellCount && filledTermino == filledCellCount)
                    {
                        self.calculateTotal()
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Error", message: "Please Enter All Information.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else  if (category4Not && category2Not && category3Not)
                {
                    if (filledTermino == filledCellCount && filledSmooth == filledCellCount && filledThread == filledCellCount)
                    {
                        self.calculateTotal()
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Error", message: "Please Enter All Information.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else  if (category1Not)
                {
                    if (filledFunction == filledCellCount)
                    {
                        self.calculateTotal()
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Error", message: "Please Enter All Information.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else  if (category2Not)
                {
                    if (filledSmooth == filledCellCount)
                    {
                        self.calculateTotal()
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Error", message: "Please Enter All Information.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else  if (category3Not)
                {
                    if (filledThread == filledCellCount)
                    {
                        self.calculateTotal()
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Error", message: "Please Enter All Information.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else  if (category4Not)
                {
                    if (filledTermino == filledCellCount)
                    {
                        self.calculateTotal()
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Error", message: "Please Enter All Information.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
                
                
                
//                if filledCellCount == 10
//                {
//                    self.calculateTotal()
//                }
//                else
//                {
//                    let alert = UIAlertController(title: "Error", message: "Please Enter All Information.", preferredStyle: UIAlertControllerStyle.alert)
//                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//                    self.present(alert, animated: true, completion: nil)
//                }
            }
                
            }
            else
            {
                let alert = UIAlertController(title: "Error", message: "Please Complete Your Edit", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        else if sender.tag == 7
        {
            
            if (isCalculate)
            {
            
            if (horseNameEdit.text == "ENTER" && functionEdit.text == "ENTER" && smoothEdit.text == "ENTER" && threadEdit.text == "ENTER" && terminoEdit.text == "ENTER")
            {
            placeNumberView.isHidden = false
            
            if saveLbl.text == "ENTER"
            {
                editTag = sender.tag
//                saveLbl.text = "SAVE"
//                saveLbl.backgroundColor = UIColor (red: (64.0/255.0), green: (255.0/255.0), blue: (237.0/255.0), alpha: 1.0)
                
            let indexPath = NSIndexPath.init(row: 0, section: 0)
            
            let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
            
            if (cell.entryField.text != "")
            {
                if (horseNameEdit.text == "ENTER" && functionEdit.text == "ENTER" && smoothEdit.text == "ENTER" && threadEdit.text == "ENTER" && terminoEdit.text == "ENTER")
                {
                    // Alert before SAVE
                    
                    let alert = UIAlertController(title: nil, message: "Do you agree with PLACINGS?", preferredStyle: UIAlertControllerStyle.alert)
                    let yesAction = UIAlertAction.init(title: "Yes", style: UIAlertActionStyle.default, handler: { _ in
                        
                        self.saveLbl.text = "SAVE"
                        self.saveLbl.backgroundColor = UIColor (red: (64.0/255.0), green: (255.0/255.0), blue: (237.0/255.0), alpha: 1.0)
                        
                        for placeRank in 0...9
                        {
                            let indexPath = NSIndexPath.init(row: placeRank, section: 0)
                            
                            let cell:DataListCell = self.judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                            
                            
                            if(cell.entryField.text != "")
                            {
                                cell.jusgePlaceField.text = String(placeRank + 1)
                            }
                            
                        }
                        
                    })
                    let noAction = UIAlertAction.init(title: "No", style: UIAlertActionStyle.default, handler: { _ in
                        
                        self.saveLbl.text = "SAVE"
                        self.saveLbl.backgroundColor = UIColor (red: (64.0/255.0), green: (255.0/255.0), blue: (237.0/255.0), alpha: 1.0)
                        
                        // To make the number hidden
                        self.invisibleCount = 0
                        self.number1ImgVw.isHidden = false
                        self.numberImg2Vw.isHidden = false
                        self.number3ImgVw.isHidden = false
                        self.number4ImgVw.isHidden = false
                        self.number5ImgVw.isHidden = false
                        self.number6ImgVw.isHidden = false
                        self.number7ImgVw.isHidden = false
                        self.number8ImgVw.isHidden = false
                        self.number9ImgVw.isHidden = false
                        self.number10ImgVw.isHidden = false
                        
                        
                        for placeCheck in 0...9
                        {
                            let indexPath = NSIndexPath.init(row: placeCheck, section: 0)
                            
                            let cell:DataListCell = self.judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                            
                            if (cell.jusgePlaceField.text == "1")
                            {
                                self.invisibleCount += 1
                                self.number1ImgVw.isHidden = true
                            }
                            else if (cell.jusgePlaceField.text == "2")
                            {
                                self.invisibleCount += 1
                                self.numberImg2Vw.isHidden = true
                            }
                            else if (cell.jusgePlaceField.text == "3")
                            {
                                self.invisibleCount += 1
                                self.number3ImgVw.isHidden = true
                            }
                            else if (cell.jusgePlaceField.text == "4")
                            {
                                self.invisibleCount += 1
                                self.number4ImgVw.isHidden = true
                            }
                            else if (cell.jusgePlaceField.text == "5")
                            {
                                self.invisibleCount += 1
                                self.number5ImgVw.isHidden = true
                            }
                            else if (cell.jusgePlaceField.text == "6")
                            {
                                self.invisibleCount += 1
                                self.number6ImgVw.isHidden = true
                            }
                            else if (cell.jusgePlaceField.text == "7")
                            {
                                self.invisibleCount += 1
                                self.number7ImgVw.isHidden = true
                            }
                            else if (cell.jusgePlaceField.text == "8")
                            {
                                self.invisibleCount += 1
                                self.number8ImgVw.isHidden = true
                            }
                            else if (cell.jusgePlaceField.text == "9")
                            {
                                self.invisibleCount += 1
                                self.number9ImgVw.isHidden = true
                            }
                            else if (cell.jusgePlaceField.text == "10")
                            {
                                self.invisibleCount += 1
                                self.number10ImgVw.isHidden = true
                            }
                            
                        }
                        
                        
                        
                        //                    if(invisibleCount == 10)
                        //                    {
                        //                        saveLbl.text = "CLEAR"
                        //                        saveLbl.backgroundColor = UIColor.red
                        //                    }
                        
                        
                        
                        if (self.mainScroll.contentOffset.y == 0)
                        {
                            self.saveEdit = false
                            self.editTag = sender.tag
                            UIView.animate(withDuration: 1.5, delay: 0, options: UIViewAnimationOptions.transitionCurlUp, animations: {
                                self.mainScroll.contentOffset.y = 70
                            }, completion: nil)
                            
                        }

                        
                    })
                    alert.addAction(noAction)
                    alert.addAction(yesAction)
                    self.present(alert, animated: true, completion: nil)
                    
                   ////////////////////////////////////////////////
                    
                    
               
                
                
                }
                else
                {
                    let alert = UIAlertController(title: "Error", message: "Please Complete Your Edit", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                
                
            }
            else
            {
                let alert = UIAlertController(title: "Error", message: "Please Enter Entry Number", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
            
            }
            else if saveLbl.text == "SAVE"
            {
                editTag = 0
                saveLbl.text = "ENTER"
                saveLbl.backgroundColor = UIColor.white
                
                let indexPath = NSIndexPath.init(row: 0, section: 0)
                
                let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                
                    
                     if (self.mainScroll.contentOffset.y > 0)
                        {
                            editTag = 0
                          //  saveEdit = true
                            UIView.animate(withDuration: 1.5, delay: 0, options: UIViewAnimationOptions.transitionCurlUp, animations: {
                                self.mainScroll.contentOffset.y = 0
                            }, completion: nil)
                            
                        }
                        
                        
//                        if (saveEdit)
//                        {
                            judgePlaceArray.removeAllObjects()
                            serialNoArray.removeAllObjects()
                            
                            for index in 0...9
                            {
                                let indexPath = NSIndexPath.init(row: index, section: 0)
                                
                                let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                                //                cell.terminoView.layer.borderWidth = 1.0
                                //                cell.terminoView.layer.borderColor = UIColor.clear.cgColor
                                //                cell.terminoField.isUserInteractionEnabled = false
                                
                                if (cell.entryField.text != "" && cell.jusgePlaceField.text != "")
                                {
                                    judgePlaceArray.add(cell.jusgePlaceField.text as! String)
                                    serialNoArray.add(String(index + 1))
                                }
                                
                            }
                            
                            

                            
                            if (judgePlaceArray.count > 0 && judgesCardList.count == judgePlaceArray.count)
                            {
                                JudgesPlaceAddEdit(categoryId: category4Id, placeValue : judgePlaceArray)
                            }
                            else
                            {
                                let alert = UIAlertController(title: "Error", message: "Please Place All Entries", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
 
                }

                
            }
            else
            {
                let alert = UIAlertController(title: "Error", message: "Please Complete Your Edit", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
                
            }
            else
            {
                    let alert = UIAlertController(title: "Error", message: "Please Complete The Calculation First", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
            }
            
        }
        
    }
        
    }
    
    
    // MARK: - UIDropInteraction Delegates

    
    @available(iOS 11.0, *)
    func customEnableDropping(on view: UIView, dropInteractionDelegate: UIDropInteractionDelegate) {
        if #available(iOS 11.0, *) {
            let dropInteraction = UIDropInteraction(delegate: dropInteractionDelegate)
            view.addInteraction(dropInteraction)
            
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    @available(iOS 11.0, *)
    func dropInteraction(_ interaction: UIDropInteraction, canHandle session: UIDropSession) -> Bool {
        // Ensure the drop session has an object of the appropriate type
        return true  //session.canLoadObjects(ofClass: UIImage.self)
    }
    
    @available(iOS 11.0, *)
    func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
        // Propose to the system to copy the item from the source app
        return UIDropProposal(operation: .copy)
    }
    

    @available(iOS 11.0, *)
    func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
        // Consume drag items (in this example, of type UIImage).
//        session.loadObjects(ofClass: UIImage.self) { imageItems in
//            let images = imageItems as! [UIImage]
//            self.dropDownImgVw.image = images.first
//        }
        
        
        
        let touchPoint = session.location(in: judgeCardTable)
        
        let cellHeight = CGFloat( judgeCardTable.frame.size.height/10)
        
        if (touchPoint.y < cellHeight*1)
        {
            let indexPath = NSIndexPath.init(row: 0, section: 0)
            let cell = self.judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
            

            
            if self.editTag == 2
            {
                if (cell.entryField.text != "" && category1Not)
                {
                    invisibleCount += 1
                    
                    if (cell.functionField.text == "")
                    {
                        cell.functionView.backgroundColor = UIColor.white
                        cell.functionField.text = "\(self.draggedImageTag)"
                    if (self.draggedImageTag == 1)
                    {
                        self.number1ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 2)
                    {
                        self.numberImg2Vw.isHidden = true
                    }
                    else if (self.draggedImageTag == 3)
                    {
                        self.number3ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 4)
                    {
                        self.number4ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 5)
                    {
                        self.number5ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 6)
                    {
                        self.number6ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 7)
                    {
                        self.number7ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 8)
                    {
                        self.number8ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 9)
                    {
                        self.number9ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 10)
                    {
                        self.number10ImgVw.isHidden = true
                    }
                    
                }
                else
                    {
                        
                        
                        if (cell.functionField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.functionField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.functionView.backgroundColor = UIColor.white
                        cell.functionField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
    
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 3
            {
                if (cell.entryField.text != "" && category2Not)
                {
                    invisibleCount += 1

                    
                    if (cell.smoothField.text == "")
                    {
                    
                    cell.smoothView.backgroundColor = UIColor.white
                    cell.smoothField.text = "\(self.draggedImageTag)"
                    
                    if (self.draggedImageTag == 1)
                    {
                        self.number1ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 2)
                    {
                        self.numberImg2Vw.isHidden = true
                    }
                    else if (self.draggedImageTag == 3)
                    {
                        self.number3ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 4)
                    {
                        self.number4ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 5)
                    {
                        self.number5ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 6)
                    {
                        self.number6ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 7)
                    {
                        self.number7ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 8)
                    {
                        self.number8ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 9)
                    {
                        self.number9ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 10)
                    {
                        self.number10ImgVw.isHidden = true
                    }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.smoothField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.smoothField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.smoothView.backgroundColor = UIColor.white
                        cell.smoothField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 4
            {
                if (cell.entryField.text != "" && category3Not)
                {
                    invisibleCount += 1

                    if (cell.threadField.text == "")
                    {
                    
                    cell.threadView.backgroundColor = UIColor.white
                    cell.threadField.text = "\(self.draggedImageTag)"
                    
                    if (self.draggedImageTag == 1)
                    {
                        self.number1ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 2)
                    {
                        self.numberImg2Vw.isHidden = true
                    }
                    else if (self.draggedImageTag == 3)
                    {
                        self.number3ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 4)
                    {
                        self.number4ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 5)
                    {
                        self.number5ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 6)
                    {
                        self.number6ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 7)
                    {
                        self.number7ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 8)
                    {
                        self.number8ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 9)
                    {
                        self.number9ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 10)
                    {
                        self.number10ImgVw.isHidden = true
                    }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.threadField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.threadField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.threadView.backgroundColor = UIColor.white
                        cell.threadField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 5
            {
                if (cell.entryField.text != "" && category4Not)
                {
                    invisibleCount += 1

                    if (cell.terminoField.text == "")
                    {
                    
                    cell.terminoView.backgroundColor = UIColor.white
                    cell.terminoField.text = "\(self.draggedImageTag)"
                    
                    if (self.draggedImageTag == 1)
                    {
                        self.number1ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 2)
                    {
                        self.numberImg2Vw.isHidden = true
                    }
                    else if (self.draggedImageTag == 3)
                    {
                        self.number3ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 4)
                    {
                        self.number4ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 5)
                    {
                        self.number5ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 6)
                    {
                        self.number6ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 7)
                    {
                        self.number7ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 8)
                    {
                        self.number8ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 9)
                    {
                        self.number9ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 10)
                    {
                        self.number10ImgVw.isHidden = true
                    }
                    
                        
                    }
                    else
                    {
                        
                        
                        if (cell.terminoField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.terminoField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.terminoView.backgroundColor = UIColor.white
                        cell.terminoField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                }
            }
            else if self.editTag == 7
            {
                if (cell.entryField.text != "")
                {
                    invisibleCount += 1

                    
                    if (cell.jusgePlaceField.text == "")
                    {
                        
                    cell.judgePlaceView.backgroundColor = UIColor.white
                    cell.jusgePlaceField.text = "\(self.draggedImageTag)"
                    
                    if (self.draggedImageTag == 1)
                    {
                        self.number1ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 2)
                    {
                        self.numberImg2Vw.isHidden = true
                    }
                    else if (self.draggedImageTag == 3)
                    {
                        self.number3ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 4)
                    {
                        self.number4ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 5)
                    {
                        self.number5ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 6)
                    {
                        self.number6ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 7)
                    {
                        self.number7ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 8)
                    {
                        self.number8ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 9)
                    {
                        self.number9ImgVw.isHidden = true
                    }
                    else if (self.draggedImageTag == 10)
                    {
                        self.number10ImgVw.isHidden = true
                    }
                        
                        
                    }
                    else
                    {
                        
                        
                        if (cell.jusgePlaceField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.judgePlaceView.backgroundColor = UIColor.white
                        cell.jusgePlaceField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            
        }
        else if (touchPoint.y < cellHeight*2)
        {
            let indexPath = NSIndexPath.init(row: 1, section: 0)
            let cell = self.judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
            
            
            
            if self.editTag == 2
            {
                if (cell.entryField.text != "" && category1Not)
                {
                    invisibleCount += 1

                    
                    if (cell.functionField.text == "")
                    {
                        cell.functionView.backgroundColor = UIColor.white
                        cell.functionField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.functionField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.functionField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.functionView.backgroundColor = UIColor.white
                        cell.functionField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 3
            {
                if (cell.entryField.text != "" && category2Not)
                {
                    invisibleCount += 1

                    
                    if (cell.smoothField.text == "")
                    {
                        
                        cell.smoothView.backgroundColor = UIColor.white
                        cell.smoothField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.smoothField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.smoothField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.smoothView.backgroundColor = UIColor.white
                        cell.smoothField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 4
            {
                if (cell.entryField.text != "" && category3Not)
                {
                    invisibleCount += 1

                    if (cell.threadField.text == "")
                    {
                        
                        cell.threadView.backgroundColor = UIColor.white
                        cell.threadField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.threadField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.threadField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.threadView.backgroundColor = UIColor.white
                        cell.threadField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 5
            {
                if (cell.entryField.text != "" && category4Not)
                {
                    invisibleCount += 1

                    if (cell.terminoField.text == "")
                    {
                        
                        cell.terminoView.backgroundColor = UIColor.white
                        cell.terminoField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        if (cell.terminoField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.terminoField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.terminoView.backgroundColor = UIColor.white
                        cell.terminoField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                }
            }
            else if self.editTag == 7
            {
                if (cell.entryField.text != "")
                {
                    invisibleCount += 1

                    if (cell.jusgePlaceField.text == "")
                    {
                        
                        cell.judgePlaceView.backgroundColor = UIColor.white
                        cell.jusgePlaceField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        if (cell.jusgePlaceField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.judgePlaceView.backgroundColor = UIColor.white
                        cell.jusgePlaceField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            
        }
        else if (touchPoint.y < cellHeight*3)
        {
            let indexPath = NSIndexPath.init(row: 2, section: 0)
            let cell = self.judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
            
            
            
            if self.editTag == 2
            {
                if (cell.entryField.text != "" && category1Not)
                {
                    invisibleCount += 1

                    if (cell.functionField.text == "")
                    {
                        cell.functionView.backgroundColor = UIColor.white
                        cell.functionField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.functionField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.functionField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.functionView.backgroundColor = UIColor.white
                        cell.functionField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 3
            {
                if (cell.entryField.text != "" && category2Not)
                {
                    invisibleCount += 1

                    if (cell.smoothField.text == "")
                    {
                        
                        cell.smoothView.backgroundColor = UIColor.white
                        cell.smoothField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.smoothField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.smoothField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.smoothView.backgroundColor = UIColor.white
                        cell.smoothField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 4
            {
                if (cell.entryField.text != "" && category3Not)
                {
                    invisibleCount += 1

                    if (cell.threadField.text == "")
                    {
                        
                        cell.threadView.backgroundColor = UIColor.white
                        cell.threadField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.threadField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.threadField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.threadView.backgroundColor = UIColor.white
                        cell.threadField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 5
            {
                if (cell.entryField.text != "" && category4Not)
                {
                    invisibleCount += 1

                    if (cell.terminoField.text == "")
                    {
                        
                        cell.terminoView.backgroundColor = UIColor.white
                        cell.terminoField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        if (cell.terminoField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.terminoField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.terminoView.backgroundColor = UIColor.white
                        cell.terminoField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                }
            }
            else if self.editTag == 7
            {
                if (cell.entryField.text != "")
                {
                    invisibleCount += 1

                    if (cell.jusgePlaceField.text == "")
                    {
                        
                        cell.judgePlaceView.backgroundColor = UIColor.white
                        cell.jusgePlaceField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        if (cell.jusgePlaceField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.judgePlaceView.backgroundColor = UIColor.white
                        cell.jusgePlaceField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            
        }
        else if (touchPoint.y < cellHeight*4)
        {
            let indexPath = NSIndexPath.init(row: 3, section: 0)
            let cell = self.judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
            
            
            
            if self.editTag == 2
            {
                if (cell.entryField.text != "" && category1Not)
                {
                    invisibleCount += 1
                    
                    if (cell.functionField.text == "")
                    {
                        cell.functionView.backgroundColor = UIColor.white
                        cell.functionField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.functionField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.functionField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.functionView.backgroundColor = UIColor.white
                        cell.functionField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 3
            {
                if (cell.entryField.text != "" && category2Not)
                {
                    invisibleCount += 1
                    
                    if (cell.smoothField.text == "")
                    {
                        
                        cell.smoothView.backgroundColor = UIColor.white
                        cell.smoothField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.smoothField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.smoothField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.smoothView.backgroundColor = UIColor.white
                        cell.smoothField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 4
            {
                if (cell.entryField.text != "" && category3Not)
                {
                    invisibleCount += 1

                    if (cell.threadField.text == "")
                    {
                        
                        cell.threadView.backgroundColor = UIColor.white
                        cell.threadField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.threadField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.threadField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.threadView.backgroundColor = UIColor.white
                        cell.threadField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 5
            {
                if (cell.entryField.text != "" && category4Not)
                {
                    invisibleCount += 1

                    if (cell.terminoField.text == "")
                    {
                        
                        cell.terminoView.backgroundColor = UIColor.white
                        cell.terminoField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        if (cell.terminoField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.terminoField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.terminoView.backgroundColor = UIColor.white
                        cell.terminoField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                }
            }
            else if self.editTag == 7
            {
                if (cell.entryField.text != "")
                {
                    invisibleCount += 1

                    if (cell.jusgePlaceField.text == "")
                    {
                        
                        cell.judgePlaceView.backgroundColor = UIColor.white
                        cell.jusgePlaceField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        if (cell.jusgePlaceField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.judgePlaceView.backgroundColor = UIColor.white
                        cell.jusgePlaceField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            
        }
        else if (touchPoint.y < cellHeight*5)
        {
            let indexPath = NSIndexPath.init(row: 4, section: 0)
            let cell = self.judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
            
            
            
            if self.editTag == 2
            {
                if (cell.entryField.text != "" && category1Not)
                {
                    invisibleCount += 1
                    
                    if (cell.functionField.text == "")
                    {
                        cell.functionView.backgroundColor = UIColor.white
                        cell.functionField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.functionField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.functionField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.functionView.backgroundColor = UIColor.white
                        cell.functionField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 3
            {
                if (cell.entryField.text != "" && category2Not)
                {
                    invisibleCount += 1

                    if (cell.smoothField.text == "")
                    {
                        
                        cell.smoothView.backgroundColor = UIColor.white
                        cell.smoothField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.smoothField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.smoothField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.smoothView.backgroundColor = UIColor.white
                        cell.smoothField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 4
            {
                if (cell.entryField.text != "" && category3Not)
                {
                    invisibleCount += 1

                    if (cell.threadField.text == "")
                    {
                        
                        cell.threadView.backgroundColor = UIColor.white
                        cell.threadField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.threadField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.threadField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.threadView.backgroundColor = UIColor.white
                        cell.threadField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 5
            {
                if (cell.entryField.text != "" && category4Not)
                {
                    invisibleCount += 1

                    if (cell.terminoField.text == "")
                    {
                        
                        cell.terminoView.backgroundColor = UIColor.white
                        cell.terminoField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        if (cell.terminoField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.terminoField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.terminoView.backgroundColor = UIColor.white
                        cell.terminoField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                }
            }
            else if self.editTag == 7
            {
                if (cell.entryField.text != "")
                {
                    invisibleCount += 1

                    if (cell.jusgePlaceField.text == "")
                    {
                        
                        cell.judgePlaceView.backgroundColor = UIColor.white
                        cell.jusgePlaceField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        if (cell.jusgePlaceField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.judgePlaceView.backgroundColor = UIColor.white
                        cell.jusgePlaceField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            
        }
        else if (touchPoint.y < cellHeight*6)
        {
            let indexPath = NSIndexPath.init(row: 5, section: 0)
            let cell = self.judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
            
            
            
            if self.editTag == 2
            {
                if (cell.entryField.text != "" && category1Not)
                {
                    invisibleCount += 1

                    if (cell.functionField.text == "")
                    {
                        cell.functionView.backgroundColor = UIColor.white
                        cell.functionField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.functionField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.functionField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.functionView.backgroundColor = UIColor.white
                        cell.functionField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 3
            {
                if (cell.entryField.text != "" && category2Not)
                {
                    invisibleCount += 1

                    if (cell.smoothField.text == "")
                    {
                        
                        cell.smoothView.backgroundColor = UIColor.white
                        cell.smoothField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.smoothField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.smoothField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.smoothView.backgroundColor = UIColor.white
                        cell.smoothField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 4
            {
                if (cell.entryField.text != "" && category3Not)
                {
                    invisibleCount += 1

                    if (cell.threadField.text == "")
                    {
                        
                        cell.threadView.backgroundColor = UIColor.white
                        cell.threadField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.threadField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.threadField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.threadView.backgroundColor = UIColor.white
                        cell.threadField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 5
            {
                if (cell.entryField.text != "" && category4Not)
                {
                    invisibleCount += 1

                    if (cell.terminoField.text == "")
                    {
                        
                        cell.terminoView.backgroundColor = UIColor.white
                        cell.terminoField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        if (cell.terminoField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.terminoField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.terminoView.backgroundColor = UIColor.white
                        cell.terminoField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                }
            }
            else if self.editTag == 7
            {
                if (cell.entryField.text != "")
                {
                    invisibleCount += 1

                    if (cell.jusgePlaceField.text == "")
                    {
                        
                        cell.judgePlaceView.backgroundColor = UIColor.white
                        cell.jusgePlaceField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        if (cell.jusgePlaceField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.judgePlaceView.backgroundColor = UIColor.white
                        cell.jusgePlaceField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            
        }
        else if (touchPoint.y < cellHeight*7)
        {
            let indexPath = NSIndexPath.init(row: 6, section: 0)
            let cell = self.judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
            
            
            
            if self.editTag == 2
            {
                if (cell.entryField.text != "" && category1Not)
                {
                    invisibleCount += 1
                    
                    if (cell.functionField.text == "")
                    {
                        cell.functionView.backgroundColor = UIColor.white
                        cell.functionField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.functionField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.functionField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.functionView.backgroundColor = UIColor.white
                        cell.functionField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 3
            {
                if (cell.entryField.text != "" && category2Not)
                {
                    invisibleCount += 1

                    if (cell.smoothField.text == "")
                    {
                        
                        cell.smoothView.backgroundColor = UIColor.white
                        cell.smoothField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.smoothField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.smoothField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.smoothView.backgroundColor = UIColor.white
                        cell.smoothField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 4
            {
                if (cell.entryField.text != "" && category3Not)
                {
                    invisibleCount += 1

                    if (cell.threadField.text == "")
                    {
                        
                        cell.threadView.backgroundColor = UIColor.white
                        cell.threadField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.threadField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.threadField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.threadView.backgroundColor = UIColor.white
                        cell.threadField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 5
            {
                if (cell.entryField.text != "" && category4Not)
                {
                    invisibleCount += 1

                    if (cell.terminoField.text == "")
                    {
                        
                        cell.terminoView.backgroundColor = UIColor.white
                        cell.terminoField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        if (cell.terminoField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.terminoField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.terminoView.backgroundColor = UIColor.white
                        cell.terminoField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                }
            }
            else if self.editTag == 7
            {
                if (cell.entryField.text != "")
                {
                    invisibleCount += 1

                    if (cell.jusgePlaceField.text == "")
                    {
                        
                        cell.judgePlaceView.backgroundColor = UIColor.white
                        cell.jusgePlaceField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        if (cell.jusgePlaceField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.judgePlaceView.backgroundColor = UIColor.white
                        cell.jusgePlaceField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            
        }
        else if (touchPoint.y < cellHeight*8)
        {
            let indexPath = NSIndexPath.init(row: 7, section: 0)
            let cell = self.judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
            
            
            
            if self.editTag == 2
            {
                if (cell.entryField.text != "" && category1Not)
                {
                    invisibleCount += 1
                    
                    if (cell.functionField.text == "")
                    {
                        cell.functionView.backgroundColor = UIColor.white
                        cell.functionField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.functionField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.functionField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.functionView.backgroundColor = UIColor.white
                        cell.functionField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 3
            {
                if (cell.entryField.text != "" && category2Not)
                {
                    invisibleCount += 1

                    if (cell.smoothField.text == "")
                    {
                        
                        cell.smoothView.backgroundColor = UIColor.white
                        cell.smoothField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.smoothField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.smoothField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.smoothView.backgroundColor = UIColor.white
                        cell.smoothField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 4
            {
                if (cell.entryField.text != "" && category3Not)
                {
                    invisibleCount += 1

                    if (cell.threadField.text == "")
                    {
                        
                        cell.threadView.backgroundColor = UIColor.white
                        cell.threadField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.threadField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.threadField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.threadView.backgroundColor = UIColor.white
                        cell.threadField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 5
            {
                if (cell.entryField.text != "" && category4Not)
                {
                    invisibleCount += 1

                    if (cell.terminoField.text == "")
                    {
                        
                        cell.terminoView.backgroundColor = UIColor.white
                        cell.terminoField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        if (cell.terminoField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.terminoField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.terminoView.backgroundColor = UIColor.white
                        cell.terminoField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                }
            }
            else if self.editTag == 7
            {
                if (cell.entryField.text != "")
                {
                    invisibleCount += 1

                    if (cell.jusgePlaceField.text == "")
                    {
                        
                        cell.judgePlaceView.backgroundColor = UIColor.white
                        cell.jusgePlaceField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        if (cell.jusgePlaceField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.judgePlaceView.backgroundColor = UIColor.white
                        cell.jusgePlaceField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            
        }
        else if (touchPoint.y < cellHeight*9)
        {
            let indexPath = NSIndexPath.init(row: 8, section: 0)
            let cell = self.judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
            
            
            
            if self.editTag == 2
            {
                if (cell.entryField.text != "" && category1Not)
                {
                    invisibleCount += 1
                    
                    if (cell.functionField.text == "")
                    {
                        cell.functionView.backgroundColor = UIColor.white
                        cell.functionField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.functionField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.functionField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.functionView.backgroundColor = UIColor.white
                        cell.functionField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 3
            {
                if (cell.entryField.text != "" && category2Not)
                {
                    invisibleCount += 1

                    if (cell.smoothField.text == "")
                    {
                        
                        cell.smoothView.backgroundColor = UIColor.white
                        cell.smoothField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.smoothField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.smoothField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.smoothView.backgroundColor = UIColor.white
                        cell.smoothField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 4
            {
                if (cell.entryField.text != "" && category3Not)
                {
                    invisibleCount += 1

                    if (cell.threadField.text == "")
                    {
                        
                        cell.threadView.backgroundColor = UIColor.white
                        cell.threadField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.threadField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.threadField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.threadView.backgroundColor = UIColor.white
                        cell.threadField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 5
            {
                if (cell.entryField.text != "" && category4Not)
                {
                    invisibleCount += 1

                    if (cell.terminoField.text == "")
                    {
                        
                        cell.terminoView.backgroundColor = UIColor.white
                        cell.terminoField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        if (cell.terminoField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.terminoField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.terminoView.backgroundColor = UIColor.white
                        cell.terminoField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                }
            }
            else if self.editTag == 7
            {
                if (cell.entryField.text != "")
                {
                    invisibleCount += 1

                    if (cell.jusgePlaceField.text == "")
                    {
                        
                        cell.judgePlaceView.backgroundColor = UIColor.white
                        cell.jusgePlaceField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        if (cell.jusgePlaceField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.judgePlaceView.backgroundColor = UIColor.white
                        cell.jusgePlaceField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            
        }
        else if (touchPoint.y < cellHeight*10)
        {
            let indexPath = NSIndexPath.init(row: 9, section: 0)
            let cell = self.judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
            
            
            
            if self.editTag == 2
            {
                if (cell.entryField.text != "" && category1Not)
                {
                    invisibleCount += 1
                    
                    if (cell.functionField.text == "")
                    {
                        cell.functionView.backgroundColor = UIColor.white
                        cell.functionField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.functionField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.functionField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.functionField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.functionView.backgroundColor = UIColor.white
                        cell.functionField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 3
            {
                if (cell.entryField.text != "" && category2Not)
                {
                    invisibleCount += 1

                    if (cell.smoothField.text == "")
                    {
                        
                        cell.smoothView.backgroundColor = UIColor.white
                        cell.smoothField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.smoothField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.smoothField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.smoothField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.smoothView.backgroundColor = UIColor.white
                        cell.smoothField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 4
            {
                if (cell.entryField.text != "" && category3Not)
                {
                    invisibleCount += 1

                    if (cell.threadField.text == "")
                    {
                        
                        cell.threadView.backgroundColor = UIColor.white
                        cell.threadField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                    }
                    else
                    {
                        
                        
                        if (cell.threadField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.threadField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.threadField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.threadView.backgroundColor = UIColor.white
                        cell.threadField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            else if self.editTag == 5
            {
                if (cell.entryField.text != "" && category4Not)
                {
                    invisibleCount += 1

                    if (cell.terminoField.text == "")
                    {
                        
                        cell.terminoView.backgroundColor = UIColor.white
                        cell.terminoField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        if (cell.terminoField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.terminoField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.terminoField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.terminoView.backgroundColor = UIColor.white
                        cell.terminoField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                }
            }
            else if self.editTag == 7
            {
                if (cell.entryField.text != "")
                {
                    invisibleCount += 1

                    if (cell.jusgePlaceField.text == "")
                    {
                        
                        cell.judgePlaceView.backgroundColor = UIColor.white
                        cell.jusgePlaceField.text = "\(self.draggedImageTag)"
                        
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        if (cell.jusgePlaceField.text == "1")
                        {
                            self.number1ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "2")
                        {
                            self.numberImg2Vw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "3")
                        {
                            self.number3ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "4")
                        {
                            self.number4ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "5")
                        {
                            self.number5ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "6")
                        {
                            self.number6ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "7")
                        {
                            self.number7ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "8")
                        {
                            self.number8ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "9")
                        {
                            self.number9ImgVw.isHidden = false
                        }
                        else if (cell.jusgePlaceField.text == "10")
                        {
                            self.number10ImgVw.isHidden = false
                        }
                        
                        
                        
                        cell.judgePlaceView.backgroundColor = UIColor.white
                        cell.jusgePlaceField.text = "\(self.draggedImageTag)"
                        if (self.draggedImageTag == 1)
                        {
                            self.number1ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 2)
                        {
                            self.numberImg2Vw.isHidden = true
                        }
                        else if (self.draggedImageTag == 3)
                        {
                            self.number3ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 4)
                        {
                            self.number4ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 5)
                        {
                            self.number5ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 6)
                        {
                            self.number6ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 7)
                        {
                            self.number7ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 8)
                        {
                            self.number8ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 9)
                        {
                            self.number9ImgVw.isHidden = true
                        }
                        else if (self.draggedImageTag == 10)
                        {
                            self.number10ImgVw.isHidden = true
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            
        }
        
        
      
        
        print("Drop Touch:",touchPoint)
        
 //       for dragItem in session.items
//        {
//            dragItem.itemProvider.loadObject(ofClass: UIImage.self, completionHandler: { (obj, err) in
//
//                if let err = err
//                {
//                    print("Failed to load our dragged item : ",err)
//                    return
//                }
//
//                guard let draggedImage = obj as? UIImage else
//                {
//                    return
//                }
//
//
//
//                DispatchQueue.main.async {
//
//                    if self.selectedCell > -1
//                    {
//
//                        let indexPath = NSIndexPath.init(row: self.selectedCell, section: 0)
//                    let cell = self.judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
//
//
//                    print("Drag:",draggedImage,"Actual:",UIImage.init(named: "Group 1"))
//
//                    if (draggedImage == UIImage.init(named: "Group 1"))
//                    {
//
//                    }
//                    else
//                    {
//                        if self.editTag == 2
//                        {
//                            cell.functionView.backgroundColor = UIColor.white
//
//                            cell.functionField.text = "\(self.draggedImageTag)"
//                        }
//                        else if self.editTag == 3
//                        {
//                            cell.smoothView.backgroundColor = UIColor.white
//                            cell.smoothField.text = "\(self.draggedImageTag)"
//                        }
//                        else if self.editTag == 4
//                        {
//                            cell.threadView.backgroundColor = UIColor.white
//                            cell.threadField.text = "\(self.draggedImageTag)"
//                        }
//                        else if self.editTag == 5
//                        {
//                            cell.terminoView.backgroundColor = UIColor.white
//                            cell.terminoField.text = "\(self.draggedImageTag)"
//                        }
//
//                    }
//
//                    let imageView = UIImageView(image : draggedImage)
//                    imageView.frame = CGRect(x:2, y:2, width:draggedImage.size.width, height:draggedImage.size.height)
//
////                    cell.functionView.addSubview(imageView)
//
//                    let centerPoint = session.location(in: cell.functionView)
//                    imageView.center = centerPoint
//                }
//
//            }
//
//
//            })
//        }
        
        // Perform additional UI updates as needed.
    }
    
    
    
    @available(iOS 11.0, *)
    func dragInteraction(_ interaction: UIDragInteraction, itemsForBeginning session: UIDragSession) -> [UIDragItem] {
  //      guard let image = numberImg2Vw.image else { return [] }
        
//        let provider = NSItemProvider(object: image)
//        let item = UIDragItem(itemProvider: provider)
    //    item.localObject = image
        
        let touchPoint = session.location(in: self.view)
        
        if let touchedImageView = self.view.hitTest(touchPoint, with: nil) as? UIImageView
        {
            print("Image TAG:",touchedImageView.tag)
            self.draggedImageTag = touchedImageView.tag
           let touchedImage = touchedImageView.image
            
            let provider = NSItemProvider(object: touchedImage!)
            let dragItem = UIDragItem(itemProvider: provider)
            
            return[dragItem]
        }
        
        
//        var Image = UIImage.init()
//        for dragItem in session.items
//        {
//            dragItem.itemProvider.loadObject(ofClass: UIImage.self, completionHandler: { (obj, err) in
//
//                if let err = err
//                {
//                    print("Failed to load our dragged item : ",err)
//                    return
//                }
//
//                guard let draggedImage = obj as? UIImage else
//                {
//                    return
//                }
//
//                Image = draggedImage
//
//            })
//        }
        
//            let provider = NSItemProvider(object: Image)
//            let dragItem = UIDragItem(itemProvider: provider)
//            item.localObject = Image
        
        /*
         Returning a non-empty array, as shown here, enables dragging. You
         can disable dragging by instead returning an empty array.
         */
        return []
    }
    
    
//    func dragInteraction(_ interaction: UIDragInteraction, itemsForBeginning session: UIDragSession) -> [UIDragItem] {
//        guard let image = number1ImgVw.image else { return [] }
//
//        let provider = NSItemProvider(object: image)
//        let item = UIDragItem(itemProvider: provider)
//        item.localObject = image
//
//        /*
//         Returning a non-empty array, as shown here, enables dragging. You
//         can disable dragging by instead returning an empty array.
//         */
//        return [item]
//    }
    
    
    // MARK : - UIDropInteraction Delegates
   
//
//    @available(iOS 11.0, *)
//    private func drop(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
//        let dropLocation = session.location(in: view)
//        // updateLayers(forDropLocation: dropLocation)
//
//        let operation: UIDropOperation
//
//        if dropDownImgVw.frame.contains(dropLocation) {
//            /*
//             If you add in-app drag-and-drop support for the .move operation,
//             you must write code to coordinate between the drag interaction
//             delegate and the drop interaction delegate.
//             */
//            operation = session.localDragSession == nil ? .copy : .move
//        } else {
//            // Do not allow dropping outside of the image view.
//            operation = .cancel
//        }
//
//        return UIDropProposal(operation: operation)
//    }
    
    
    
    // MARK: - UITableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:DataListCell = judgeCardTable.dequeueReusableCell(withIdentifier: "JudgeCardCell") as! DataListCell
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return judgeCardTable.frame.size.height/10   //(80/768) * self.view.frame.size.height;
    }
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if let cell = cell as? DataListCell
        {
            cell.serialNumber.text = String(indexPath.row+1)
            
            cell.entryField.tag = indexPath.row
            cell.nameField.tag = indexPath.row
            
            if (judgesCardList.count > indexPath.row)
            {
                let objectDict : NSDictionary = judgesCardList.object(at: indexPath.row) as! NSDictionary
                
                if (objectDict.object(forKey: "calculate") as! String == "0")
                {
                    cell.isUserInteractionEnabled = true
                }
                else
                {
                    cell.isUserInteractionEnabled = false
                }
                
                cell.entryField.text = objectDict.object(forKey: "entrynumber") as! String
                cell.nameField.text = objectDict.object(forKey: "horsename") as! String
                
                if objectDict.object(forKey: "judgeplace") as! String != "0"
                {
                    cell.judgePlaceView.backgroundColor = UIColor.white
                    cell.jusgePlaceField.text = objectDict.object(forKey: "judgeplace") as! String
                }
                else
                {
                    cell.judgePlaceView.backgroundColor = UIColor.clear
                    cell.jusgePlaceField.text = ""
                }
                
                if objectDict.object(forKey: "total") as! String != "0.00"
                {
                    cell.calculateField.text = objectDict.object(forKey: "total") as! String
                }
                else
                {
                    cell.calculateField.text = ""
                }
                
                
                if ((objectDict.object(forKey: "category_details") as! NSArray).count > 0)
                {
                for catIndex in 0...(objectDict.object(forKey: "category_details") as! NSArray).count - 1
                {
                    let objectCat : NSDictionary = (objectDict.object(forKey: "category_details") as! NSArray).object(at: catIndex) as! NSDictionary
                    
                    if catIndex == 0
                    {
                        if (objectCat.object(forKey: "place") as! String == "0")
                        {
                            cell.functionView.backgroundColor = UIColor.clear
                            cell.functionField.text = ""
                        }
                        else
                        {
                            cell.functionView.backgroundColor = UIColor.white
                            cell.functionField.text = objectCat.object(forKey: "place") as! String
                        }
                    }
                    else if catIndex == 1
                    {
                        if (objectCat.object(forKey: "place") as! String == "0")
                        {
                            cell.smoothView.backgroundColor = UIColor.clear
                            cell.smoothField.text = ""
                        }
                        else
                        {
                        
                            cell.smoothView.backgroundColor = UIColor.white
                            cell.smoothField.text = objectCat.object(forKey: "place") as! String
                        }
                    }
                    else if catIndex == 2
                    {
                        if (objectCat.object(forKey: "place") as! String == "0")
                        {
                            cell.threadView.backgroundColor = UIColor.clear
                            cell.threadField.text = ""
                        }
                        else
                        {
                            cell.threadView.backgroundColor = UIColor.white
                            cell.threadField.text = objectCat.object(forKey: "place") as! String
                        }
                    }
                    else
                    {
                        if (objectCat.object(forKey: "place") as! String == "0")
                        {
                            cell.terminoView.backgroundColor = UIColor.clear
                            cell.terminoField.text = ""
                        }
                        else
                        {
                            cell.terminoView.backgroundColor = UIColor.white
                          cell.terminoField.text = objectCat.object(forKey: "place") as! String
                        }
                    }
                    
                }
                    
                
            }
                
                
            }
            else
            {
                
                if (horseNameEdit.text == "DONE")
                {
                    cell.entryView.isUserInteractionEnabled = true
                   cell.entryField.isUserInteractionEnabled = true
                }
                else
                {
                    cell.entryField.layer.borderWidth = 0.0
                    cell.entryField.layer.borderColor = UIColor.clear.cgColor
                    cell.entryView.layer.borderWidth = 0.0
                    cell.entryView.layer.borderColor = UIColor.clear.cgColor
                    cell.entryField.isUserInteractionEnabled = false
                }
            
                cell.entryField.text = ""
                cell.nameField.isUserInteractionEnabled = false
                cell.nameField.text = ""
                cell.functionView.backgroundColor = UIColor.clear
                cell.functionField.text = ""
                cell.smoothView.backgroundColor = UIColor.clear
                cell.smoothField.text = ""
                cell.threadView.backgroundColor = UIColor.clear
                cell.threadField.text = ""
                cell.terminoView.backgroundColor = UIColor.clear
                cell.terminoField.text = ""
                cell.calculateField.text = ""
                cell.judgePlaceView.backgroundColor = UIColor.clear
                cell.jusgePlaceField.text = ""
            }
            
            
        }
        
    }
    
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        selectedCell = indexPath.row
    }
    
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, canHandle session: UIDropSession) -> Bool {
        return true //model.canHandle(session)
    }
    
    @available(iOS 11.0, *)
    func canHandle(_ session: UIDropSession) -> Bool {
        return session.canLoadObjects(ofClass: NSString.self)
    }
//
//    @available(iOS 11.0, *)
//    func tableView(_ tableView: UITableView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UITableViewDropProposal {
//        // The .move operation is available only for dragging within a single app.
//        if tableView.hasActiveDrag {
//            if session.items.count > 1 {
//                return UITableViewDropProposal(operation: .cancel)
//            } else {
//                return UITableViewDropProposal(operation: .move, intent: .insertAtDestinationIndexPath)
//            }
//        } else {
//            return UITableViewDropProposal(operation: .copy, intent: .insertIntoDestinationIndexPath)
//        }
//    }
//
//    @available(iOS 11.0, *)
//    @available(iOS 11.0, *)
//    func tableView(_ tableView: UITableView, performDropWith coordinator: UITableViewDropCoordinator) {
//        let destinationIndexPath: IndexPath
//
//        if let indexPath = coordinator.destinationIndexPath {
//            destinationIndexPath = indexPath
//        } else {
//            // Get last index path of table view.
//            let section = tableView.numberOfSections - 1
//            let row = tableView.numberOfRows(inSection: section)
//            destinationIndexPath = IndexPath(row: row, section: section)
//        }
//
//        coordinator.session.loadObjects(ofClass: NSString.self) { items in
//            // Consume drag items.
//            let stringItems = items as! [String]
//
//            var indexPaths = [IndexPath]()
//            for (index, item) in stringItems.enumerated() {
//                let indexPath = IndexPath(row: destinationIndexPath.row + index, section: destinationIndexPath.section)
//              //  self.model.addItem(item, at: indexPath.row)
//                indexPaths.append(indexPath)
//            }
//
//            tableView.insertRows(at: indexPaths, with: .automatic)
//        }
//    }
    
    
    
//    @available(iOS 11.0, *)
//    public func tableView(_ tableView: UITableView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UITableViewDropProposal
//    {
//
//    }
    
    
    // MARK: - TextField Delegates
    public func textFieldDidBeginEditing(_ textField: UITextField)
    {
        /////////////////////// Extra Addition ////////////
        if(selectTextTag == -1)
        {
            selectTextTag = textField.tag
        }
        else
        {
            let prevIndex = NSIndexPath.init(row: selectTextTag, section: 0)
            let prevCell = self.judgeCardTable.cellForRow(at: prevIndex as IndexPath) as! DataListCell

            if(selectTextTag != textField.tag)
            {

            if(prevCell.entryField.text as! String != "")
            {
            HorseFetch(RowId : selectTextTag, HorseEntryNo : prevCell.entryField.text as! String)
            }

            selectTextTag = textField.tag
            }

        }
        
        
        ////////////////// Extra Addition /////////////////
        
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.transitionCurlUp, animations: {
            self.mainScroll.contentOffset.y = (CGFloat(Int(self.judgeCardTable.frame.size.height/10) * (textField.tag + 1)) + 60)
        }, completion: nil)
    }
    
   
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool
    {
        //  HorseEntryTable.setContentOffset(CGPoint (x: 0, y: 0), animated: true)
        textField.endEditing(true)


        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.transitionCurlUp, animations: {
            self.mainScroll.contentOffset.y = 0
        }, completion: nil)


        return true
    }
    public func textFieldDidEndEditing(_ textField: UITextField)
    {
//        let alert = UIAlertController(title: nil, message: "New Entry", preferredStyle: UIAlertControllerStyle.alert)
//
//        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//
//        self.present(alert, animated: true, completion: nil)
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let indexPath = IndexPath(row: textField.tag, section: 0)
        let cell = judgeCardTable.cellForRow(at: indexPath) as! DataListCell
        
         let scalars = string.unicodeScalars
        if textField == cell.entryField
        {
            if (scalars[scalars.startIndex].value > 46 && scalars[scalars.startIndex].value < 58)  // string == "0" || string == "1" || string == "2" || string == "3" || string == "4" || string == "5" || string == "6" || string == "7" || string == "8" || string == "9"
            {
                var startString = ""
                if (textField.text != nil)
                {
                    startString += textField.text!
                }
                startString += string
                let limitNumber = Int(startString)
                
                if (Double(limitNumber!)>=1 && Double(limitNumber!)<=1000)
                {
                    return true
                }
                else
                {
                    return false
                }
                
            }
            else if (scalars[scalars.startIndex].value == 65533)
            {
                return true
            }
            else
            {
                return false
            }
            
        }
        else
        {
            return true
        }
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        //  HorseEntryTable.setContentOffset(CGPoint (x: 0, y: 0), animated: true)
        textField.endEditing(true)
        
        
        let prevIndex = NSIndexPath.init(row: textField.tag, section: 0)
        let prevCell = self.judgeCardTable.cellForRow(at: prevIndex as IndexPath) as! DataListCell
        
//        if(selectTextTag != textField.tag)
//        {
        
            if(prevCell.entryField.text as! String != "")
            {
                HorseFetch(RowId : textField.tag, HorseEntryNo : prevCell.entryField.text as! String)
            }
            
//            selectTextTag = textField.tag
//        }
        
    
    
    ////////////////// Extra Addition /////////////////
        
        
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.transitionCurlUp, animations: {
            self.mainScroll.contentOffset.y = 0
        }, completion: nil)
        
        
        return true
    }
        
        
//        let indexPath = IndexPath(row: textField.tag, section: 0)
//        let cell = judgeCardTable.cellForRow(at: indexPath) as! DataListCell
//
//        if textField == cell.entryField
//        {
//
//        if cell.entryField.text as! String != ""
//        {
//            if showLbl.text == "Choose Show Name"
//            {
//                showLbl.textColor = UIColor.red
//            }
//            else
//            {
//                cellIndex = String(textField.tag)
//
//                if (cell.entryField.text != "")
//                {
//                    // Validation Checking
//                    if (textField.tag > 0)
//                    {
//                    let prevIndexPath = IndexPath(row: textField.tag - 1, section: 0)
//                    let prevCell = judgeCardTable.cellForRow(at: prevIndexPath) as! DataListCell
//
//                    if (prevCell.entryField.text != "")
//                    {
//                         var duplicate = 0
//                        for index in 0...9
//                        {
//                            let indexPath = NSIndexPath.init(row: index, section: 0)
//
//                            let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
//
//
//                            // Duplicate checking
//
//
//
//                            var classDataId = ""
//
//
//                            print("text tag:",textField.tag,"index:",index,"textfield value:",textField.text as! String,"cell value:",cell.entryField.text as! String)
//
//                                    if (textField.tag != index && textField.text as! String == cell.entryField.text as! String)
//                                    {
//                                        duplicate = duplicate + 1
//                                    }
//
//                        }
//
//                            if (duplicate > 0)
//                            {
//                                Error = true
//
//                                let alert = UIAlertController(title: "Error", message: "Horse Entry Already Selected", preferredStyle: UIAlertControllerStyle.alert)
//                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//
//                                self.present(alert, animated: true, completion: nil)
//
//                              //  break
//                            }
//                            else
//                            {
//                                cell.nameField.text = ""
//                                cell.functionField.text = ""
//                                cell.smoothField.text = ""
//                                cell.threadField.text = ""
//                                cell.terminoField.text = ""
//
//
//                                HorseFetch(RowId : textField.tag, HorseEntryNo : cell.entryField.text as! String)
//                               // break
//                            }
//
//
//                    }
//                    else
//                    {
//                        let alert = UIAlertController(title: "Error", message: "You cannot leave the field blank", preferredStyle: UIAlertControllerStyle.alert)
//                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//                        self.present(alert, animated: true, completion: nil)
//                    }
//
//                    }
//                    else
//                    {
//                        for index in 0...9
//                        {
//                            let indexPath = NSIndexPath.init(row: index, section: 0)
//
//                            let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
//
//
//                        // Duplicate checking
//
//                        var duplicate = 0
//
//                        var classDataId = ""
//
//                        if judgesCardList.count > 0
//                        {
//                            var rowCount = 0
//                            for tempObject in judgesCardList
//                            {
//                                let classObject = tempObject as! NSDictionary
//
//
//
//                                if (textField.tag != index && classObject.object(forKey: "entrynumber") as! String == cell.entryField.text)
//                                {
//                                    duplicate = duplicate + 1
//                                }
//
//                                rowCount = rowCount + 1
//                            }
//
//                        }
//
//                        if (duplicate > 0)
//                        {
//                            Error = true
//
//                            let alert = UIAlertController(title: "Error", message: "Horse Entry Already Selected", preferredStyle: UIAlertControllerStyle.alert)
//                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//
//                            self.present(alert, animated: true, completion: nil)
//
//                            break
//                        }
//                        else
//                        {
//                            cell.nameField.text = ""
//                            cell.functionField.text = ""
//                            cell.smoothField.text = ""
//                            cell.threadField.text = ""
//                            cell.terminoField.text = ""
//
//                            HorseFetch(RowId : textField.tag, HorseEntryNo : cell.entryField.text as! String)
//                            break
//                        }
//
//
//                        }
//
//                    }
//                }
//                else
//                {
//                    print("judgescardlist object:",judgesCardList.object(at: textField.tag))
//
//                    judgesCardList.removeObject(at: textField.tag)
//
//                    for index in 0...9
//                    {
//                        let indexPath = NSIndexPath.init(row: index, section: 0)
//
//                        let cell:DataListCell = self.judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
//
//                        cell.entryField.text = ""
//                        cell.entryView.layer.borderWidth = 0.0
//                        cell.entryView.layer.borderColor = UIColor.clear.cgColor
//
//                        cell.nameField.text = ""
//                        cell.functionView.backgroundColor = UIColor.clear
//                        cell.functionField.text = ""
//                        cell.smoothView.backgroundColor = UIColor.clear
//                        cell.smoothField.text = ""
//                        cell.threadView.backgroundColor = UIColor.clear
//                        cell.threadField.text = ""
//                        cell.terminoView.backgroundColor = UIColor.clear
//                        cell.terminoField.text = ""
//                        cell.calculateField.text = ""
//                        cell.judgePlaceView.backgroundColor = UIColor.clear
//                        cell.jusgePlaceField.text = ""
//
//                    }
//
//                    judgeCardTable.reloadData()
//                }
//            }
//        }
//        else
//        {
//
//
//            let clearIndexPath = IndexPath(row: textField.tag , section: 0)
//            let clearCell = judgeCardTable.cellForRow(at: clearIndexPath) as! DataListCell
//
//            clearCell.nameField.text = ""
//            clearCell.functionField.text = ""
//            clearCell.smoothField.text = ""
//            clearCell.threadField.text = ""
//            clearCell.terminoField.text = ""
//
//            clearCell.functionView.backgroundColor = UIColor.clear
//            clearCell.smoothView.backgroundColor = UIColor.clear
//            clearCell.threadView.backgroundColor = UIColor.clear
//            clearCell.terminoView.backgroundColor = UIColor.clear
//
//            if (textField.tag <= judgesCardList.count - 1)
//            {
//
//                judgesCardList.removeObject(at: textField.tag)
//
//                for index in 0...9
//                {
//                    let indexPath = NSIndexPath.init(row: index, section: 0)
//
//                    let cell:DataListCell = self.judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
//
//                    cell.entryField.text = ""
//                    cell.entryView.layer.borderWidth = 0.0
//                    cell.entryView.layer.borderColor = UIColor.clear.cgColor
//
//                    cell.nameField.text = ""
//                    cell.functionView.backgroundColor = UIColor.clear
//                    cell.functionField.text = ""
//                    cell.smoothView.backgroundColor = UIColor.clear
//                    cell.smoothField.text = ""
//                    cell.threadView.backgroundColor = UIColor.clear
//                    cell.threadField.text = ""
//                    cell.terminoView.backgroundColor = UIColor.clear
//                    cell.terminoField.text = ""
//                    cell.calculateField.text = ""
//                    cell.judgePlaceView.backgroundColor = UIColor.clear
//                    cell.jusgePlaceField.text = ""
//
//                }
//
//                judgeCardTable.reloadData()
//
//
//
//
////                print("judgescardlist object:",judgesCardList)
////                judgeCardTable.beginUpdates()
////                judgeCardTable.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
////                judgeCardTable.endUpdates()
////
//
//
//               // judgeCardTable.reloadData()
//            }
//
//        }
//
//        }
        
    
    

 
    
    
    // MARK: - Horse Exist or Not Api Execute
    func checkHorseExist(horseId: NSArray)
    {
        
        DispatchQueue.main.async {
            self.CheckLoader()
        }
        
        
        let  url_to_request = GlobalViewController.GLOBALAPI + "show/checkHorseExist"
        
        print("Full URL:",url_to_request)
        
        let urlString:NSURL = NSURL(string: url_to_request)!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: urlString as URL)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        

        
        var parameter = ""
        let horseIDToSend : NSMutableArray = NSMutableArray(array:horseId)
        
        print("parameter:",parameter)
        request.httpMethod = "POST"
        
        
        do {
            
            //Convert to Data
            let jsonData = try JSONSerialization.data(withJSONObject: horseIDToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            
            //Convert back to string. Usually only do this for debugging
            if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                print(JSONString)
                
                let allowedCharacterSet = CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[] ").inverted
                
                if let escapedString = JSONString.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) {
                    print(escapedString)
                    
                    //  paramString = "updatepreferences=\(escapedString)"
                    
                    parameter = "showid=\(ShowId)&horseentry=\(escapedString)"
                    
                }
                
                
            }
            
            
            
            print("param string \(parameter)")
            
        } catch {
            
        }

        request.httpBody = parameter.data(using: String.Encoding.utf8)
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            
            
            (data, response, error) in
            
            
            guard let _:Data = data, let _:URLResponse = response, error == nil
                
                else
            {
                print("error")
                let alert = UIAlertController(title: "Error", message:error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                OperationQueue.main.addOperation
                    {
                        self.CheckLoader()
                        self.present(alert, animated: true, completion: nil)
                }
                return
            }
            //    self.statusCode = [(NSHTTPURLResponse*)response statusCode];
            if let httpResponse = response as? HTTPURLResponse
            {
                
                
                print("statusCode \(httpResponse.statusCode)")
                if (httpResponse.statusCode == 200)
                {
                    
                    
                    
                    let JSONData = data
                    do {
                        let JSON = try JSONSerialization.jsonObject(with: JSONData!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                        guard let JSONDictionary: NSDictionary = JSON as? NSDictionary else {
                            print("Not a Dictionary")
                            // put in function
                            
                            print("JSONDictionary \(JSON)")
                            DispatchQueue.main.async {
                                self.CheckLoader()
                            }
                            return
                        }
                        
                        print("JSONDictionary \(JSONDictionary)")
                        
                        
                        let status : Bool = JSONDictionary.object(forKey: "status") as! Bool
                        
                        if (status == true)
                        {
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                                let details:NSMutableArray = NSMutableArray(array:JSONDictionary.object(forKey: "horseentry") as! NSArray)
                                
                                print("Horse Fetch:",details)
                                
                                if(details.count > 0)
                                {
                                    let horseNumbers = details.componentsJoined(by: ",")
                                    
                                    
                                    let alert = UIAlertController(title: "New Entry", message: "Entry # \(horseNumbers) does not exist. Do you want to add New Entry?", preferredStyle: UIAlertControllerStyle.alert)
                                    
                                    let yesAction = UIAlertAction.init(title: "Yes", style: UIAlertActionStyle.default, handler: { _ in
                                        print("New Entry")
                                        
                                        // Add Judges Card
                                        self.JudgesCardAddEdit(horseId: self.horseIdArray)
                                        
                                    })
                                    
                                    let noAction = UIAlertAction.init(title: "No", style: UIAlertActionStyle.default, handler: { _ in
                                        print("Fetch Entry")
                                        
                                        
                                    
                                    repeat {
                                    self.horseIdArray.remove(details.object(at: 0) as! String)
                                    
                                    details.remove(details.object(at: 0) as! String)
                                        
                                    }while(details.count > 0)
                                        
                                        if(self.horseIdArray.count > 0)
                                        {
                                        // Add Judges Card
                                        self.JudgesCardAddEdit(horseId: self.horseIdArray)
                                        }
                                        else
                                        {
                                          self.judgesCardList.removeAllObjects()
                                            self.judgeCardTable.reloadData()
                                        }
                                        
                                    })
                                    
                                    alert.addAction(noAction)
                                    alert.addAction(yesAction)
                                    
                                    
                                        self.present(alert, animated: true, completion: nil)
                                }
                                else
                                {
                                    // Add Judges Card
                                    self.JudgesCardAddEdit(horseId: self.horseIdArray)
                                }
                                

                                
                            }
                            
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                            }
                        }
                        
                    }
                    catch let JSONError as NSError {
                        print("\(JSONError)")
                        let alert = UIAlertController(title: "Error", message: JSONError.localizedDescription as String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        OperationQueue.main.addOperation
                            {
                                self.CheckLoader()
                                self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
                else if(httpResponse.statusCode == 400)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else if(httpResponse.statusCode == 500)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        self.CheckLoader()
                    }
                }
                
            }
            
        })
        
        task.resume()
        
    }
    
    
    
    
    // MARK: - Horse Fetch Api Execute
    func HorseFetch(RowId : NSInteger, HorseEntryNo : String)
    {
        
        
//        DispatchQueue.main.async {
//            self.CheckLoader()
//        }
//
        
        let  url_to_request = GlobalViewController.GLOBALAPI + "show/horsefetch"
        
        print("Full URL:",url_to_request)
        
        let urlString:NSURL = NSURL(string: url_to_request)!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: urlString as URL)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        let parameter = "entrynumber=\(HorseEntryNo)&showid=\(ShowId)"
        
        print("parameter:",parameter)
        request.httpMethod = "POST"
        request.httpBody = parameter.data(using: String.Encoding.utf8)
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            
            
            (data, response, error) in
            
            
            guard let _:Data = data, let _:URLResponse = response, error == nil
                
                else
            {
                print("error")
                let alert = UIAlertController(title: "Error", message:error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                OperationQueue.main.addOperation
                    {
                      //  self.CheckLoader()
                        self.present(alert, animated: true, completion: nil)
                }
                return
            }
            //    self.statusCode = [(NSHTTPURLResponse*)response statusCode];
            if let httpResponse = response as? HTTPURLResponse
            {
                
                
                print("statusCode \(httpResponse.statusCode)")
                if (httpResponse.statusCode == 200)
                {
                    
                    
                    
                    let JSONData = data
                    do {
                        let JSON = try JSONSerialization.jsonObject(with: JSONData!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                        guard let JSONDictionary: NSDictionary = JSON as? NSDictionary else {
                            print("Not a Dictionary")
                            // put in function
                            
                            print("JSONDictionary \(JSON)")
                            DispatchQueue.main.async {
                                self.CheckLoader()
                            }
                            return
                        }
                        
                        print("JSONDictionary \(JSONDictionary)")
                        
                        
                        let status : Bool = JSONDictionary.object(forKey: "status") as! Bool
                        
                        if (status == true)
                        {
                            DispatchQueue.main.async {
                             //   self.CheckLoader()
                                
                            let details:NSMutableArray = NSMutableArray(array:JSONDictionary.object(forKey: "details") as! NSArray)
                                
                                print("Horse Fetch:",details)
                                
                                let indexPath = IndexPath(row: RowId, section: 0)
                                let cell = self.judgeCardTable.cellForRow(at: indexPath) as! DataListCell
                                cell.nameField.text = (details.object(at: 0) as! NSDictionary).object(forKey: "name") as! String
                                
                            }
                            
                        }
                        else
                        {
                            let alert = UIAlertController(title: "New Entry", message: "This Entry # does not exist. Do you want to add a New Entry?", preferredStyle: UIAlertControllerStyle.alert)
                            
                            let yesAction = UIAlertAction.init(title: "Yes", style: UIAlertActionStyle.default, handler: { _ in
                                print("New Entry")
                                
                                self.HorseEntry(RowId: RowId, HorseEntryNo: HorseEntryNo)
                                
                            })
                            
                            let noAction = UIAlertAction.init(title: "No", style: UIAlertActionStyle.default, handler: { _ in
                                print("Fetch Entry")
                                
                                let indexPath = IndexPath(row: RowId, section: 0)
                                let cell = self.judgeCardTable.cellForRow(at: indexPath) as! DataListCell
                                cell.entryField.text = ""
                            })
                            
                            alert.addAction(noAction)
                            alert.addAction(yesAction)
                            
                            
                            DispatchQueue.main.async {
                                self.present(alert, animated: true, completion: nil)
                             //   self.CheckLoader()
                                
                            }
                        }
                        
                    }
                    catch let JSONError as NSError {
                        print("\(JSONError)")
                        let alert = UIAlertController(title: "Error", message: JSONError.localizedDescription as String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        OperationQueue.main.addOperation
                            {
                              //  self.CheckLoader()
                                self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
                else if(httpResponse.statusCode == 400)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                          //  self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else if(httpResponse.statusCode == 500)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                         //   self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                     //   self.CheckLoader()
                    }
                }
                
            }
            
        })
        
        task.resume()
        
    }
    
    
    // MARK: - Horse Entry Api Execute
    func HorseEntry(RowId : NSInteger, HorseEntryNo : String)
    {
        
        
        DispatchQueue.main.async {
          //  self.CheckLoader()
        }
        
        
        let  url_to_request = GlobalViewController.GLOBALAPI + "show/horseentry"
        
        print("Full URL:",url_to_request)
        
        let urlString:NSURL = NSURL(string: url_to_request)!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: urlString as URL)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        let parameter = "entrynumber=\(HorseEntryNo)&showid=\(ShowId)&name=&id="
        
        print("parameter:",parameter)
        request.httpMethod = "POST"
        request.httpBody = parameter.data(using: String.Encoding.utf8)
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            
            
            (data, response, error) in
            
            
            guard let _:Data = data, let _:URLResponse = response, error == nil
                
                else
            {
                print("error")
                let alert = UIAlertController(title: "Error", message:error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                OperationQueue.main.addOperation
                    {
                      //  self.CheckLoader()
                        self.present(alert, animated: true, completion: nil)
                }
                return
            }
            //    self.statusCode = [(NSHTTPURLResponse*)response statusCode];
            if let httpResponse = response as? HTTPURLResponse
            {
                
                
                print("statusCode \(httpResponse.statusCode)")
                if (httpResponse.statusCode == 200)
                {
                    
                    
                    
                    let JSONData = data
                    do {
                        let JSON = try JSONSerialization.jsonObject(with: JSONData!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                        guard let JSONDictionary: NSDictionary = JSON as? NSDictionary else {
                            print("Not a Dictionary")
                            // put in function
                            
                            print("JSONDictionary \(JSON)")
                            DispatchQueue.main.async {
                               // self.CheckLoader()
                            }
                            return
                        }
                        
                        print("JSONDictionary \(JSONDictionary)")
                        
                        
                        let status : Bool = JSONDictionary.object(forKey: "status") as! Bool
                        
                        if (status == true)
                        {
                            DispatchQueue.main.async {
                             //   self.CheckLoader()
                                
                                let details:NSDictionary = JSONDictionary.object(forKey: "details") as! NSDictionary
                                
                                print("Horse Entry:",details)
                                
                                
                                let alert = UIAlertController(title: nil, message: JSONDictionary.value(forKey: "message") as? String, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                
                              //  self.present(alert, animated: true, completion: nil)
                                
                            }
                            
                        }
                        else
                        {
                            let alert = UIAlertController(title: "Error", message: JSONDictionary.value(forKey: "message") as? String, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            
                            DispatchQueue.main.async {
                                self.present(alert, animated: true, completion: nil)
                               // self.CheckLoader()
                                
                                
                            }
                        }
                        
                    }
                    catch let JSONError as NSError {
                        print("\(JSONError)")
                        let alert = UIAlertController(title: "Error", message: JSONError.localizedDescription as String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        OperationQueue.main.addOperation
                            {
                               // self.CheckLoader()
                                self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
                else if(httpResponse.statusCode == 400)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                          //  self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else if(httpResponse.statusCode == 500)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                          //  self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                       // self.CheckLoader()
                    }
                }
                
            }
            
        })
        
        task.resume()
        
    }
    
    
    // MARK: - Judge's Card Fetch Api Execute
    func FetchJudgesCard()
    {
        DispatchQueue.main.async {
            self.CheckLoader()
        }
        
        
        let  url_to_request = GlobalViewController.GLOBALAPI + "show/fetchjudgescard"
        
        print("Full URL:",url_to_request)
        
        let urlString:NSURL = NSURL(string: url_to_request)!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: urlString as URL)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        let parameter = "classid=\(classId)&showid=\(ShowId)"
        
        print("parameter:",parameter)
        request.httpMethod = "POST"
        request.httpBody = parameter.data(using: String.Encoding.utf8)
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            
            
            (data, response, error) in
            
            
            guard let _:Data = data, let _:URLResponse = response, error == nil
                
                else
            {
                print("error")
                let alert = UIAlertController(title: "Error", message:error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                OperationQueue.main.addOperation
                    {
                        self.CheckLoader()
                        self.present(alert, animated: true, completion: nil)
                }
                return
            }
            //    self.statusCode = [(NSHTTPURLResponse*)response statusCode];
            if let httpResponse = response as? HTTPURLResponse
            {
                
                
                print("statusCode \(httpResponse.statusCode)")
                if (httpResponse.statusCode == 200)
                {
                    
                    
                    
                    let JSONData = data
                    do {
                        let JSON = try JSONSerialization.jsonObject(with: JSONData!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                        guard let JSONDictionary: NSDictionary = JSON as? NSDictionary else {
                            print("Not a Dictionary")
                            // put in function
                            
                            print("JSONDictionary \(JSON)")
                            DispatchQueue.main.async {
                                self.CheckLoader()
                            }
                            return
                        }
                        
                        print("JSONDictionary \(JSONDictionary)")
                        
                        
                        let status : Bool = JSONDictionary.object(forKey: "status") as! Bool
                        
                        if (status == true)
                        {
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                               // let details:NSDictionary = JSONDictionary.object(forKey: "card_details") as! NSDictionary
                                
                                
                                for index in 0...9
                                {
                                    let indexPath = NSIndexPath.init(row: index, section: 0)
                                    
                                    let cell:DataListCell = self.judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                                    
                                    cell.entryField.text = ""
                                    cell.entryField.layer.borderWidth = 0.0
                                    cell.entryField.layer.borderColor = UIColor.clear.cgColor
                                    cell.entryView.layer.borderWidth = 0.0
                                    cell.entryView.layer.borderColor = UIColor.clear.cgColor
                                    
                                cell.entryField.isUserInteractionEnabled = false
                                    cell.nameField.text = ""
                                    cell.functionView.backgroundColor = UIColor.clear
                                    cell.functionField.text = ""
                                    cell.smoothView.backgroundColor = UIColor.clear
                                    cell.smoothField.text = ""
                                    cell.threadView.backgroundColor = UIColor.clear
                                    cell.threadField.text = ""
                                    cell.terminoView.backgroundColor = UIColor.clear
                                    cell.terminoField.text = ""
                                    cell.calculateField.text = ""
                                    cell.judgePlaceView.backgroundColor = UIColor.clear
                                    cell.jusgePlaceField.text = ""
                                    
                                }
                                
                                self.smoothEditBtn.isUserInteractionEnabled = true
                                self.smoothEdit.text = "ENTER"
                                self.smoothEdit.backgroundColor = UIColor.white
                                self.functionEditBtn.isUserInteractionEnabled = true
                                self.functionEdit.text = "ENTER"
                                self.functionEdit.backgroundColor = UIColor.white
                                self.threadEditBtn.isUserInteractionEnabled = true
                                self.threadEdit.text = "ENTER"
                                self.threadEdit.backgroundColor = UIColor.white
                                self.terminoEditBtn.isUserInteractionEnabled = true
                                self.terminoEdit.text = "ENTER"
                                self.terminoEdit.backgroundColor = UIColor.white
                                self.horseNameEditBtn.isUserInteractionEnabled = true
                                self.horseNameEdit.text = "ENTER"
                                self.horseNameEdit.backgroundColor = UIColor.white
                                self.calculateBtn.isUserInteractionEnabled = true
                                
                            self.saveBtn.isUserInteractionEnabled = true
                                self.saveLbl.text = "ENTER"
                                self.saveLbl.backgroundColor = UIColor.white
                                
                                
                                
                                self.judgesCardList = NSMutableArray(array:JSONDictionary.object(forKey: "card_details") as! NSArray)
                                
                                print("Card Fetch:",JSONDictionary.object(forKey: "card_details") as! NSArray)
                                
                               
                                if (JSONDictionary.object(forKey: "iscalculate") as! Bool)
                                {
                               
                                   self.isCalculate = true
                                self.smoothEditBtn.isUserInteractionEnabled = false
                                self.functionEditBtn.isUserInteractionEnabled = false
                                self.threadEditBtn.isUserInteractionEnabled = false
                                self.terminoEditBtn.isUserInteractionEnabled = false
                                self.horseNameEditBtn.isUserInteractionEnabled = false
                                self.calculateBtn.isUserInteractionEnabled = false
                                    
                                    
                                    self.placeHeader.isHidden = true
                                    self.entryHeader.isHidden = true
                                    self.functionHeader.isHidden = true
                                    self.smoothHeader.isHidden = true
                                    self.threadHeader.isHidden = true
                                    self.terminoHeader.isHidden = true
                                    self.calculateHeader.isHidden = true
                                    self.saveHeader.isHidden = false
                                    
                                    self.printBtn.isHidden = true
                                    self.PrintLbl.isHidden = true
                                
                                }
                                else
                                {
                                    self.isCalculate = false
                                    self.placeHeader.isHidden = false
                                    
                                    self.entryHeader.isHidden = false
                                    
                                    self.functionHeader.isHidden = false
                                    
                                    self.smoothHeader.isHidden = false
                                    
                                    self.threadHeader.isHidden = false
                                    
                                    self.terminoHeader.isHidden = false
                                    
                                    self.calculateHeader.isHidden = false
                                    self.saveHeader.isHidden = false
                                    
                                    self.printBtn.isHidden = true
                                    self.PrintLbl.isHidden = true
                                }
                                
                                
                                if (JSONDictionary.object(forKey: "isjudgeplace") as! Bool)
                                {
                                    self.placeHeader.isHidden = true
                                    self.entryHeader.isHidden = true
                                    self.functionHeader.isHidden = true
                                    self.smoothHeader.isHidden = true
                                    self.threadHeader.isHidden = true
                                    self.terminoHeader.isHidden = true
                                    self.calculateHeader.isHidden = true
                                    self.saveHeader.isHidden = true
                                    
                                    self.printBtn.isHidden = false
                                    self.PrintLbl.isHidden = false
                                    
                                    
                                    if (JSONDictionary.object(forKey: "isimagesaved") as! Bool)
                                    {
                                        self.PrintLbl.text = "CARD SAVED TO GALLERY"
                                    }
                                    else
                                    {
                                        self.PrintLbl.text = "SAVE CARD TO GALLERY"
                                    }
                                    
                                }

                                    
                                self.judgeCardTable.reloadData()
                                self.judgeCardTable.isHidden = false
                                self.tableHeaderView.isHidden = false
                                
                            }
                            
                        }
                        else
                        {
                            let alert = UIAlertController(title: "Error", message: JSONDictionary.value(forKey: "message") as? String, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            
                            DispatchQueue.main.async
                            {
                               // self.present(alert, animated: true, completion: nil)
                                self.CheckLoader()
                                
                                self.placeHeader.isHidden = false
                                self.entryHeader.isHidden = false
                                self.functionHeader.isHidden = false
                                self.smoothHeader.isHidden = false
                                self.threadHeader.isHidden = false
                                self.terminoHeader.isHidden = false
                                self.calculateHeader.isHidden = false
                                self.saveHeader.isHidden = false
                                
                                self.printBtn.isHidden = true
                                self.PrintLbl.isHidden = true
                                
                                self.smoothEditBtn.isUserInteractionEnabled = true
                                self.smoothEdit.text = "ENTER"
                                self.smoothEdit.backgroundColor = UIColor.white
                                self.functionEditBtn.isUserInteractionEnabled = true
                                self.functionEdit.text = "ENTER"
                                self.functionEdit.backgroundColor = UIColor.white
                                self.threadEditBtn.isUserInteractionEnabled = true
                                self.threadEdit.text = "ENTER"
                                self.threadEdit.backgroundColor = UIColor.white
                                self.terminoEditBtn.isUserInteractionEnabled = true
                                self.terminoEdit.text = "ENTER"
                                self.terminoEdit.backgroundColor = UIColor.white
                                self.horseNameEditBtn.isUserInteractionEnabled = true
                                self.horseNameEdit.text = "ENTER"
                                self.horseNameEdit.backgroundColor = UIColor.white
                                self.calculateBtn.isUserInteractionEnabled = true
                                
                                self.saveBtn.isUserInteractionEnabled = true
                                self.saveLbl.text = "ENTER"
                                self.saveLbl.backgroundColor = UIColor.white
                                
                                
                                self.judgesCardList.removeAllObjects()
                                
                                for index in 0...9
                                {
                                    let indexPath = NSIndexPath.init(row: index, section: 0)
                                    
                                    let cell:DataListCell = self.judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                                    
                                    cell.entryField.text = ""
                                cell.entryField.isUserInteractionEnabled = false
                                    cell.nameField.text = ""
                                    cell.functionView.backgroundColor = UIColor.clear
                                    cell.functionField.text = ""
                                    cell.smoothView.backgroundColor = UIColor.clear
                                    cell.smoothField.text = ""
                                    cell.threadView.backgroundColor = UIColor.clear
                                    cell.threadField.text = ""
                                    cell.terminoView.backgroundColor = UIColor.clear
                                    cell.terminoField.text = ""
                                    cell.calculateField.text = ""
                                    cell.judgePlaceView.backgroundColor = UIColor.clear
                                    cell.jusgePlaceField.text = ""
                                    
                                }
                                
                                self.judgeCardTable.reloadData()
                                self.judgeCardTable.isHidden = false
                                self.tableHeaderView.isHidden = false
                                
                            }
                        }
                        
                    }
                    catch let JSONError as NSError {
                        print("\(JSONError)")
                        let alert = UIAlertController(title: "Error", message: JSONError.localizedDescription as String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        OperationQueue.main.addOperation
                            {
                                self.CheckLoader()
                                self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
                else if(httpResponse.statusCode == 400)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            self.CheckLoader()
                            
                            
                            self.smoothEditBtn.isUserInteractionEnabled = true
                            self.functionEditBtn.isUserInteractionEnabled = true
                            self.threadEditBtn.isUserInteractionEnabled = true
                            self.terminoEditBtn.isUserInteractionEnabled = true
                            self.horseNameEditBtn.isUserInteractionEnabled = true
                            self.calculateBtn.isUserInteractionEnabled = true
                            
                            self.judgesCardList.removeAllObjects()
                            
                            for index in 0...9
                            {
                                let indexPath = NSIndexPath.init(row: index, section: 0)
                                
                                let cell:DataListCell = self.judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                                
                                cell.entryField.text = ""
                                cell.entryField.isUserInteractionEnabled = false
                                cell.nameField.text = ""
                                cell.functionView.backgroundColor = UIColor.clear
                                cell.functionField.text = ""
                                cell.smoothView.backgroundColor = UIColor.clear
                                cell.smoothField.text = ""
                                cell.threadView.backgroundColor = UIColor.clear
                                cell.threadField.text = ""
                                cell.terminoView.backgroundColor = UIColor.clear
                                cell.terminoField.text = ""
                                cell.calculateField.text = ""
                                cell.judgePlaceView.backgroundColor = UIColor.clear
                                cell.jusgePlaceField.text = ""
                                
                            }
                            
                            self.judgeCardTable.reloadData()
                            self.judgeCardTable.isHidden = false
                            self.tableHeaderView.isHidden = false
                            
                            
                            
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else if(httpResponse.statusCode == 500)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        self.CheckLoader()
                    }
                }
                
            }
            
        })
        
        task.resume()
        
    }
    
    
    // MARK: - Judge's Card Add Edit Api Execute
    func JudgesCardAddEdit(horseId : NSArray)//String)
    {
        DispatchQueue.main.async {
            self.CheckLoader()
        }
        
        
        let  url_to_request = GlobalViewController.GLOBALAPI + "show/judgescardhorseaddoreditnew"
        
        print("Full URL:",url_to_request)
        
        let urlString:NSURL = NSURL(string: url_to_request)!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: urlString as URL)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
//        var parameter = "classid=\(classId)&showid=\(ShowId)&horseid=\(horseId)&sl_no=\(Int(cellIndex)! + 1)"
        
        var parameter = ""
        let horseIDToSend : NSMutableArray = NSMutableArray(array:horseId)
        let serialNoToSend : NSMutableArray = NSMutableArray(array:serialNoArray)
        
        print("parameter:",parameter)
        request.httpMethod = "POST"
        
        
        do {
            
            //Convert to Data
            let jsonData = try JSONSerialization.data(withJSONObject: horseIDToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            let jsonData1 = try JSONSerialization.data(withJSONObject: serialNoToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            //Convert back to string. Usually only do this for debugging
            if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                print(JSONString)
                
                let allowedCharacterSet = CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[] ").inverted
                
                if let escapedString = JSONString.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) {
                    print(escapedString)
                    
                  //  paramString = "updatepreferences=\(escapedString)"
                    
                    parameter = "classid=\(classId)&showid=\(ShowId)&horseid=\(escapedString)"
                    
                }
                
                
            }
            
            //Convert back to string. Usually only do this for debugging
            if let JSONString = String(data: jsonData1, encoding: String.Encoding.utf8) {
                print(JSONString)
                
                let allowedCharacterSet = CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[] ").inverted
                
                if let escapedString = JSONString.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) {
                    print(escapedString)
                    
                    //  paramString = "updatepreferences=\(escapedString)"
                    
                    parameter = "\(parameter)&sl_no=\(escapedString)"
                    
                }
                
                
            }
            
            
            print("param string \(parameter)")
            
        } catch {
            
        }
        
        
        
        request.httpBody = parameter.data(using: String.Encoding.utf8)
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            
            
            (data, response, error) in
            
            
            guard let _:Data = data, let _:URLResponse = response, error == nil
                
                else
            {
                print("error")
                let alert = UIAlertController(title: "Error", message:error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                OperationQueue.main.addOperation
                    {
                        self.CheckLoader()
                        self.present(alert, animated: true, completion: nil)
                }
                return
            }
            //    self.statusCode = [(NSHTTPURLResponse*)response statusCode];
            if let httpResponse = response as? HTTPURLResponse
            {
                
                print("statusCode \(httpResponse.statusCode)")
                if (httpResponse.statusCode == 200)
                {
                    
                    
                    
                    let JSONData = data
                    do {
                        let datastring = NSString(data: JSONData!, encoding: String.Encoding.utf8.rawValue)
                        print("Error Die---\( datastring as! String)")
                        
                        let JSON = try JSONSerialization.jsonObject(with: JSONData!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                        guard let JSONDictionary: NSDictionary = JSON as? NSDictionary else {
                            print("Not a Dictionary")
                            // put in function
                            
                            print("JSONDictionary \(JSON)")
                            DispatchQueue.main.async {
                                self.CheckLoader()
                            }
                            return
                        }
                        
                        print("JSONDictionary \(JSONDictionary)")
                        
                        
                        let status : Bool = JSONDictionary.object(forKey: "status") as! Bool
                        
                        if (status == true)
                        {
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                              //  let details:NSArray = NSMutableArray(array : JSONDictionary.object(forKey: "card_details") as! NSArray)
                                
                                self.judgesCardList = NSMutableArray(array:JSONDictionary.object(forKey: "card_details") as! NSArray)
                                
                                self.judgeCardTable.reloadData()
                                self.judgeCardTable.isHidden = false
                                self.tableHeaderView.isHidden = false
                                
                                print("Add Card:",self.judgesCardList)
                                
                                let alert = UIAlertController(title: nil, message: JSONDictionary.value(forKey: "message") as? String, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        }
                        else
                        {
                            let alert = UIAlertController(title: "Error", message: JSONDictionary.value(forKey: "message") as? String, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            
                            DispatchQueue.main.async {
                                self.present(alert, animated: true, completion: nil)
                                self.CheckLoader()
                                
                            }
                        }
                        
                    }
                    catch let JSONError as NSError {
                        print("\(JSONError)")
                        let alert = UIAlertController(title: "Error", message: JSONError.localizedDescription as String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        OperationQueue.main.addOperation
                            {
                                self.CheckLoader()
                                self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
                else if(httpResponse.statusCode == 400)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else if(httpResponse.statusCode == 500)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        self.CheckLoader()
                    }
                }
                
            }
            
        })
        
        task.resume()
        
    }
    
    
    // MARK: - Category Add Edit Api Execute
    func CategoryAddEdit(categoryId : String, placeValue :  NSArray, editTag : Int )
    {
        DispatchQueue.main.async {
            self.CheckLoader()
        }
        
        
        let  url_to_request = GlobalViewController.GLOBALAPI + "show/judgescardcategoryaddoredit"
        
        print("Full URL:",url_to_request)
        
        let urlString:NSURL = NSURL(string: url_to_request)!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: urlString as URL)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
      //  let parameter = "classid=\(classId)&showid=\(ShowId)&categoryid=\(categoryId)&sl_no=\(Int(cellIndex)! + 1)&place=\(placeValue)"
        
        var parameter = ""
        let placeToSend : NSMutableArray = NSMutableArray(array:placeValue)
        let serialNoToSend : NSMutableArray = NSMutableArray(array:serialNoArray)
        
        print("parameter:",parameter)
        request.httpMethod = "POST"
        
        
        do {
            
            //Convert to Data
            let jsonData = try JSONSerialization.data(withJSONObject: placeToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            let jsonData1 = try JSONSerialization.data(withJSONObject: serialNoToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            //Convert back to string. Usually only do this for debugging
            if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                print(JSONString)
                
                let allowedCharacterSet = CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[] ").inverted
                
                if let escapedString = JSONString.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) {
                    print(escapedString)
                    
                    //  paramString = "updatepreferences=\(escapedString)"
                    
                    
                    parameter = "classid=\(classId)&showid=\(ShowId)&categoryid=\(categoryId)&place=\(escapedString)"
                    
                }
                
                
            }
            
            //Convert back to string. Usually only do this for debugging
            if let JSONString = String(data: jsonData1, encoding: String.Encoding.utf8) {
                print(JSONString)
                
                let allowedCharacterSet = CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[] ").inverted
                
                if let escapedString = JSONString.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) {
                    print(escapedString)
                    
                    //  paramString = "updatepreferences=\(escapedString)"
                    
                    parameter = "\(parameter)&sl_no=\(escapedString)"
                    
                    
                }
                
                
            }
            
            
            print("param string \(parameter)")
            
        } catch {
            
        }
        
        
        request.httpBody = parameter.data(using: String.Encoding.utf8)
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            
            
            (data, response, error) in
            
            
            guard let _:Data = data, let _:URLResponse = response, error == nil
                
                else
            {
                print("error")
                let alert = UIAlertController(title: "Error", message:error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                OperationQueue.main.addOperation
                    {
                        self.CheckLoader()
                        self.present(alert, animated: true, completion: nil)
                }
                return
            }
            //    self.statusCode = [(NSHTTPURLResponse*)response statusCode];
            if let httpResponse = response as? HTTPURLResponse
            {
                
                print("statusCode \(httpResponse.statusCode)")
                if (httpResponse.statusCode == 200)
                {
                    
                    
                    
                    let JSONData = data
                    do {
                        let datastring = NSString(data: JSONData!, encoding: String.Encoding.utf8.rawValue)
                        print("Error Die---\( datastring as! String)")
                        
                        
                        let JSON = try JSONSerialization.jsonObject(with: JSONData!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                        guard let JSONDictionary: NSDictionary = JSON as? NSDictionary else {
                            print("Not a Dictionary")
                            // put in function
                            
                            print("JSONDictionary \(JSON)")
                            DispatchQueue.main.async {
                                self.CheckLoader()
                            }
                            return
                        }
                        
                        print("JSONDictionary \(JSONDictionary)")
                        
                        
                        let status : Bool = JSONDictionary.object(forKey: "status") as! Bool
                        
                        if (status == true)
                        {
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                               // let details:NSMutableArray = NSMutableArray(array : JSONDictionary.object(forKey: "details") as! NSArray)
                                self.judgesCardList = NSMutableArray(array:JSONDictionary.object(forKey: "card_details") as! NSArray)
                                
                                self.judgeCardTable.reloadData()
                                self.judgeCardTable.isHidden = false
                                self.tableHeaderView.isHidden = false
                         
                                
                                let alert = UIAlertController(title: nil, message: JSONDictionary.value(forKey: "message") as? String, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                
                            }
                            
                        }
                        else
                        {
                            let alert = UIAlertController(title: "Error", message: JSONDictionary.value(forKey: "message") as? String, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            
                            DispatchQueue.main.async {
                                self.present(alert, animated: true, completion: nil)
                                self.CheckLoader()
                                
                            }
                        }
                        
                    }
                    catch let JSONError as NSError {
                        print("\(JSONError)")
                        let alert = UIAlertController(title: "Error", message: JSONError.localizedDescription as String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        OperationQueue.main.addOperation
                            {
                                self.CheckLoader()
                                self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
                else if(httpResponse.statusCode == 400)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else if(httpResponse.statusCode == 500)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        self.CheckLoader()
                    }
                }
                
            }
            
        })
        
        task.resume()
        
    }
    
    
    // MARK: - Judge's Place Add Edit Api Execute
    func JudgesPlaceAddEdit(categoryId : String, placeValue :  NSArray)
    {
        DispatchQueue.main.async {
            self.CheckLoader()
        }
        
        
        let  url_to_request = GlobalViewController.GLOBALAPI + "show/judgescardjudgeplaceaddoredit"
        
        print("Full URL:",url_to_request)
        
        let urlString:NSURL = NSURL(string: url_to_request)!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: urlString as URL)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        //  let parameter = "classid=\(classId)&showid=\(ShowId)&categoryid=\(categoryId)&sl_no=\(Int(cellIndex)! + 1)&place=\(placeValue)"
        
        var parameter = ""
        let placeToSend : NSMutableArray = NSMutableArray(array:placeValue)
        let serialNoToSend : NSMutableArray = NSMutableArray(array:serialNoArray)
        
        print("parameter:",parameter)
        request.httpMethod = "POST"
        
        
        do {
            
            //Convert to Data
            let jsonData = try JSONSerialization.data(withJSONObject: placeToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            let jsonData1 = try JSONSerialization.data(withJSONObject: serialNoToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            //Convert back to string. Usually only do this for debugging
            if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                print(JSONString)
                
                let allowedCharacterSet = CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[] ").inverted
                
                if let escapedString = JSONString.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) {
                    print(escapedString)
                    
                    //  paramString = "updatepreferences=\(escapedString)"
                    
                    
                    parameter = "classid=\(classId)&showid=\(ShowId)&judgeplace=\(escapedString)"
                    
                }
                
                
            }
            
            //Convert back to string. Usually only do this for debugging
            if let JSONString = String(data: jsonData1, encoding: String.Encoding.utf8) {
                print(JSONString)
                
                let allowedCharacterSet = CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[] ").inverted
                
                if let escapedString = JSONString.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) {
                    print(escapedString)
                    
                    //  paramString = "updatepreferences=\(escapedString)"
                    
                    parameter = "\(parameter)&sl_no=\(escapedString)"
                    
                    
                }
                
                
            }
            
            
            print("param string \(parameter)")
            
        } catch {
            
        }
        
        
        request.httpBody = parameter.data(using: String.Encoding.utf8)
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            
            
            (data, response, error) in
            
            
            guard let _:Data = data, let _:URLResponse = response, error == nil
                
                else
            {
                print("error")
                let alert = UIAlertController(title: "Error", message:error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                OperationQueue.main.addOperation
                    {
                        self.CheckLoader()
                        self.present(alert, animated: true, completion: nil)
                }
                return
            }
            //    self.statusCode = [(NSHTTPURLResponse*)response statusCode];
            if let httpResponse = response as? HTTPURLResponse
            {
                
                print("statusCode \(httpResponse.statusCode)")
                if (httpResponse.statusCode == 200)
                {
                    
                    
                    
                    let JSONData = data
                    do {
                        let datastring = NSString(data: JSONData!, encoding: String.Encoding.utf8.rawValue)
                        print("Error Die---\( datastring as! String)")
                        
                        
                        let JSON = try JSONSerialization.jsonObject(with: JSONData!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                        guard let JSONDictionary: NSDictionary = JSON as? NSDictionary else {
                            print("Not a Dictionary")
                            // put in function
                            
                            print("JSONDictionary \(JSON)")
                            DispatchQueue.main.async {
                                self.CheckLoader()
                            }
                            return
                        }
                        
                        print("JSONDictionary \(JSONDictionary)")
                        
                        
                        let status : Bool = JSONDictionary.object(forKey: "status") as! Bool
                        
                        if (status == true)
                        {
                            
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                                // let details:NSDictionary = JSONDictionary.object(forKey: "card_details") as! NSDictionary
                                
                                
//                                for index in 0...9
//                                {
//                                    let indexPath = NSIndexPath.init(row: index, section: 0)
//
//                                    let cell:DataListCell = self.judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
//
//                                    cell.entryField.text = ""
//                                    cell.entryField.isUserInteractionEnabled = false
//                                    cell.nameField.text = ""
//                                    cell.functionView.backgroundColor = UIColor.clear
//                                    cell.functionField.text = ""
//                                    cell.smoothView.backgroundColor = UIColor.clear
//                                    cell.smoothField.text = ""
//                                    cell.threadView.backgroundColor = UIColor.clear
//                                    cell.threadField.text = ""
//                                    cell.terminoView.backgroundColor = UIColor.clear
//                                    cell.terminoField.text = ""
//                                    cell.calculateField.text = ""
//                                    cell.judgePlaceView.backgroundColor = UIColor.clear
//                                    cell.jusgePlaceField.text = ""
//
//                                }
                                
                                self.smoothEditBtn.isUserInteractionEnabled = true
                                self.functionEditBtn.isUserInteractionEnabled = true
                                self.threadEditBtn.isUserInteractionEnabled = true
                                self.terminoEditBtn.isUserInteractionEnabled = true
                                self.horseNameEditBtn.isUserInteractionEnabled = true
                                self.calculateBtn.isUserInteractionEnabled = true
                                
                                
                                
//                                self.judgesCardList = NSMutableArray(array:JSONDictionary.object(forKey: "card_details") as! NSArray)
                                
                                print("Card Fetch:",JSONDictionary.object(forKey: "card_details") as! NSArray)
                                
                                
                                if (JSONDictionary.object(forKey: "iscalculate") as! Bool)
                                {
                                    self.isCalculate = true
                                    self.smoothEditBtn.isUserInteractionEnabled = false
                                    self.functionEditBtn.isUserInteractionEnabled = false
                                    self.threadEditBtn.isUserInteractionEnabled = false
                                    self.terminoEditBtn.isUserInteractionEnabled = false
                                    self.horseNameEditBtn.isUserInteractionEnabled = false
                                    self.calculateBtn.isUserInteractionEnabled = false
                                    
                                    
                                    self.placeHeader.isHidden = true
                                    self.entryHeader.isHidden = true
                                    self.functionHeader.isHidden = true
                                    self.smoothHeader.isHidden = true
                                    self.threadHeader.isHidden = true
                                    self.terminoHeader.isHidden = true
                                    self.calculateHeader.isHidden = true
                                    self.saveHeader.isHidden = false
                                    
                                    self.printBtn.isHidden = true
                                    self.PrintLbl.isHidden = true
                                    
                                }
                                else
                                {
                                    self.isCalculate = false
                                    self.placeHeader.isHidden = false
                                    
                                    self.entryHeader.isHidden = false
                                    
                                    self.functionHeader.isHidden = false
                                    
                                    self.smoothHeader.isHidden = false
                                    
                                    self.threadHeader.isHidden = false
                                    
                                    self.terminoHeader.isHidden = false
                                    
                                    self.calculateHeader.isHidden = false
                                    self.saveHeader.isHidden = false
                                    
                                    self.printBtn.isHidden = true
                                    self.PrintLbl.isHidden = true
                                }
                                
                                
                                
                                if (JSONDictionary.object(forKey: "isjudgeplace") as! Bool)
                                {
                                    self.placeHeader.isHidden = true
                                    self.entryHeader.isHidden = true
                                    self.functionHeader.isHidden = true
                                    self.smoothHeader.isHidden = true
                                    self.threadHeader.isHidden = true
                                    self.terminoHeader.isHidden = true
                                    self.calculateHeader.isHidden = true
                                    self.saveHeader.isHidden = true
                                    
                                    self.printBtn.isHidden = false
                                    self.PrintLbl.isHidden = false
                                }
 
                                
//                                self.judgeCardTable.reloadData()
//                                self.judgeCardTable.isHidden = false
//                                self.tableHeaderView.isHidden = false
                                
                                

//                                self.placeHeader.isHidden = true
//                                self.entryHeader.isHidden = true
//                                self.functionHeader.isHidden = true
//                                self.smoothHeader.isHidden = true
//                                self.threadHeader.isHidden = true
//                                self.terminoHeader.isHidden = true
//                                self.calculateHeader.isHidden = true
//                                self.saveHeader.isHidden = true
//
//                                self.printBtn.isHidden = false
//                                self.PrintLbl.isHidden = false
                                
                                let alert = UIAlertController(title: nil, message: JSONDictionary.value(forKey: "message") as? String, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                
                            }
                            
                        }
                        else
                        {
                            let alert = UIAlertController(title: "Error", message: JSONDictionary.value(forKey: "message") as? String, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            
                            DispatchQueue.main.async {
                                self.present(alert, animated: true, completion: nil)
                                self.CheckLoader()
                                
                            }
                        }
                        
                    }
                    catch let JSONError as NSError {
                        print("\(JSONError)")
                        let alert = UIAlertController(title: "Error", message: JSONError.localizedDescription as String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        OperationQueue.main.addOperation
                            {
                                self.CheckLoader()
                                self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
                else if(httpResponse.statusCode == 400)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else if(httpResponse.statusCode == 500)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        self.CheckLoader()
                    }
                }
                
            }
            
        })
        
        task.resume()
        
    }
    
    
    
    // MARK: - Judge's Card Calculation Api Execute
    @objc func calculateTotal()
    {
        DispatchQueue.main.async {
            self.CheckLoader()
        }
        
        
        let  url_to_request = GlobalViewController.GLOBALAPI + "show/calculatetotal"
        
        print("Full URL:",url_to_request)
        
        let urlString:NSURL = NSURL(string: url_to_request)!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: urlString as URL)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        let parameter = "classid=\(classId)&showid=\(ShowId)"
        
        print("parameter:",parameter)
        request.httpMethod = "POST"
        request.httpBody = parameter.data(using: String.Encoding.utf8)
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            
            
            (data, response, error) in
            
            
            guard let _:Data = data, let _:URLResponse = response, error == nil
                
                else
            {
                print("error")
                let alert = UIAlertController(title: "Error", message:error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                OperationQueue.main.addOperation
                    {
                        self.CheckLoader()
                        self.present(alert, animated: true, completion: nil)
                }
                return
            }
            if let httpResponse = response as? HTTPURLResponse
            {
                
                
                print("statusCode \(httpResponse.statusCode)")
                if (httpResponse.statusCode == 200)
                {
                    
                    
                    
                    let JSONData = data
                    do {
                        let JSON = try JSONSerialization.jsonObject(with: JSONData!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                        guard let JSONDictionary: NSDictionary = JSON as? NSDictionary else {
                            print("Not a Dictionary")
                            // put in function
                            
                            print("JSONDictionary \(JSON)")
                            DispatchQueue.main.async {
                                self.CheckLoader()
                            }
                            return
                        }
                        
                        print("JSONDictionary \(JSONDictionary)")
                        
                        
                        let status : Bool = JSONDictionary.object(forKey: "status") as! Bool
                        
                        if (status == true)
                        {
                            
                            
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                                // let details:NSDictionary = JSONDictionary.object(forKey: "card_details") as! NSDictionary
                                
                                
                                for index in 0...9
                                {
                                    let indexPath = NSIndexPath.init(row: index, section: 0)
                                    
                                    let cell:DataListCell = self.judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
                                    
                                    cell.entryField.text = ""
                                    cell.entryField.isUserInteractionEnabled = false
                                    cell.nameField.text = ""
                                    cell.functionView.backgroundColor = UIColor.clear
                                    cell.functionField.text = ""
                                    cell.smoothView.backgroundColor = UIColor.clear
                                    cell.smoothField.text = ""
                                    cell.threadView.backgroundColor = UIColor.clear
                                    cell.threadField.text = ""
                                    cell.terminoView.backgroundColor = UIColor.clear
                                    cell.terminoField.text = ""
                                    cell.calculateField.text = ""
                                    cell.judgePlaceView.backgroundColor = UIColor.clear
                                    cell.jusgePlaceField.text = ""
                                    
                                }
                                
                                self.smoothEditBtn.isUserInteractionEnabled = true
                                self.functionEditBtn.isUserInteractionEnabled = true
                                
                                self.threadEditBtn.isUserInteractionEnabled = true
                                self.terminoEditBtn.isUserInteractionEnabled = true
                                self.horseNameEditBtn.isUserInteractionEnabled = true
                                self.calculateBtn.isUserInteractionEnabled = true
                                
                                
                                
                                self.judgesCardList = NSMutableArray(array:JSONDictionary.object(forKey: "card_details") as! NSArray)
                                
                                print("Card Fetch:",JSONDictionary.object(forKey: "card_details") as! NSArray)
                                
                                
                                if (JSONDictionary.object(forKey: "iscalculate") as! Bool)
                                {
                                   self.isCalculate = true
                                self.smoothEditBtn.isUserInteractionEnabled = false
                                    
                                    self.functionEditBtn.isUserInteractionEnabled = false
                                    self.threadEditBtn.isUserInteractionEnabled = false
                                    self.terminoEditBtn.isUserInteractionEnabled = false
                                    self.horseNameEditBtn.isUserInteractionEnabled = false
                                    self.calculateBtn.isUserInteractionEnabled = false
                                    
                                    self.placeHeader.isHidden = true
                                    self.entryHeader.isHidden = true
                                    self.functionHeader.isHidden = true
                                    self.smoothHeader.isHidden = true
                                    self.threadHeader.isHidden = true
                                    self.terminoHeader.isHidden = true
                                    self.calculateHeader.isHidden = true
                                    self.saveHeader.isHidden = false
                                    
                                    self.printBtn.isHidden = true
                                    self.PrintLbl.isHidden = true
                                    
                                }
                                else
                                {
                                   self.isCalculate = false
                                    self.placeHeader.isHidden = false
                                    
                                    self.entryHeader.isHidden = false
                                    
                                    self.functionHeader.isHidden = false
                                    
                                    self.smoothHeader.isHidden = false
                                    
                                    self.threadHeader.isHidden = false
                                    
                                    self.terminoHeader.isHidden = false
                                    
                                    self.calculateHeader.isHidden = false
                                    self.saveHeader.isHidden = false
                                    
                                    self.printBtn.isHidden = true
                                    self.PrintLbl.isHidden = true
                                }
                                
                                
                                
                                if (JSONDictionary.object(forKey: "isjudgeplace") as! Bool)
                                {
                                    self.placeHeader.isHidden = true
                                    self.entryHeader.isHidden = true
                                    self.functionHeader.isHidden = true
                                    self.smoothHeader.isHidden = true
                                    self.threadHeader.isHidden = true
                                    self.terminoHeader.isHidden = true
                                    self.calculateHeader.isHidden = true
                                    self.saveHeader.isHidden = true
                                    
                                    self.printBtn.isHidden = false
                                    self.PrintLbl.isHidden = false
                                }

                                
                                self.judgeCardTable.reloadData()
                                self.judgeCardTable.isHidden = false
                                self.tableHeaderView.isHidden = false
                                
                            }
                            
                            
//                            DispatchQueue.main.async {
//                                self.CheckLoader()
//
//
//
//                                var indexCount = 0
//
//
//                            let calculateArray = JSONDictionary.object(forKey: "total") as! NSArray
//
//                            self.smoothEditBtn.isUserInteractionEnabled = false
//                            self.functionEditBtn.isUserInteractionEnabled = false
//                            self.threadEditBtn.isUserInteractionEnabled = false
//                            self.terminoEditBtn.isUserInteractionEnabled = false
//                            self.horseNameEditBtn.isUserInteractionEnabled = false
//                            self.calculateBtn.isUserInteractionEnabled = false
//
//                            for index in 0...9
//                            {
//                                    let indexPath = NSIndexPath.init(row: index, section: 0)
//
//                                    let cell:DataListCell = self.judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
//
//                                    cell.calculateField.text = String(calculateArray.object(at: index) as! Int)
//
//                            }
//
//                                print("After Calculation:",self.judgesCardList)
//
//                              //  self.judgeCardTable.reloadData()
//                                self.judgeCardTable.isHidden = false
//                                self.tableHeaderView.isHidden = false
//
//                            }
                            
                        }
                        else
                        {
                            let alert = UIAlertController(title: "Error", message: JSONDictionary.value(forKey: "message") as? String, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            
                            DispatchQueue.main.async
                                {
                                    // self.present(alert, animated: true, completion: nil)
                                    self.CheckLoader()
                                    
                                    self.judgesCardList.removeAllObjects()
                                    
                                    self.judgeCardTable.reloadData()
                                    self.judgeCardTable.isHidden = false
                                    self.tableHeaderView.isHidden = false
                                    
                            }
                        }
                        
                    }
                    catch let JSONError as NSError {
                        print("\(JSONError)")
                        let alert = UIAlertController(title: "Error", message: JSONError.localizedDescription as String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        OperationQueue.main.addOperation
                            {
                                self.CheckLoader()
                                self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
                else if(httpResponse.statusCode == 400)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else if(httpResponse.statusCode == 500)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        self.CheckLoader()
                    }
                }
                
            }
            
        })
        
        task.resume()
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Show Click
    @IBAction func showClick(_ sender: Any)
    {
        ShowView.removeFromSuperview()
        show = true
        className = false
        
        if showList.count>0
        {
            ShowView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
            ShowView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            ShowView.center = self.view.center
            self.view.addSubview(ShowView)
            
            
            let typeView = UIView(frame: CGRect(x: 0, y: ShowView.frame.height-250, width: ShowView.frame.width, height: 250))
            typeView.backgroundColor = UIColor.white
            ShowView.addSubview(typeView)
            
            // Create a Picker
            let pickerView = UIPickerView()
            
            pickerView.delegate = self
            pickerView.dataSource = self as! UIPickerViewDataSource
            
            
            pickerView.frame = CGRect(x: 10, y: 50, width: ShowView.frame.width-20, height: 200)
            pickerView.backgroundColor = UIColor.white
            
            
            // Add DataPicker to the view
            typeView.addSubview(pickerView)
            
            let savebtn:UIButton = UIButton(frame: CGRect(x: typeView.frame.size.width-60, y: pickerView.frame.origin.y-50, width: 50, height: 50))
            savebtn.setTitleColor(GlobalViewController.ThemeColor, for: .normal)
            savebtn.setTitle("Save", for: .normal)
            savebtn.addTarget(self, action:#selector(NPCategoryByDivisionEditViewController.ShowSaveClick), for: .touchUpInside)
            //  savebtn.tag = (sender as AnyObject).tag
            typeView.addSubview(savebtn)
            
            let cancelbtn:UIButton = UIButton(frame: CGRect(x: 10, y: pickerView.frame.origin.y-50, width: 80, height: 50))
            cancelbtn.setTitleColor(GlobalViewController.ThemeColor, for: .normal)
            cancelbtn.setTitle("Cancel", for: .normal)
            cancelbtn.addTarget(self, action:#selector(NPCategoryByDivisionEditViewController.ShowCancelClick), for: .touchUpInside)
            typeView.addSubview(cancelbtn)
            

        }
        else
        {
            classShowList()
        }
        
        
    }
    
    
    @objc func ShowSaveClick()
    {
        ShowView.removeFromSuperview()
        if show
        {
            show = false
            
            if (showLbl.text == "Choose Show Name")
            {
                showLbl.textColor = UIColor.red
            }
            else
            {
               UserDefaults.standard.set(showLbl.text as! String, forKey: "ShowName")
                UserDefaults.standard.set(ShowId, forKey: "ShowID")
                HeaderView.showNameHeader.text = showLbl.text as! String
                
                
                let object:NSDictionary = showList.object(at: showIndex) as! NSDictionary
                
                
                let temp:NSArray = object.object(forKey: "judge") as! NSArray
                UserDefaults.standard.set(judgeList.mutableCopy() as! NSMutableArray, forKey: "JudgeArray")
                
                UserDefaults.standard.set(object.object(forKey: "default_judge") as! NSInteger ,forKey: "defaultJudge")
                
                showBtn.isUserInteractionEnabled = false
                dropDownImgVw.isHidden = true
                
            }
            
        }
        else if className
        {
            UIView.animate(withDuration: 1.5, delay: 0, options: UIViewAnimationOptions.transitionCurlUp, animations: {
                self.mainScroll.contentOffset.y = 0
            }, completion: nil)
            
            className = false
           if (categoryList.count > 0)
            {
            for index in 0...3
            {
                let object : NSDictionary = categoryList.object(at: index) as! NSDictionary
               if index == 0
               {
                    category1.text = "\(object.object(forKey: "categoryname") as! String)"
                    category1Weight.text = "\(object.object(forKey: "weight") as! String)%"
                    category1Id = object.object(forKey: "categoryid") as! String
                
                
                    if (object.object(forKey: "weight") as! String == "0")
                    {
                        functionEdit.isHidden = true
                        functionEditBtn.isHidden = true
                        category1Not = false
                    }
                    else
                    {
                        functionEdit.isHidden = false
                        functionEditBtn.isHidden = false
                        category1Not = true
                    }
                
               }
               else if index == 1
               {
                    category2.text = "\(object.object(forKey: "categoryname") as! String)"
                
                    category2Weight.text = "\(object.object(forKey: "weight") as! String)%"
                    category2Id = object.object(forKey: "categoryid") as! String
                
                    if (object.object(forKey: "weight") as! String == "0")
                    {
                        smoothEdit.isHidden = true
                        smoothEditBtn.isHidden = true
                        category2Not = false
                    }
                    else
                    {
                        smoothEdit.isHidden = false
                        smoothEditBtn.isHidden = false
                        category2Not = true
                    }
                
                
               }
               else if index == 2
               {
                    category3.text = "\(object.object(forKey: "categoryname") as! String)"
                
                    category3Weight.text = "\(object.object(forKey: "weight") as! String)%"
                    category3Id = object.object(forKey: "categoryid") as! String
                
                    if (object.object(forKey: "weight") as! String == "0")
                    {
                        threadEdit.isHidden = true
                        threadEditBtn.isHidden = true
                        category3Not = false
                    }
                    else
                    {
                        threadEdit.isHidden = false
                        threadEditBtn.isHidden = false
                        category3Not = true
                    }
                
               }
               else if index == 3
               {
                    category4.text = "\(object.object(forKey: "categoryname") as! String)"
                
                    category4Weight.text = "\(object.object(forKey: "weight") as! String)%"
                    category4Id = object.object(forKey: "categoryid") as! String
                
                
                    if (object.object(forKey: "weight") as! String == "0")
                    {
                        terminoEdit.isHidden = true
                        terminoEditBtn.isHidden = true
                        category4Not = false
                    }
                    else
                    {
                        terminoEdit.isHidden = false
                        terminoEditBtn.isHidden = false
                        category4Not = true
                    }
               }
            }
                
                
                

                
                let object:NSDictionary = classList.object(at: classIndex) as! NSDictionary
                
                appDel.className =  object.object(forKey: "classname") as! String
                appDel.classNumber = object.object(forKey: "classnumber") as! String
                appDel.ClassID = object.object(forKey: "id") as! String
                appDel.divisionName = object.object(forKey: "divisionname") as! String
                appDel.judgeName = object.object(forKey: "judgename") as! String
                
                classId = object.object(forKey: "id") as! String
                
                classLbl.textColor = UIColor.black
                classLbl.text = "CLASS #: \( object.object(forKey: "classnumber") as! String)"
                nameLbl.text = "\( object.object(forKey: "classname") as! String)"
                divisionLbl.text = "DIVISION:  \( object.object(forKey: "divisionname") as! String)"
                divisionId = object.object(forKey: "division") as! String
                defaultJudgeLbl.text = "JUDGE:  \( object.object(forKey: "judgename") as! String)"
                judgeId = object.object(forKey: "judge") as! String
    
            FetchJudgesCard()
            
            
        }
            
            
        }
        
        
    }
    
    // MARK: - cancel click
    @objc func ShowCancelClick()
    {
        
        // ShowName = ""
        
        if show
        {
            show = false
            
            if (UserDefaults.standard.object(forKey: "ShowName") != nil && UserDefaults.standard.object(forKey: "ShowName") as! String != "")
            {
                HeaderView.showNameHeader.text = UserDefaults.standard.object(forKey: "ShowName") as! String
                ShowId = UserDefaults.standard.object(forKey: "ShowID") as! String
            }
            else
            {
                HeaderView.showNameHeader.text = ""
                showLbl.text = "Choose Show Name"
                
            }
            showLbl.textColor = UIColor.black
            ShowView.removeFromSuperview()
        }
        else if className
        {
            className = false
           
   
            
            if(appDel.classNumber != "")
            {
                classLbl.text = "CLASS #: \(appDel.classNumber)"
            }
            else
            {
                classLbl.text = "CLASS #:"
            }
            
            if (appDel.className != "")
            {
                nameLbl.text = "\(appDel.className)"
            }
            else
            {
                nameLbl.text = "" //"NAME:"
            }
            
            if (appDel.divisionName != "")
            {
                divisionLbl.text = "DIVISION:  \(appDel.divisionName)"
            }
            else
            {
                divisionLbl.text = "DIVISION:"
            }
            
            
            if (appDel.judgeName != "")
            {
                defaultJudgeLbl.text = "JUDGE:  \(appDel.judgeName)"
            }
            else
            {
                defaultJudgeLbl.text = "JUDGE:"
            }
            
            
            if (appDel.ClassID != "")
            {
                classId = appDel.ClassID
            }
            else
            {
                classId = ""
            }
            
            
//            divisionId = ""
//
//            judgeId = ""
//             classId = ""
            categoryList.removeAllObjects()
            
            ShowView.removeFromSuperview()
            
        }
        
        
    }
    
    
    // MARK: - Class Click
    @IBAction func classClick(_ sender: Any)
    {
        
        if showLbl.text == "Choose Show Name"
        {
            showLbl.textColor = UIColor.red
        }
        else
        {
            ShowView.removeFromSuperview()
            show = false
            className = true
            
//            if classList.count>0
//            {
//                ShowView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
//                ShowView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
//                ShowView.center = self.view.center
//                self.view.addSubview(ShowView)
//
//
//                let typeView = UIView(frame: CGRect(x: 0, y: ShowView.frame.height-250, width: ShowView.frame.width, height: 250))
//                typeView.backgroundColor = UIColor.white
//                ShowView.addSubview(typeView)
//
//                // Create a Picker
//                let pickerView = UIPickerView()
//
//                pickerView.delegate = self
//                pickerView.dataSource = self as! UIPickerViewDataSource
//
//
//                pickerView.frame = CGRect(x: 10, y: 50, width: ShowView.frame.width-20, height: 200)
//                pickerView.backgroundColor = UIColor.white
//
//
//                // Add DataPicker to the view
//                typeView.addSubview(pickerView)
//
//                let savebtn:UIButton = UIButton(frame: CGRect(x: typeView.frame.size.width-60, y: pickerView.frame.origin.y-50, width: 50, height: 50))
//                savebtn.setTitleColor(GlobalViewController.ThemeColor, for: .normal)
//                savebtn.setTitle("Save", for: .normal)
//                savebtn.addTarget(self, action:#selector(NPCategoryByDivisionEditViewController.ShowSaveClick), for: .touchUpInside)
//                //  savebtn.tag = (sender as AnyObject).tag
//                typeView.addSubview(savebtn)
//
//                let cancelbtn:UIButton = UIButton(frame: CGRect(x: 10, y: pickerView.frame.origin.y-50, width: 80, height: 50))
//                cancelbtn.setTitleColor(GlobalViewController.ThemeColor, for: .normal)
//                cancelbtn.setTitle("Cancel", for: .normal)
//                cancelbtn.addTarget(self, action:#selector(NPCategoryByDivisionEditViewController.ShowCancelClick), for: .touchUpInside)
//                typeView.addSubview(cancelbtn)
//
//
//            }
//            else
//            {
                ClassListAPI()
//            }
            
            
        }
    }
    
    
    // MARK: - Show List Api Execute
    func classShowList()
    {
        
        DispatchQueue.main.async {
            self.CheckLoader()
        }
        
        let  url_to_request = GlobalViewController.GLOBALAPI + "show/fetchparticularshowjudge"
        
        print("Full URL:",url_to_request)
        
        let urlString:NSURL = NSURL(string: url_to_request)!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: urlString as URL)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        var parameter : String = ""
        
        parameter = "showid="
        
        print("parameter:",parameter)
        request.httpMethod = "POST"
        request.httpBody = parameter.data(using: String.Encoding.utf8)
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            
            
            (data, response, error) in
            
            
            guard let _:Data = data, let _:URLResponse = response, error == nil
                
                else
            {
                print("error")
                let alert = UIAlertController(title: "Error", message:error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                OperationQueue.main.addOperation
                    {
                        self.CheckLoader()
                        self.present(alert, animated: true, completion: nil)
                }
                return
            }
            //    self.statusCode = [(NSHTTPURLResponse*)response statusCode];
            if let httpResponse = response as? HTTPURLResponse
            {
                
                
                print("statusCode \(httpResponse.statusCode)")
                if (httpResponse.statusCode == 200)
                {
                    
                    let JSONData = data
                    do {
                        let JSON = try JSONSerialization.jsonObject(with: JSONData!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                        guard let JSONDictionary: NSDictionary = JSON as? NSDictionary else {
                            print("Not a Dictionary")
                            // put in function
                            
                            print("JSONDictionary \(JSON)")
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                                
                            }
                            return
                        }
                        
                        print("JSONDictionary \(JSONDictionary)")
                        
                        
                        let status : Bool = JSONDictionary.object(forKey: "status") as! Bool
                        
                        
                        if (status)
                        {
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                                let temparray = JSONDictionary.object(forKey: "details") as! NSArray
                                
                           //     self.divisionList = NSMutableArray(array:JSONDictionary.object(forKey: "divisions") as! NSArray)
                                
                                
                                
                                self.showList = temparray.mutableCopy() as! NSMutableArray
                                
                                if self.showList.count > 0
                                {
                                    self.ShowView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                                    self.ShowView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
                                    self.ShowView.center = self.view.center
                                    self.view.addSubview(self.ShowView)
                                    
                                    let typeView = UIView(frame: CGRect(x: 0, y: self.ShowView.frame.height-250, width: self.ShowView.frame.width, height: 250))
                                    typeView.backgroundColor = UIColor.white
                                    self.ShowView.addSubview(typeView)
                                    
                                    // Create a Picker
                                    let pickerView = UIPickerView()
                                    
                                    pickerView.delegate = self
                                    pickerView.dataSource = self as UIPickerViewDataSource
                                    
                                    
                                    pickerView.frame = CGRect(x: 10, y: 50, width: self.ShowView.frame.width-20, height: 200)
                                    pickerView.backgroundColor = UIColor.white
                                    
                                    
                                    // Add DataPicker to the view
                                    typeView.addSubview(pickerView)
                                    
                                    let savebtn:UIButton = UIButton(frame: CGRect(x: typeView.frame.size.width-60, y: pickerView.frame.origin.y-50, width: 50, height: 50))
                                    savebtn.setTitleColor(GlobalViewController.ThemeColor, for: .normal)
                                    savebtn.setTitle("Save", for: .normal)
                                    savebtn.addTarget(self, action:#selector(NPCategoryByDivisionEditViewController.ShowSaveClick), for: .touchUpInside)
                                    // savebtn.tag = (sender as AnyObject).tag
                                    typeView.addSubview(savebtn)
                                    
                                    let cancelbtn:UIButton = UIButton(frame: CGRect(x: 10, y: pickerView.frame.origin.y-50, width: 80, height: 50))
                                    cancelbtn.setTitleColor(GlobalViewController.ThemeColor, for: .normal)
                                    cancelbtn.setTitle("Cancel", for: .normal)
                                    cancelbtn.addTarget(self, action:#selector(NPCategoryByDivisionEditViewController.ShowCancelClick), for: .touchUpInside)
                                    // cancelbtn.tag = (sender as AnyObject).tag
                                    typeView.addSubview(cancelbtn)
                                    
                              
                                }
                                else
                                {
                                    self.showList.removeAllObjects()
                                }
                                
                                
                            }
                            
                            
                            
                        }
                        else
                        {
                            
                            
                            DispatchQueue.main.async {
                                
                                self.showList.removeAllObjects()
                                //  self.ShowTable.isHidden = true
                                
                                self.CheckLoader()
                                
                                
                            }
                        }
                        
                        
                    }
                    catch let JSONError as NSError {
                        let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                        print("Error---\(String(describing: datastring))")
                        print("Error---\(String(describing: data))")
                        print("Error mesg---\(JSONError)")
                        
                        let alert = UIAlertController(title: "Error", message: JSONError.localizedDescription as String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        OperationQueue.main.addOperation
                            {
                                self.classList.removeAllObjects()
                                // self.ShowTable.isHidden = true
                                self.CheckLoader()
                                self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                    
                }
                else if(httpResponse.statusCode == 400)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            // self.ShowTable.isHidden = true
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else if(httpResponse.statusCode == 500)
                {
                    let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print("Error---\(String(describing: datastring))")
                    
                    
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            //  self.ShowTable.isHidden = true
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        //  self.ShowTable.isHidden = true
                        self.CheckLoader()
                    }
                }
                
                
            }
            
            
        })
        
        
        task.resume()
        
        
    }
    
    
    // MARK: - Class List Api Execute
    func ClassListAPI()
    {
        
        DispatchQueue.main.async {
            self.CheckLoader()
        }
        
        let  url_to_request = GlobalViewController.GLOBALAPI + "show/fetchclass"
        
        print("Full URL:",url_to_request)
        
        let urlString:NSURL = NSURL(string: url_to_request)!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: urlString as URL)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        var parameter : String = ""
        
        parameter = "showid=\(ShowId)"
        
        print("parameter:",parameter)
        request.httpMethod = "POST"
        request.httpBody = parameter.data(using: String.Encoding.utf8)
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            
            
            (data, response, error) in
            
            
            guard let _:Data = data, let _:URLResponse = response, error == nil
                
                else
            {
                print("error")
                let alert = UIAlertController(title: "Error", message:error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                OperationQueue.main.addOperation
                    {
                        self.CheckLoader()
                        self.present(alert, animated: true, completion: nil)
                }
                return
            }
            //    self.statusCode = [(NSHTTPURLResponse*)response statusCode];
            if let httpResponse = response as? HTTPURLResponse
            {
                
                
                print("statusCode \(httpResponse.statusCode)")
                if (httpResponse.statusCode == 200)
                {
                    
                    let JSONData = data
                    do {
                        let JSON = try JSONSerialization.jsonObject(with: JSONData!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                        guard let JSONDictionary: NSDictionary = JSON as? NSDictionary else {
                            print("Not a Dictionary")
                            // put in function
                            
                            print("JSONDictionary \(JSON)")
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                                
                            }
                            return
                        }
                        
                        print("JSONDictionary \(JSONDictionary)")
                        
                        
                        let status : Bool = JSONDictionary.object(forKey: "status") as! Bool
                        
                        
                        if (status)
                        {
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                                let temparray = JSONDictionary.object(forKey: "details") as! NSArray
                                
                                
                                self.classList = temparray.mutableCopy() as! NSMutableArray
                                
                                if self.classList.count > 0
                                {
                                    self.ShowView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                                    self.ShowView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
                                    self.ShowView.center = self.view.center
                                    self.view.addSubview(self.ShowView)
                                    
                                    
                                    let typeView = UIView(frame: CGRect(x: 0, y: self.ShowView.frame.height-250, width: self.ShowView.frame.width, height: 250))
                                    typeView.backgroundColor = UIColor.white
                                    self.ShowView.addSubview(typeView)
                                    
                                    // Create a Picker
                                    let pickerView = UIPickerView()
                                    
                                    pickerView.delegate = self
                                    pickerView.dataSource = self as! UIPickerViewDataSource
                                    
                                    
                                    pickerView.frame = CGRect(x: 10, y: 50, width: self.ShowView.frame.width-20, height: 200)
                                    pickerView.backgroundColor = UIColor.white
                                    
                                    
                                    // Add DataPicker to the view
                                    typeView.addSubview(pickerView)
                                    
                                    let savebtn:UIButton = UIButton(frame: CGRect(x: typeView.frame.size.width-60, y: pickerView.frame.origin.y-50, width: 50, height: 50))
                                    savebtn.setTitleColor(GlobalViewController.ThemeColor, for: .normal)
                                    savebtn.setTitle("Save", for: .normal)
                                    savebtn.addTarget(self, action:#selector(NPCategoryByDivisionEditViewController.ShowSaveClick), for: .touchUpInside)
                                    //  savebtn.tag = (sender as AnyObject).tag
                                    typeView.addSubview(savebtn)
                                    
                                    let cancelbtn:UIButton = UIButton(frame: CGRect(x: 10, y: pickerView.frame.origin.y-50, width: 80, height: 50))
                                    cancelbtn.setTitleColor(GlobalViewController.ThemeColor, for: .normal)
                                    cancelbtn.setTitle("Cancel", for: .normal)
                                    cancelbtn.addTarget(self, action:#selector(NPCategoryByDivisionEditViewController.ShowCancelClick), for: .touchUpInside)
                                    typeView.addSubview(cancelbtn)
                                    
                                    
                                }
                                else
                                {
                                    self.classList.removeAllObjects()
                                }
                                
                            }
                            
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                
                                self.classList.removeAllObjects()
                                self.CheckLoader()
                                
                                
                            }
                        }
                        
                        
                    }
                    catch let JSONError as NSError {
                        let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                        print("Error---\(String(describing: datastring))")
                        print("Error---\(String(describing: data))")
                        print("Error mesg---\(JSONError)")
                        
                        let alert = UIAlertController(title: "Error", message: JSONError.localizedDescription as String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        OperationQueue.main.addOperation
                            {
                                
                                self.classList.removeAllObjects()
                                self.CheckLoader()
                                self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                    
                }
                else if(httpResponse.statusCode == 400)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            // self.ShowTable.isHidden = true
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else if(httpResponse.statusCode == 500)
                {
                    let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print("Error---\(String(describing: datastring))")
                    
                    
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            //  self.ShowTable.isHidden = true
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        //  self.ShowTable.isHidden = true
                        self.CheckLoader()
                    }
                }
                
                
            }
            
            
        })
        
        
        task.resume()
        
        
    }
    
    
    // MARK: - UIpicker Delegates
    // The number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if show
        {
            return showList.count
        }
        else if className
        {
            return classList.count
        }
        else
        {
            return judgeList.count
        }
        
        // return ShowListArray.count
        
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if show
        {
            let object:NSDictionary = showList.object(at: row) as! NSDictionary
            
            return object.object(forKey: "showname") as! String
            
        }
        else if className
        {
            let object:NSDictionary = classList.object(at: row) as! NSDictionary
            
            return object.object(forKey: "classnumber") as! String
        }
        else
        {
            let object:NSDictionary = judgeList.object(at: row) as! NSDictionary
            
            return object.object(forKey: "judgename") as! String
        }
    
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
        if show
        {
            let object:NSDictionary = showList.object(at: row) as! NSDictionary
            showIndex = row
            showLbl.textColor = UIColor.black
            showLbl.text = object.object(forKey: "showname") as! String
            
            
           // J1DefaultJudge = object.object(forKey: "default_judge") as! NSInteger
            
            
            
            ShowId = object.object(forKey: "id") as! String
            
            //            showTable.isHidden = true
            //            showClass = false
            
            
            judgeList.removeAllObjects()
            
            let temp:NSArray = object.object(forKey: "judge") as! NSArray
            
            
            for object1 in temp
            {
                self.judgeList.add(object1)
            }
            
            
        }
        else if className
        {
            classIndex = row
            
            let object:NSDictionary = classList.object(at: row) as! NSDictionary
            
            classId = object.object(forKey: "id") as! String
            
            classLbl.textColor = UIColor.black
            classLbl.text = "CLASS #: \( object.object(forKey: "classnumber") as! String)"
            nameLbl.text = "\( object.object(forKey: "classname") as! String)"
            divisionLbl.text = "DIVISION:  \( object.object(forKey: "divisionname") as! String)"
            divisionId = object.object(forKey: "division") as! String
            defaultJudgeLbl.text = "JUDGE:  \( object.object(forKey: "judgename") as! String)"
            judgeId = object.object(forKey: "judge") as! String
            
            categoryList = NSMutableArray(array: object.object(forKey: "categorydetails") as! NSArray)
            
        }
        
        
        
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel: UILabel? = (view as? UILabel)
        if pickerLabel == nil {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name: "Roboto-Regular", size: 30)
            pickerLabel?.textAlignment = .center
        }
        
        if show
        {
            let object:NSDictionary = showList.object(at: row) as! NSDictionary

            pickerLabel?.text = object.object(forKey: "showname") as! String
        }
        else if className
        {
            let object:NSDictionary = classList.object(at: row) as! NSDictionary
            
            pickerLabel?.text = object.object(forKey: "classnumber") as! String
        }
        
        pickerLabel?.textColor = UIColor.black
        
        return pickerLabel!
    }
    
    
    
    
    @IBAction func printClick(_ sender: Any)
    {
        let alert = UIAlertController(title: nil, message: "Do you want to Save Card to Gallery?", preferredStyle: UIAlertControllerStyle.alert)
        
        let yesAction = UIAlertAction.init(title: "YES", style: UIAlertActionStyle.default) { _ in
            
//            let printView  = UIView.init(frame: self.view.frame)
//            let printHeaderView : NPHeaderView = self.HeaderView
//            let printMainView : UIScrollView = self.mainScroll
//            print("Print Header:",printHeaderView.subviews)
            
            self.HeaderView.btnFile.isHidden = true
            self.HeaderView.btnShow.isHidden = true
            self.HeaderView.btnPreferences.isHidden = true
            
//            printView.addSubview(printHeaderView)
//            printView.addSubview(printMainView)
            
            //Create the UIImage
            UIGraphicsBeginImageContext(self.view.frame.size)
            self.view.layer.render(in: UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
//            UIGraphicsBeginImageContext(self.mainScroll.frame.size)
//            self.mainScroll.layer.render(in: UIGraphicsGetCurrentContext()!)
            
            //Save it to the camera roll
            UIImageWriteToSavedPhotosAlbum(image!, nil, nil, nil)
            
            self.HeaderView.btnFile.isHidden = false
            self.HeaderView.btnShow.isHidden = false
            self.HeaderView.btnPreferences.isHidden = false
            
            self.ScreenShotToSave(image: image!)
            
            // Success Alert
            
//            let alert = UIAlertController(title: nil, message: "Judge's Card image saved to Gallery.", preferredStyle: UIAlertControllerStyle.alert)
//            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//
//            self.present(alert, animated: true, completion: nil)
            
        }
        
        let noAction = UIAlertAction.init(title: "NO", style: UIAlertActionStyle.cancel) { _ in
            
            
        }
        
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
        
       
    }

    
    // MARK: - Image SAVE Api Execute
    @objc func ScreenShotToSave(image : UIImage)
    {
        DispatchQueue.main.async {
            self.CheckLoader()
        }
        
        let imageData : NSData = UIImagePNGRepresentation(image) as! NSData
        let base64String : String = imageData.base64EncodedString(options: NSData.Base64EncodingOptions.endLineWithLineFeed)
        
        let  url_to_request = GlobalViewController.GLOBALAPI + "show/printjudgesCard"
        
        print("Full URL:",url_to_request)
        
        let urlString:NSURL = NSURL(string: url_to_request)!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: urlString as URL)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        print("Base 64 String:",base64String)
        
        let parameter = "classid=\(classId)&showid=\(ShowId)&judgescardimage=\(base64String)"
        
        print("parameter:",parameter)
        request.httpMethod = "POST"
        request.httpBody = parameter.data(using: String.Encoding.utf8)
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            
            
            (data, response, error) in
            
            
            guard let _:Data = data, let _:URLResponse = response, error == nil
                
                else
            {
                print("error")
                let alert = UIAlertController(title: "Error", message:error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                OperationQueue.main.addOperation
                    {
                        self.CheckLoader()
                        self.present(alert, animated: true, completion: nil)
                }
                return
            }
            if let httpResponse = response as? HTTPURLResponse
            {
                
                
                print("statusCode \(httpResponse.statusCode)")
                if (httpResponse.statusCode == 200)
                {
                    
                    
                    
                    let JSONData = data
                    do {
                        let JSON = try JSONSerialization.jsonObject(with: JSONData!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                        guard let JSONDictionary: NSDictionary = JSON as? NSDictionary else {
                            print("Not a Dictionary")
                            // put in function
                            
                            print("JSONDictionary \(JSON)")
                            DispatchQueue.main.async {
                                self.CheckLoader()
                            }
                            return
                        }
                        
                        print("JSONDictionary \(JSONDictionary)")
                        
                        
                        let status : Bool = JSONDictionary.object(forKey: "status") as! Bool
                        
                        if (status == true)
                        {
                            
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                                self.PrintLbl.text = "CARD SAVED TO GALLERY"
                                
                                            let alert = UIAlertController(title: nil, message: "Judge's Card image saved to Gallery.", preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                
                                            self.present(alert, animated: true, completion: nil)
                                
                            }

                        }
                        else
                        {
                            let alert = UIAlertController(title: "Error", message: JSONDictionary.value(forKey: "message") as? String, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            
                            DispatchQueue.main.async
                                {
                                    // self.present(alert, animated: true, completion: nil)
                                    self.CheckLoader()
                                    
//                                    self.judgesCardList.removeAllObjects()
//
//                                    self.judgeCardTable.reloadData()
//                                    self.judgeCardTable.isHidden = false
//                                    self.tableHeaderView.isHidden = false
                                    
                            }
                        }
                        
                    }
                    catch let JSONError as NSError {
                        print("\(JSONError)")
                        let alert = UIAlertController(title: "Error", message: JSONError.localizedDescription as String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        OperationQueue.main.addOperation
                            {
                                self.CheckLoader()
                                self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
                else if(httpResponse.statusCode == 400)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else if(httpResponse.statusCode == 500)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        self.CheckLoader()
                    }
                }
                
            }
            
        })
        
        task.resume()
        
    }
    
    
    @IBAction func clearPlace(_ sender: Any)
    {
       
        
        invisibleCount = 0
        self.number1ImgVw.isHidden = false
        self.numberImg2Vw.isHidden = false
        self.number3ImgVw.isHidden = false
        self.number4ImgVw.isHidden = false
        self.number5ImgVw.isHidden = false
        self.number6ImgVw.isHidden = false
        self.number7ImgVw.isHidden = false
        self.number8ImgVw.isHidden = false
        self.number9ImgVw.isHidden = false
        self.number10ImgVw.isHidden = false
        
        
        for placeCheck in 0...9
        {
            let indexPath = NSIndexPath.init(row: placeCheck, section: 0)
            
            let cell:DataListCell = judgeCardTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
            
            if (editTag == 2)
            {
                cell.functionField.text = ""
                cell.functionView.backgroundColor = UIColor.clear
            }
            else if (editTag == 3)
            {
                cell.smoothField.text = ""
                cell.smoothView.backgroundColor = UIColor.clear
            }
            else if (editTag == 4)
            {
                cell.threadField.text = ""
                cell.threadView.backgroundColor = UIColor.clear
            }
            else if (editTag == 5)
            {
                cell.terminoField.text = ""
                cell.terminoView.backgroundColor = UIColor.clear
            }
            else if (editTag == 7)
            {
                cell.jusgePlaceField.text = ""
                cell.judgePlaceView.backgroundColor = UIColor.clear
            }
            
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

