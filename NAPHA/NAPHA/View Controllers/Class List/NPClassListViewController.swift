//
//  NPClassListViewController.swift
//  NAPHA
//
//  Created by Barsan on 16/02/18.
//  Copyright © 2018 Esolz. All rights reserved.
//

import UIKit

class NPClassListViewController: GlobalViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    @IBOutlet weak var ClassHeaderView: UIView!
    @IBOutlet weak var ClassJudgeTable: UITableView!
    @IBOutlet weak var lblShowName: UILabel!
    @IBOutlet weak var lblJudge1: UILabel!
    @IBOutlet weak var lblJudge2: UILabel!
    @IBOutlet weak var judge1View: UIView!
    @IBOutlet weak var judge2View: UIView!
    @IBOutlet weak var judge1Sep: UIView!
    
    @IBOutlet weak var btnShowName: UIButton!
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var showTable: UITableView!
    @IBOutlet weak var divisionTable: UITableView!
    @IBOutlet weak var judgeTable: UITableView!
    
    var divisionId : String = ""
    var judgeId : String = "", judge_position = "0"
    var ShowId : String = ""
    var Judge1 : String = ""
    var Judge2 : String = ""
    var ShowName : String = ""
    var Judge1Id : String = ""
    var Judge2Id : String = ""
    var J1DefaultJudge : NSInteger = 0
    var J1Frame : CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
    var RowIndex : NSInteger = 0, showIndex :  NSInteger = 0
    var ShowView = UIView()
    
    var rowCount = 1
    
    var classJudgeIndex = 0
    var judge1LblPrevX : CGFloat = 0.0
    
    
    var classList = NSMutableArray(), showList = NSMutableArray(), judgeList = NSMutableArray(), divisionList = NSMutableArray()
    var showClass = false, division = false, judge = false
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Add a notification for keyboard change events
        //   NotificationCenter.default.addObserver(self, selector: Selector("keyboardWillChange:"), name: .UIKeyboardWillChangeFrame, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        judge1LblPrevX = judge1View.frame.origin.x
        
        setData()
        
        
        
    }
    
    func setData()
    {
        // Do any additional setup after loading the view.
        print("details-- \(ShowId)--\(Judge1)--\(Judge2)--\(J1DefaultJudge)")
        
        J1Frame = lblJudge1.frame
        
        if ShowId.characters.count == 0
        {
            lblShowName.text = "Choose Show Name"
            
            let str1 = "JUDGE #1 :"
            
            let attributedKey1 = [NSAttributedStringKey.font :UIFont(name:GlobalViewController.RobotoBold, size: (CGFloat(Get_fontSize(size: 18)))), NSAttributedStringKey.foregroundColor : UIColor.black]
            
            let attributedText1 = NSAttributedString.init(string: str1, attributes: attributedKey1)
            
            
            let str2 = " Judge name"
            
            let attributedKey2 = [NSAttributedStringKey.font :UIFont(name: GlobalViewController.RobotoRegular, size: (CGFloat(Get_fontSize(size: 18)))), NSAttributedStringKey.foregroundColor :UIColor.lightGray]
            
            let attributedText2 = NSAttributedString.init(string: str2, attributes: attributedKey2)
            
            let combination = NSMutableAttributedString()
            
            combination.append(attributedText1)
            
            combination.append(attributedText2)
            
            lblJudge1.attributedText = combination
            
            let str3 = "JUDGE #2 :"
            
            let attributedKey3 = [NSAttributedStringKey.font :UIFont(name:GlobalViewController.RobotoBold, size: (CGFloat(Get_fontSize(size: 18)))), NSAttributedStringKey.foregroundColor : UIColor.black]
            
            let attributedText3 = NSAttributedString.init(string: str3, attributes: attributedKey3)
            
            let combination1 = NSMutableAttributedString()
            combination1.append(attributedText3)
            
            combination1.append(attributedText2)
            
            lblJudge2.attributedText = combination1
        }
        else
        {
            lblShowName.text = ShowName
            lblJudge1.text = "JUDGE #1 : \(Judge1)"
            lblJudge2.text = "JUDGE #2 : \(Judge2)"
            

            
//            if J1DefaultJudge == 1
//            {
//                judge1View.frame.origin.x = judge2View.frame.origin.x
//                judge2View.isHidden = true
//                judge1Sep.isHidden = false
//            }
            
        }
        
        
        
        
        
        let tempMutDict = NSMutableDictionary()
        tempMutDict.setValue("", forKey: "id")
        tempMutDict.setValue("", forKey: "classnumber")
        tempMutDict.setValue("", forKey: "classname")
        tempMutDict.setValue("", forKey: "divisionname")
        tempMutDict.setValue("", forKey: "judgename")
        tempMutDict.setValue("", forKey: "judge")
        tempMutDict.setValue("", forKey: "division")
        
        
        
        classList.add(tempMutDict)
        
        if (UserDefaults.standard.object(forKey: "ShowName") != nil && UserDefaults.standard.object(forKey: "ShowName") as! String != "")
        {
            HeaderView.showNameHeader.text = UserDefaults.standard.object(forKey: "ShowName") as! String
            ShowId = UserDefaults.standard.object(forKey: "ShowID") as! String
            lblShowName.text = UserDefaults.standard.object(forKey: "ShowName") as! String
            
            judgeList = NSMutableArray(array : UserDefaults.standard.object(forKey: "JudgeArray") as! NSArray)
            
            print("Judge List:",judge)
            
            if (judgeList.count > 0)
            {
                if (judgeList.count == 1)
                {
                    let objectTemp : NSDictionary = judgeList.object(at: 0) as! NSDictionary
                    
                    lblJudge1.text = "JUDGE #1 : \(objectTemp.object(forKey: "judgename") as! String)"
                    judgeId = objectTemp.object(forKey: "judgeid") as! String
                    Judge1Id = objectTemp.object(forKey: "judgeid") as! String
                }
                else
                {
                    let objectTemp : NSDictionary = judgeList.object(at: 0) as! NSDictionary
                    let objectTemp1 : NSDictionary = judgeList.object(at: 1) as! NSDictionary
                    
                    lblJudge1.text = "JUDGE #1 : \(objectTemp.object(forKey: "judgename") as! String)"
                    lblJudge2.text = "JUDGE #2 : \(objectTemp1.object(forKey: "judgename") as! String)"
                }
                
            }

                print("Default Judge",UserDefaults.standard.object(forKey: "defaultJudge") as! NSInteger)

                J1DefaultJudge = UserDefaults.standard.object(forKey: "defaultJudge") as! NSInteger
            
        }
        
        if ShowId != ""
        {
            if J1DefaultJudge == 1
            {
                judgeId = Judge1Id
            }
            else
            {
                if judgeList.count == 1
                {
                    judgeId = Judge1Id
                }
            }
            
            
            ClassListAPI()
        }
        
        
    }
    
    
    // MARK: - Class List Api Execute
    func ClassListAPI()
    {
        
        DispatchQueue.main.async {
            self.CheckLoader()
        }
        
        let  url_to_request = GlobalViewController.GLOBALAPI + "show/fetchclass"
        
        print("Full URL:",url_to_request)
        
        let urlString:NSURL = NSURL(string: url_to_request)!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: urlString as URL)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        var parameter : String = ""
        
        parameter = "showid=\(ShowId)"
        
        print("parameter:",parameter)
        request.httpMethod = "POST"
        request.httpBody = parameter.data(using: String.Encoding.utf8)
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            
            
            (data, response, error) in
            
            
            guard let _:Data = data, let _:URLResponse = response, error == nil
                
                else
            {
                print("error")
                let alert = UIAlertController(title: "Error", message:error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                OperationQueue.main.addOperation
                    {
                        self.CheckLoader()
                        self.present(alert, animated: true, completion: nil)
                }
                return
            }
            //    self.statusCode = [(NSHTTPURLResponse*)response statusCode];
            if let httpResponse = response as? HTTPURLResponse
            {
                
                
                print("statusCode \(httpResponse.statusCode)")
                if (httpResponse.statusCode == 200)
                {
                    
                    let JSONData = data
                    do {
                        let JSON = try JSONSerialization.jsonObject(with: JSONData!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                        guard let JSONDictionary: NSDictionary = JSON as? NSDictionary else {
                            print("Not a Dictionary")
                            // put in function
                            
                            print("JSONDictionary \(JSON)")
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                                
                            }
                            return
                        }
                        
                        print("JSONDictionary \(JSONDictionary)")
                        
                        
                        let status : Bool = JSONDictionary.object(forKey: "status") as! Bool
                        
                        
                        if (status)
                        {
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                                let temparray = JSONDictionary.object(forKey: "details") as! NSArray
                                
                                
                                self.classList.removeAllObjects()
                                
                                self.classList = temparray.mutableCopy() as! NSMutableArray
                                
                                let tempDict = NSMutableDictionary()
                                tempDict.setValue("", forKey: "id")
                                tempDict.setValue("", forKey: "classnumber")
                                tempDict.setValue("", forKey: "classname")
                                tempDict.setValue("", forKey: "divisionname")
                                
                                if self.J1DefaultJudge == 1
                                {
                                    
                                    let judgeobject = self.judgeList.object(at: 0) as! NSDictionary
                                    
                                    tempDict.setValue("\(judgeobject.object(forKey: "judgename") as! String)", forKey: "judgename")
                                    tempDict.setValue("\(judgeobject.object(forKey: "judgeid") as! String)", forKey: "judge")
                                    
                                }
                                else
                                {
                                    
                                    if self.judgeList.count == 1
                                    {
                                        let judgeobject = self.judgeList.object(at: 0) as! NSDictionary
                                        tempDict.setValue(judgeobject.object(forKey: "judgename"), forKey: "judgename")
                                        tempDict.setValue(judgeobject.object(forKey: "judgeid") as! String, forKey: "judge")
                                    }
                                    else
                                    {
                                        tempDict.setValue("", forKey: "judgename")
                                        tempDict.setValue("", forKey: "judge")
                                    }
                                    
                                }
                                
                                
                                tempDict.setValue("", forKey: "division")
                                
                                self.classList.add(tempDict)
                                
                                
                                
                                if self.classList.count > 0
                                {
                                    self.ClassJudgeTable.isHidden = false
                                    self.ClassJudgeTable.reloadData()
                                    
                                }
                                else
                                {
                                    
                                    self.classList.removeAllObjects()
                                    self.ClassJudgeTable.reloadData()
                                    //  self.ShowTable.isHidden = true
                                }
                                
                                
                            }
                            
                            
                            
                        }
                        else
                        {
                            
                            
                            DispatchQueue.main.async {
                                
                                self.classList.removeAllObjects()
                                
                                
                                let tempDict = NSMutableDictionary()
                                tempDict.setValue("", forKey: "id")
                                tempDict.setValue("", forKey: "classnumber")
                                tempDict.setValue("", forKey: "classname")
                                tempDict.setValue("", forKey: "divisionname")
                                if self.J1DefaultJudge == 1
                                {
                                    let judgeobject = self.judgeList.object(at: 0) as! NSDictionary
                                    
                                    tempDict.setValue("\(judgeobject.object(forKey: "judgename") as! String)", forKey: "judgename")
                                    tempDict.setValue("\(judgeobject.object(forKey: "judgeid") as! String)", forKey: "judge")
                                    
                                }
                                else
                                {
                                    if self.judgeList.count == 1
                                    {
                                        let judgeobject = self.judgeList.object(at: 0) as! NSDictionary
                                        tempDict.setValue(judgeobject.object(forKey: "judgename"), forKey: "judgename")
                                        tempDict.setValue(judgeobject.object(forKey: "judgeid") as! String, forKey: "judge")
                                    }
                                    else
                                    {
                                        tempDict.setValue("", forKey: "judgename")
                                        tempDict.setValue("", forKey: "judge")
                                    }
                                }
                                tempDict.setValue("", forKey: "division")
                                
                                self.classList.add(tempDict);
                                self.ClassJudgeTable.isHidden = false
                                
                                self.ClassJudgeTable.reloadData()
                                
                                self.CheckLoader()
                                
                                
                            }
                        }
                        
                        
                    }
                    catch let JSONError as NSError {
                        let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                        print("Error---\(String(describing: datastring))")
                        print("Error---\(String(describing: data))")
                        print("Error mesg---\(JSONError)")
                        
                        let alert = UIAlertController(title: "Error", message: JSONError.localizedDescription as String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        OperationQueue.main.addOperation
                            {
                                
                                self.classList.removeAllObjects()
                                self.ClassJudgeTable.reloadData()
                                // self.ShowTable.isHidden = true
                                self.CheckLoader()
                                self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                    
                }
                else if(httpResponse.statusCode == 400)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            // self.ShowTable.isHidden = true
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else if(httpResponse.statusCode == 500)
                {
                    let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print("Error---\(String(describing: datastring))")
                    
                    
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            //  self.ShowTable.isHidden = true
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        //  self.ShowTable.isHidden = true
                        self.CheckLoader()
                    }
                }
                
                
            }
            
            
        })
        
        
        task.resume()
        
        
    }
    
    
    // MARK: - Division List Api Execute
    func DivisionListAPI(divTag : Int)
    {
        
        DispatchQueue.main.async {
            self.CheckLoader()
        }
        
        let  url_to_request = GlobalViewController.GLOBALAPI + "show/fetchdivision"
        
        print("Full URL:",url_to_request)
        
        let urlString:NSURL = NSURL(string: url_to_request)!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: urlString as URL)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        var parameter : String = "showid=\(ShowId)"
        
        print("parameter:",parameter)
        request.httpMethod = "POST"
        request.httpBody = parameter.data(using: String.Encoding.utf8)
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            
            
            (data, response, error) in
            
            
            guard let _:Data = data, let _:URLResponse = response, error == nil
                
                else
            {
                print("error")
                let alert = UIAlertController(title: "Error", message:error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                OperationQueue.main.addOperation
                    {
                        self.CheckLoader()
                        self.present(alert, animated: true, completion: nil)
                }
                return
            }
            //    self.statusCode = [(NSHTTPURLResponse*)response statusCode];
            if let httpResponse = response as? HTTPURLResponse
            {
                
                
                print("statusCode \(httpResponse.statusCode)")
                if (httpResponse.statusCode == 200)
                {
                    
                    let JSONData = data
                    do {
                        let JSON = try JSONSerialization.jsonObject(with: JSONData!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                        guard let JSONDictionary: NSDictionary = JSON as? NSDictionary else {
                            print("Not a Dictionary")
                            // put in function
                            
                            print("JSONDictionary \(JSON)")
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                                
                            }
                            return
                        }
                        
                        print("JSONDictionary \(JSONDictionary)")
                        
                        
                        let status : Bool = JSONDictionary.object(forKey: "status") as! Bool
                        
                        
                        if (status)
                        {
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                                let temparray = JSONDictionary.object(forKey: "details") as! NSArray
                                
                                self.divisionList.removeAllObjects()
                                
                                self.divisionList = temparray.mutableCopy() as! NSMutableArray
                                
                                
                                if self.divisionList.count > 0
                                {
                                    self.ShowView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                                    self.ShowView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
                                    self.ShowView.center = self.view.center
                                    self.view.addSubview(self.ShowView)
                                    
                                    
                                    let typeView = UIView(frame: CGRect(x: 0, y: self.ShowView.frame.height-250, width: self.ShowView.frame.width, height: 250))
                                    typeView.backgroundColor = UIColor.white
                                    self.ShowView.addSubview(typeView)
                                    
                                    // Create a Picker
                                    let pickerView = UIPickerView()
                                    
                                    pickerView.delegate = self
                                    pickerView.dataSource = self as! UIPickerViewDataSource
                                    
                                    
                                    pickerView.frame = CGRect(x: 10, y: 50, width: self.ShowView.frame.width-20, height: 200)
                                    pickerView.backgroundColor = UIColor.white
                                    
                                    
                                    // Add DataPicker to the view
                                    typeView.addSubview(pickerView)
                                    
                                    let savebtn:UIButton = UIButton(frame: CGRect(x: typeView.frame.size.width-60, y: pickerView.frame.origin.y-50, width: 50, height: 50))
                                    savebtn.setTitleColor(GlobalViewController.ThemeColor, for: .normal)
                                    savebtn.setTitle("Save", for: .normal)
                                    savebtn.addTarget(self, action:#selector(NPClassListViewController.ShowSaveClickDivJud), for: .touchUpInside)
                                    savebtn.tag = divTag
                                    typeView.addSubview(savebtn)
                                    
                                    let cancelbtn:UIButton = UIButton(frame: CGRect(x: 10, y: pickerView.frame.origin.y-50, width: 80, height: 50))
                                    cancelbtn.setTitleColor(GlobalViewController.ThemeColor, for: .normal)
                                    cancelbtn.setTitle("Cancel", for: .normal)
                                    cancelbtn.addTarget(self, action:#selector(NPClassListViewController.ShowCancelClickDivJud), for: .touchUpInside)
                                    cancelbtn.tag = divTag
                                    typeView.addSubview(cancelbtn)
                                    
                                    
//                                    self.divisionTable.isHidden = false
//                                    self.divisionTable.reloadData()
                                    
                                }
                                else
                                {
                                    
                                    self.classList.removeAllObjects()
                                    self.ClassJudgeTable.reloadData()
                                    //  self.ShowTable.isHidden = true
                                }
                                
                                
                            }
                            
                            
                            
                        }
                        else
                        {
                            
                            
                            DispatchQueue.main.async {
                                
                                self.classList.removeAllObjects()
                          //      self.ClassJudgeTable.reloadData()
                                //  self.ShowTable.isHidden = true
                                
                                self.CheckLoader()
                                
                                
                            }
                        }
                        
                        
                    }
                    catch let JSONError as NSError {
                        let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                        print("Error---\(String(describing: datastring))")
                        print("Error---\(String(describing: data))")
                        print("Error mesg---\(JSONError)")
                        
                        let alert = UIAlertController(title: "Error", message: JSONError.localizedDescription as String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        OperationQueue.main.addOperation
                            {
                                
                                self.classList.removeAllObjects()
                                self.ClassJudgeTable.reloadData()
                                // self.ShowTable.isHidden = true
                                self.CheckLoader()
                                self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                    
                }
                else if(httpResponse.statusCode == 400)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            // self.ShowTable.isHidden = true
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else if(httpResponse.statusCode == 500)
                {
                    let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print("Error---\(String(describing: datastring))")
                    
                    
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            //  self.ShowTable.isHidden = true
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        //  self.ShowTable.isHidden = true
                        self.CheckLoader()
                    }
                }
                
                
            }
            
            
        })
        
        
        task.resume()
        
        
    }
    
    
    
    
    
    // MARK: -  UITableView Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if showClass
        {
            return showList.count
        }
        else if division
        {
            return divisionList.count
        }
        else if judge
        {
            return judgeList.count
        }
        else
        {
            return classList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if showClass
        {
            let cell:DataListCell = showTable.dequeueReusableCell(withIdentifier: "ClassShowCell") as! DataListCell
            
            return cell
        }
        else if division
        {
            let cell:DataListCell = divisionTable.dequeueReusableCell(withIdentifier: "DivisionCell") as! DataListCell
            
            return cell
        }
        else if judge
        {
            let cell:DataListCell = judgeTable.dequeueReusableCell(withIdentifier: "JudgeCell") as! DataListCell
            
            return cell
        }
        else
        {
            let cell:DataListCell = ClassJudgeTable.dequeueReusableCell(withIdentifier: "DataCell") as! DataListCell
            
            return cell
        }
        
        
    }
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        let cell = cell as! DataListCell
        
        if showClass
        {
            let object:NSDictionary = showList.object(at: indexPath.row) as! NSDictionary
            
            cell.showName.text = object.object(forKey: "showname") as! String
            
        }
        else if division
        {
            let object:NSDictionary = divisionList.object(at: indexPath.row) as! NSDictionary
            
            cell.divisionName.text = object.object(forKey: "name") as! String
        }
        else if judge
        {
            let object:NSDictionary = judgeList.object(at: indexPath.row) as! NSDictionary
            
            cell.judgeName.text = object.object(forKey: "judgename") as! String
        }
        else
        {
            
            let object:NSDictionary = classList.object(at: indexPath.row) as! NSDictionary
            
            cell.classField.text = object.object(forKey: "classnumber") as! String
            
            cell.classNameField.text = object.object(forKey: "classname") as! String
            cell.divisionField.text = object.object(forKey: "divisionname") as! String
            
            if J1DefaultJudge == 1
            {
                if object.object(forKey: "judgename") as! String == ""
                {
                    cell.judgeField.text = Judge1
                    
                }
                else
                {
                    cell.judgeField.text = object.object(forKey: "judgename") as! String
                }
                
               
                cell.judgeBtn.isUserInteractionEnabled = true
                cell.judgeDropDown.isHidden = false
            }
            else
            {
                
                cell.judgeField.text = object.object(forKey: "judgename") as! String
                
                if judgeList.count == 1
                {
                    cell.judgeBtn.isUserInteractionEnabled = false
                    cell.judgeDropDown.isHidden = true
                }
                else
                {
                
                    cell.judgeBtn.isUserInteractionEnabled = true
                    cell.judgeDropDown.isHidden = false
                    
                }
                
            }
            
            
            cell.classNameField.tag = indexPath.row
            cell.classField.tag = indexPath.row
            
            cell.saveBtn.layer.cornerRadius = 8
            
            cell.divisionBtn.tag = indexPath.row
            cell.judgeBtn.tag = indexPath.row
            cell.saveBtn.tag = indexPath.row
            print("Cell Tag",cell.saveBtn.tag, cell.classField.tag)
            
            
            
            cell.divisionBtn.addTarget(self, action: #selector(NPClassListViewController.divisionTapped), for: .touchUpInside)
            
            cell.judgeBtn.addTarget(self, action: #selector(NPClassListViewController.judgeTapped), for: .touchUpInside)
            
            cell.saveBtn.addTarget(self, action: #selector(NPClassListViewController.saveOrAddNew), for: .touchUpInside)
            
        }
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if showClass
        {
            let object:NSDictionary = showList.object(at: indexPath.row) as! NSDictionary
            
            lblShowName.text = object.object(forKey: "showname") as! String
            
            J1DefaultJudge = object.object(forKey: "default_judge") as! NSInteger
            
           
            
            ShowId = object.object(forKey: "id") as! String
            
            showTable.isHidden = true
            showClass = false
            
            
            //            for objectTemp in self.showList
            //            {
            //                let temp:NSDictionary = objectTemp as! NSDictionary
            
            judgeList.removeAllObjects()
            
            let temp:NSArray = object.object(forKey: "judge") as! NSArray

            
            for object1 in temp
            {
                self.judgeList.add(object1)
            }
            
            
            
            if J1DefaultJudge == 1
            {
                let judgeobject = temp.object(at: 0) as! NSDictionary
                
                Judge1Id = judgeobject.object(forKey: "judgeid") as! String
                
                judgeId = Judge1Id
                
//                judge1View.frame.origin.x = judge2View.frame.origin.x
//                judge2View.isHidden = true
//                lblJudge2.isHidden = true
//                judge1Sep.isHidden = false
                
                
                lblJudge1.text = "JUDGE #1 : \(judgeobject.object(forKey: "judgename") as! String)"
                
                if judgeList.count > 0
                {
                    let judgeObj : NSDictionary = judgeList.object(at: 1) as! NSDictionary
                    
                    lblJudge2.text = "JUDGE #2 : \(judgeobject.object(forKey: "judgename") as! String)"
                }
                
            }
            else
            {
                judge1View.frame.origin.x = judge1LblPrevX
                judge2View.isHidden = false
                lblJudge2.isHidden = false
                judge1Sep.isHidden = true
                
                
                let judgeObj : NSDictionary = judgeList.object(at: 0) as! NSDictionary
                
                lblJudge1.text = "JUDGE #1 : \(judgeObj.object(forKey: "judgename") as! String)"
                
                let judgeObj1 : NSDictionary = judgeList.object(at: 1) as! NSDictionary
                
                lblJudge2.text = "JUDGE #2 : \(judgeObj1.object(forKey: "judgename") as! String)"
                
            }
            
           
            
            
            
            
            ClassListAPI()
            
        }
        else if division
        {
            
            
            let object:NSDictionary = divisionList.object(at: indexPath.row) as! NSDictionary
            
            divisionId = object.object(forKey: "id") as! String
            
            
            
            divisionTable.isHidden = true
            division = false
            
            let tempMutDict:NSMutableDictionary = NSMutableDictionary(dictionary: classList.object(at: classJudgeIndex) as! NSDictionary)
            
            
            tempMutDict.setValue(divisionId, forKey: "division")
            
            classList.replaceObject(at: classJudgeIndex, with: tempMutDict)
            
            
            let indexPath = NSIndexPath.init(row: classJudgeIndex, section: 0)
            
            let cell:DataListCell = ClassJudgeTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
            
            cell.divisionField.text = object.object(forKey: "name") as! String
            
        }
        else if judge
        {
            
            let object:NSDictionary = judgeList.object(at: indexPath.row) as! NSDictionary
            
            judgeId = object.object(forKey: "judgeid") as! String
            
            
            judgeTable.isHidden = true
            judge = false
            
            
            let tempMutDict:NSMutableDictionary = NSMutableDictionary(dictionary: classList.object(at: classJudgeIndex) as! NSDictionary)
            
            tempMutDict.setValue(judgeId, forKey: "judge")
            
            classList.replaceObject(at: classJudgeIndex, with: tempMutDict)
            
            let indexPath = NSIndexPath.init(row: classJudgeIndex, section: 0)
            
            let cell:DataListCell = ClassJudgeTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
            
            cell.judgeField.text = object.object(forKey: "judgename") as! String
            
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return ClassJudgeTable.frame.size.height/10 //(80/768) * self.view.frame.size.height;
    }
    
    @IBAction func ShowTapped(_ sender: Any)
    {
        showClass = true
        
        divisionTable.isHidden = true
        division = false
        
        judge = false
        judgeTable.isHidden = true
        
        if showList.count>0
        {
            ShowView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
            ShowView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            ShowView.center = self.view.center
            self.view.addSubview(ShowView)
            
            
            let typeView = UIView(frame: CGRect(x: 0, y: ShowView.frame.height-250, width: ShowView.frame.width, height: 250))
            typeView.backgroundColor = UIColor.white
            ShowView.addSubview(typeView)
            
            // Create a Picker
            let pickerView = UIPickerView()
            
            pickerView.delegate = self
            pickerView.dataSource = self as! UIPickerViewDataSource
            
            
            pickerView.frame = CGRect(x: 10, y: 50, width: ShowView.frame.width-20, height: 200)
            pickerView.backgroundColor = UIColor.white
            
            
            // Add DataPicker to the view
            typeView.addSubview(pickerView)
            
            let savebtn:UIButton = UIButton(frame: CGRect(x: typeView.frame.size.width-60, y: pickerView.frame.origin.y-50, width: 50, height: 50))
            savebtn.setTitleColor(GlobalViewController.ThemeColor, for: .normal)
            savebtn.setTitle("Save", for: .normal)
            savebtn.addTarget(self, action:#selector(NPCategoryByDivisionEditViewController.ShowSaveClick), for: .touchUpInside)
          //  savebtn.tag = (sender as AnyObject).tag
            typeView.addSubview(savebtn)
            
            let cancelbtn:UIButton = UIButton(frame: CGRect(x: 10, y: pickerView.frame.origin.y-50, width: 80, height: 50))
            cancelbtn.setTitleColor(GlobalViewController.ThemeColor, for: .normal)
            cancelbtn.setTitle("Cancel", for: .normal)
            cancelbtn.addTarget(self, action:#selector(NPCategoryByDivisionEditViewController.ShowCancelClick), for: .touchUpInside)
         //   cancelbtn.tag = (sender as AnyObject).tag
            typeView.addSubview(cancelbtn)
            
            
            
            
//            showTable.isHidden = false
//            showTable.reloadData()
        }
        else
        {
            classShowList()
        }
        
        
     
        
    }
    
    @objc func divisionTapped(sender : UIButton)
    {
        let indexPath = NSIndexPath.init(row: sender.tag, section: 0)
        let cell:DataListCell = ClassJudgeTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
        
        cell.classField.resignFirstResponder()
        cell.classNameField.resignFirstResponder()
        
        
        classJudgeIndex = sender.tag
        
        showTable.isHidden = true
        showClass = false
        
        judge = false
        judgeTable.isHidden = true
        
        division = true
        
//        if divisionList.count>0
//        {
//            ShowView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
//            ShowView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
//            ShowView.center = self.view.center
//            self.view.addSubview(ShowView)
//
//
//            let typeView = UIView(frame: CGRect(x: 0, y: ShowView.frame.height-250, width: ShowView.frame.width, height: 250))
//            typeView.backgroundColor = UIColor.white
//            ShowView.addSubview(typeView)
//
//            // Create a Picker
//            let pickerView = UIPickerView()
//
//            pickerView.delegate = self
//            pickerView.dataSource = self as! UIPickerViewDataSource
//
//
//            pickerView.frame = CGRect(x: 10, y: 50, width: ShowView.frame.width-20, height: 200)
//            pickerView.backgroundColor = UIColor.white
//
//
//            // Add DataPicker to the view
//            typeView.addSubview(pickerView)
//
//            let savebtn:UIButton = UIButton(frame: CGRect(x: typeView.frame.size.width-60, y: pickerView.frame.origin.y-50, width: 50, height: 50))
//            savebtn.setTitleColor(GlobalViewController.ThemeColor, for: .normal)
//            savebtn.setTitle("Save", for: .normal)
//            savebtn.addTarget(self, action:#selector(NPClassListViewController.ShowSaveClickDivJud), for: .touchUpInside)
//          savebtn.tag = (sender as AnyObject).tag
//            typeView.addSubview(savebtn)
//
//            let cancelbtn:UIButton = UIButton(frame: CGRect(x: 10, y: pickerView.frame.origin.y-50, width: 80, height: 50))
//            cancelbtn.setTitleColor(GlobalViewController.ThemeColor, for: .normal)
//            cancelbtn.setTitle("Cancel", for: .normal)
//            cancelbtn.addTarget(self, action:#selector(NPClassListViewController.ShowCancelClickDivJud), for: .touchUpInside)
//           cancelbtn.tag = (sender as AnyObject).tag
//            typeView.addSubview(cancelbtn)
//
//
//        }
//        else
//        {
            if (lblShowName.text == "Choose Show Name")
            {
                lblShowName.text = "Choose Show Name"
                lblShowName.textColor = UIColor.red
            }
            else
            {
                DivisionListAPI(divTag : sender.tag)
            }
     //   }
        
        
    }
    
    @objc func judgeTapped(sender : UIButton)
    {
        let indexPath = NSIndexPath.init(row: sender.tag, section: 0)
        let cell:DataListCell = ClassJudgeTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
        
        cell.classNameField.resignFirstResponder()
        cell.classField.resignFirstResponder()
        
        
        
        classJudgeIndex = sender.tag
        
        showTable.isHidden = true
        showClass = false
        
        division = false
        divisionTable.isHidden = true
        
        if judgeList.count>0
        {
        
        judge = true
        
        ShowView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        ShowView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        ShowView.center = self.view.center
        self.view.addSubview(ShowView)
        
        
        let typeView = UIView(frame: CGRect(x: 0, y: ShowView.frame.height-250, width: ShowView.frame.width, height: 250))
        typeView.backgroundColor = UIColor.white
        ShowView.addSubview(typeView)
        
        // Create a Picker
        let pickerView = UIPickerView()
        
        pickerView.delegate = self
        pickerView.dataSource = self as! UIPickerViewDataSource
        
        
        pickerView.frame = CGRect(x: 10, y: 50, width: ShowView.frame.width-20, height: 200)
        pickerView.backgroundColor = UIColor.white
        
        
        // Add DataPicker to the view
        typeView.addSubview(pickerView)
        
        let savebtn:UIButton = UIButton(frame: CGRect(x: typeView.frame.size.width-60, y: pickerView.frame.origin.y-50, width: 50, height: 50))
        savebtn.setTitleColor(GlobalViewController.ThemeColor, for: .normal)
        savebtn.setTitle("Save", for: .normal)
        savebtn.addTarget(self, action:#selector(NPClassListViewController.ShowSaveClickDivJud), for: .touchUpInside)
        savebtn.tag = (sender as AnyObject).tag
        typeView.addSubview(savebtn)
        
        let cancelbtn:UIButton = UIButton(frame: CGRect(x: 10, y: pickerView.frame.origin.y-50, width: 80, height: 50))
        cancelbtn.setTitleColor(GlobalViewController.ThemeColor, for: .normal)
        cancelbtn.setTitle("Cancel", for: .normal)
        cancelbtn.addTarget(self, action:#selector(NPClassListViewController.ShowCancelClickDivJud), for: .touchUpInside)
        cancelbtn.tag = (sender as AnyObject).tag
        typeView.addSubview(cancelbtn)
        
        
        }
        
        
//        if judge
//        {
//            judge = false
//            judgeTable.isHidden = true
//        }
//        else
//        {
//
//            judge = true
//            judgeTable.isHidden = false
//            judgeTable.reloadData()
//
//            judgeTable.frame.origin.y = (80/768) * self.view.frame.size.height * CGFloat((sender.tag + 1))
//
//            ClassJudgeTable.addSubview(judgeTable)
//
//        }
        
        
        
    }
    
    // MARK: - Class List Api Execute
    func classShowList()
    {
        
        DispatchQueue.main.async {
            self.CheckLoader()
        }
        
        let  url_to_request = GlobalViewController.GLOBALAPI + "show/fetchparticularshowjudge"
        
        print("Full URL:",url_to_request)
        
        let urlString:NSURL = NSURL(string: url_to_request)!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: urlString as URL)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        var parameter : String = ""
        
        parameter = "showid="
        
        print("parameter:",parameter)
        request.httpMethod = "POST"
        request.httpBody = parameter.data(using: String.Encoding.utf8)
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            
            
            (data, response, error) in
            
            
            guard let _:Data = data, let _:URLResponse = response, error == nil
                
                else
            {
                print("error")
                let alert = UIAlertController(title: "Error", message:error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                OperationQueue.main.addOperation
                    {
                        self.CheckLoader()
                        self.present(alert, animated: true, completion: nil)
                }
                return
            }
            //    self.statusCode = [(NSHTTPURLResponse*)response statusCode];
            if let httpResponse = response as? HTTPURLResponse
            {
                
                
                print("statusCode \(httpResponse.statusCode)")
                if (httpResponse.statusCode == 200)
                {
                    
                    let JSONData = data
                    do {
                        let JSON = try JSONSerialization.jsonObject(with: JSONData!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                        guard let JSONDictionary: NSDictionary = JSON as? NSDictionary else {
                            print("Not a Dictionary")
                            // put in function
                            
                            print("JSONDictionary \(JSON)")
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                                
                            }
                            return
                        }
                        
                        print("JSONDictionary \(JSONDictionary)")
                        
                        
                        let status : Bool = JSONDictionary.object(forKey: "status") as! Bool
                        
                        
                        if (status)
                        {
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                                let temparray = JSONDictionary.object(forKey: "details") as! NSArray
                                
                                self.divisionList = NSMutableArray(array:JSONDictionary.object(forKey: "divisions") as! NSArray)
                                
                                
                                
                                self.showList = temparray.mutableCopy() as! NSMutableArray
                                
                                if self.showList.count > 0
                                {
                                    self.ShowView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                                    self.ShowView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
                                    self.ShowView.center = self.view.center
                                    self.view.addSubview(self.ShowView)
                                    
                                    let typeView = UIView(frame: CGRect(x: 0, y: self.ShowView.frame.height-250, width: self.ShowView.frame.width, height: 250))
                                    typeView.backgroundColor = UIColor.white
                                    self.ShowView.addSubview(typeView)
                                    
                                    // Create a Picker
                                    let pickerView = UIPickerView()
                                    
                                    pickerView.delegate = self
                                    pickerView.dataSource = self as UIPickerViewDataSource
                                    
                                    
                                    pickerView.frame = CGRect(x: 10, y: 50, width: self.ShowView.frame.width-20, height: 200)
                                    pickerView.backgroundColor = UIColor.white
                                    
                                    
                                    // Add DataPicker to the view
                                    typeView.addSubview(pickerView)
                                    
                                    let savebtn:UIButton = UIButton(frame: CGRect(x: typeView.frame.size.width-60, y: pickerView.frame.origin.y-50, width: 50, height: 50))
                                    savebtn.setTitleColor(GlobalViewController.ThemeColor, for: .normal)
                                    savebtn.setTitle("Save", for: .normal)
                                    savebtn.addTarget(self, action:#selector(NPCategoryByDivisionEditViewController.ShowSaveClick), for: .touchUpInside)
                                   // savebtn.tag = (sender as AnyObject).tag
                                    typeView.addSubview(savebtn)
                                    
                                    let cancelbtn:UIButton = UIButton(frame: CGRect(x: 10, y: pickerView.frame.origin.y-50, width: 80, height: 50))
                                    cancelbtn.setTitleColor(GlobalViewController.ThemeColor, for: .normal)
                                    cancelbtn.setTitle("Cancel", for: .normal)
                                    cancelbtn.addTarget(self, action:#selector(NPCategoryByDivisionEditViewController.ShowCancelClick), for: .touchUpInside)
                                   // cancelbtn.tag = (sender as AnyObject).tag
                                    typeView.addSubview(cancelbtn)
                                    
                                    
                                    
                                    
                                    //            showTable.isHidden = false
                                    //            showTable.reloadData()
                                }
                                else
                                {
                                    
                                    self.classList.removeAllObjects()
                                   // self.ClassJudgeTable.reloadData()
                                    //  self.ShowTable.isHidden = true
                                }
                                
                                
                            }
                            
                            
                            
                        }
                        else
                        {
                            
                            
                            DispatchQueue.main.async {
                                
                                
                                
                                self.classList.removeAllObjects()
                                self.ClassJudgeTable.reloadData()
                                //  self.ShowTable.isHidden = true
                                
                                self.CheckLoader()
                                
                                
                            }
                        }
                        
                        
                    }
                    catch let JSONError as NSError {
                        let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                        print("Error---\(String(describing: datastring))")
                        print("Error---\(String(describing: data))")
                        print("Error mesg---\(JSONError)")
                        
                        let alert = UIAlertController(title: "Error", message: JSONError.localizedDescription as String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        OperationQueue.main.addOperation
                            {
                                
                                self.classList.removeAllObjects()
                                self.ClassJudgeTable.reloadData()
                                // self.ShowTable.isHidden = true
                                self.CheckLoader()
                                self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                    
                }
                else if(httpResponse.statusCode == 400)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            // self.ShowTable.isHidden = true
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else if(httpResponse.statusCode == 500)
                {
                    let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print("Error---\(String(describing: datastring))")
                    
                    
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            //  self.ShowTable.isHidden = true
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        //  self.ShowTable.isHidden = true
                        self.CheckLoader()
                    }
                }
                
                
            }
            
            
        })
        
        
        task.resume()
        
        
    }
    
    // MARK : - SAVE Click
    @objc func saveOrAddNew(sender:UIButton)
    {
        
        print("Tag Value",sender.tag)
        let indexPath = NSIndexPath.init(row: sender.tag, section: 0)
        
        let cell:DataListCell = ClassJudgeTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
        
        cell.classNameField.resignFirstResponder()
        cell.classField.resignFirstResponder()
        
        if (lblShowName.text == "Choose Show Name")
        {
            lblShowName.text = "Choose Show Name"
            lblShowName.textColor = UIColor.red
        }
        else if (cell.classField.text == "")
        {
            cell.classField.attributedPlaceholder = NSAttributedString(string: "Enter Class",
                                                                       attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
        }
        else if (cell.classNameField.text == "")
        {
            cell.classNameField.attributedPlaceholder = NSAttributedString(string: "Enter Class Name",
                                                                           attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
        }
        else if (cell.divisionField.text == "")
        {
            cell.divisionField.attributedPlaceholder = NSAttributedString(string: "Select Division",
                                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
        }
        else if (cell.judgeField.text == "")
        {
            cell.judgeField.attributedPlaceholder = NSAttributedString(string: "Select Judge",
                                                                       attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
        }
        else
        {
            // Duplicate checking
            
            var duplicate = 0
            
            var classDataId = ""
            
            if classList.count > 1
            {
                var rowCount = 0
                for tempObject in classList
                {
                    
                    let classObject = tempObject as! NSDictionary
                
                    if (classObject.object(forKey: "id") != nil)
                    {
                    classDataId = classObject.object(forKey: "id") as! String
                    }
                    
                    if (rowCount != sender.tag && classObject.object(forKey: "classnumber") as! String == cell.classField.text)
                    {
                        
                        print ("Class Number :",cell.classField.text as! String)
                        duplicate = duplicate + 1
                    
//                        if duplicate > 0
//                        {
//                            break
//                        }
                    }
                
                    rowCount = rowCount + 1
                }
                
            }
            
            if (duplicate > 0)
            {
                let alert = UIAlertController(title: "Error", message: "Class Number Already Exist", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                
                    self.present(alert, animated: true, completion: nil)
            }
            else
            {
                if sender.tag == self.classList.count - 1
                {
                    
                    
                }
                else
                {
                    let object:NSDictionary = classList.object(at: sender.tag) as! NSDictionary
                    
                    divisionId = object.object(forKey: "division") as! String
                    judgeId = object.object(forKey: "judge") as! String
                }
                
                ClassSaveEditAPI(classID : cell.classField.text!, className : cell.classNameField.text!, divisionID : divisionId, judgeID : judgeId,judgePosition : judge_position , cellTag : sender.tag)
            }
            
          
        }
        
    }
    
    
    
    // MARK: - Horse Save/Edit Api Execute
    func ClassSaveEditAPI(classID : String, className : String, divisionID : String, judgeID : String, judgePosition : String , cellTag : Int)
    {
        
        
        DispatchQueue.main.async {
            self.CheckLoader()
        }
        
        
        let  url_to_request = GlobalViewController.GLOBALAPI + "show/classaddoredit"
        
        print("Full URL:",url_to_request)
        
        let urlString:NSURL = NSURL(string: url_to_request)!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: urlString as URL)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        var parameter = ""
        
        print("parameter:",parameter)
        request.httpMethod = "POST"
        
        
//        do {
//
//            //Convert to Data
//            let jsonData = try JSONSerialization.data(withJSONObject: className, options: JSONSerialization.WritingOptions.prettyPrinted)
//
//
//            //Convert back to string. Usually only do this for debugging
//        if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8)
//           {
//                print(JSONString)
//
//                let allowedCharacterSet = CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[] ").inverted
//
//                if let escapedString = JSONString.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) {
//                    print(escapedString)
//
//                    parameter = "show=\(ShowId)&classnumber=\(classID)&classname=\(escapedString)&division=\(divisionID)&judge=\(judgeID)&judge_position=\(judgePosition)"
//
//                }
//
//
//            }
//
//
//
//            print("param string \(parameter)")
//
//        } catch {
        
//        }
        
        let somedata = className.data(using: String.Encoding.utf8)
        let backToString = String(data: somedata!, encoding: String.Encoding.utf8) as String!
        
        let allowedCharacterSet = CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[] ").inverted
        
        let escapedString = backToString?.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet)
        
        
         parameter = "show=\(ShowId)&classnumber=\(classID)&classname=\(escapedString as! String)&division=\(divisionID)&judge=\(judgeID)&judge_position=\(judgePosition)"
        
        print("param string : \(parameter)")
        request.httpBody = parameter.data(using: String.Encoding.utf8)
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            
            
            (data, response, error) in
            
            
            guard let _:Data = data, let _:URLResponse = response, error == nil
                
                else
            {
                print("error")
                let alert = UIAlertController(title: "Error", message:error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                OperationQueue.main.addOperation
                    {
                        self.CheckLoader()
                        self.present(alert, animated: true, completion: nil)
                }
                return
            }
            //    self.statusCode = [(NSHTTPURLResponse*)response statusCode];
            if let httpResponse = response as? HTTPURLResponse
            {
                
                
                print("statusCode \(httpResponse.statusCode)")
                if (httpResponse.statusCode == 200)
                {
                    
                    
                    
                    let JSONData = data
                    do {
                        let datastring = NSString(data: JSONData!, encoding: String.Encoding.utf8.rawValue)
                        print("Error Die---\( datastring as! String)")
                        
                        
                        let JSON = try JSONSerialization.jsonObject(with: JSONData!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                        guard let JSONDictionary: NSDictionary = JSON as? NSDictionary else {
                            print("Not a Dictionary")
                            // put in function
                            
                            print("JSONDictionary \(JSON)")
                            DispatchQueue.main.async {
                                self.CheckLoader()
                            }
                            return
                        }
                        
                        print("JSONDictionary \(JSONDictionary)")
                        
                        
                        let status : Bool = JSONDictionary.object(forKey: "status") as! Bool
                        
                        if (status == true)
                        {
                            
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                                
                                let details:NSDictionary = JSONDictionary.object(forKey: "details") as! NSDictionary
                                
                                if cellTag < 89
                                {
                                    
                                    if cellTag == self.classList.count - 1
                                    {
                                        
                                        
                                        let tempDict = NSMutableDictionary()
                                        tempDict.setValue("", forKey: "id")
                                        tempDict.setValue("", forKey: "classnumber")
                                        tempDict.setValue("", forKey: "classname")
                                        tempDict.setValue("", forKey: "divisionname")
                                        tempDict.setValue("", forKey: "judgename")
                                        tempDict.setValue("", forKey: "judge")
                                        tempDict.setValue("", forKey: "division")
                                        
                                        self.classList.insert(details, at: self.classList.count-1)
                                        
                                        self.ClassJudgeTable.beginUpdates()
                                        self.ClassJudgeTable.insertRows(at: [IndexPath(row: self.classList.count-1, section: 0)], with: .none)
                                        self.ClassJudgeTable.endUpdates()
                                        
                                        
                                    }
                                    else
                                    {
                                         self.classList.replaceObject(at: cellTag, with: details)
                                    }
                                    
                                }
                                else
                                {
                                    self.classList.replaceObject(at: cellTag, with: details)
                                }
                                
                                
                                
                            }
                            
                            
                        }
                        else
                        {
                            let alert = UIAlertController(title: "Error", message: JSONDictionary.value(forKey: "message") as? String, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            
                            DispatchQueue.main.async {
                                self.present(alert, animated: true, completion: nil)
                                self.CheckLoader()
                                
                                
                            }
                        }
                        
                        
                    }
                    catch let JSONError as NSError {
                        print("\(JSONError)")
                        let alert = UIAlertController(title: "Error", message: JSONError.localizedDescription as String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        OperationQueue.main.addOperation
                            {
                                self.CheckLoader()
                                self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                    
                }
                else if(httpResponse.statusCode == 400)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else if(httpResponse.statusCode == 500)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        self.CheckLoader()
                    }
                }
                
                
            }
            
            
        })
        
        
        task.resume()
        
        
    }
    
    // MARK: - UITextField Delegates
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        let indexPath = NSIndexPath.init(row: textField.tag, section: 0)
        let cell:DataListCell = ClassJudgeTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
        cell.classField.resignFirstResponder()
        cell.classNameField.resignFirstResponder()
        
        return true
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField)
    {
        RowIndex = textField.tag
        
        let indexPath = NSIndexPath.init(row: textField.tag, section: 0)
        let cell:DataListCell = ClassJudgeTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
        
        judge = false
        division = false
        
        judgeTable.isHidden = true
        divisionTable.isHidden = true
        
        
        ClassJudgeTable.scrollToRow(at: ClassJudgeTable.indexPath(for: cell)!, at: .top, animated: true)
        
        
        
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
//        if textField.tag == 0
//        {
        
        let indexPath = IndexPath(row: textField.tag, section: 0)
        let cell = ClassJudgeTable.cellForRow(at: indexPath) as! DataListCell
        
        if textField == cell.classField
        {
            let scalars = string.unicodeScalars
            
            if textField.text == ""
            {
                if scalars[scalars.startIndex].value == 32
                {
                    return false
                }
                else
                {
                    print("Ascii:",scalars[scalars.startIndex].value)
                    if (scalars[scalars.startIndex].value > 96 && scalars[scalars.startIndex].value < 123) || (scalars[scalars.startIndex].value > 64 && scalars[scalars.startIndex].value < 91) || (scalars[scalars.startIndex].value > 47 && scalars[scalars.startIndex].value < 58) || scalars[scalars.startIndex].value == 65533
                    {
                        return true
                    }
                    else
                    {
                        return false
                    }
                }
                
            }
            else
            {
                if (scalars[scalars.startIndex].value > 96 && scalars[scalars.startIndex].value < 123) || (scalars[scalars.startIndex].value > 64 && scalars[scalars.startIndex].value < 91) || (scalars[scalars.startIndex].value > 47 && scalars[scalars.startIndex].value < 58) || scalars[scalars.startIndex].value == 65533 || scalars[scalars.startIndex].value == 32
                {
                    return true
                }
                else
                {
                    return false
                }
            }
        }
        else if (textField == cell.classNameField)
        {
            let scalars = string.unicodeScalars
            
            if textField.text == ""
            {
                if scalars[scalars.startIndex].value == 32
                {
                    return false
                }
                else
                {
//                    print("Ascii:",scalars[scalars.startIndex].value)
//                    if (scalars[scalars.startIndex].value > 96 && scalars[scalars.startIndex].value < 123) || (scalars[scalars.startIndex].value > 64 && scalars[scalars.startIndex].value < 91) || (scalars[scalars.startIndex].value > 47 && scalars[scalars.startIndex].value < 58) || scalars[scalars.startIndex].value == 65533
//                    {
                        return true
//                    }
//                    else
//                    {
//                        return false
//                    }
                }
                
            }
            else
            {
//                if (scalars[scalars.startIndex].value > 96 && scalars[scalars.startIndex].value < 123) || (scalars[scalars.startIndex].value > 64 && scalars[scalars.startIndex].value < 91) || (scalars[scalars.startIndex].value > 47 && scalars[scalars.startIndex].value < 58) || scalars[scalars.startIndex].value == 65533 || scalars[scalars.startIndex].value == 32
//                {
                    return true
//                }
//                else
//                {
//                    return false
//                }
            }
        }
        else
        {
            return true
        }
        

        
    }
    
    
    
    
    // MARK: - keyboard Will Show
    @objc func keyboardWillShow(notification: NSNotification)
    {
        let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        
        print(" row index \(RowIndex)")
        //  var y = (HorseEntryTable.frame.origin.y + HorseEntryTable.frame.size.height) - (keyboardSize?.height)!
        
        //   print("height brfore \(SendMessageView.frame.size.height)")
        
        ClassJudgeTable.frame = CGRect(x: ClassJudgeTable.frame.origin.x,y: ClassJudgeTable.frame.origin.y , width: ClassJudgeTable.frame.size.width,height: (view.frame.size.height - ClassJudgeTable.frame.origin.y) - (keyboardSize?.height)!);
        
        if classList.count != 0
        {
            let index = IndexPath(row:RowIndex, section: 0) // use your index number or Indexpath
            ClassJudgeTable.scrollToRow(at: index,at: .bottom, animated: false)
            
            ClassJudgeTable.isScrollEnabled = false
        }
        
    }
    // MARK: - keyboard Will hide
    @objc func keyboardWillHide(notification: NSNotification)
    {
        let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        
        ClassJudgeTable.frame = CGRect(x: ClassJudgeTable.frame.origin.x,y: ClassJudgeTable.frame.origin.y , width: ClassJudgeTable.frame.size.width,height: (view.frame.size.height - ClassJudgeTable.frame.origin.y));
        
        ClassJudgeTable.isScrollEnabled = true
    }
    
    
    // MARK: - UIpicker Delegates
    // The number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if showClass
        {
            return showList.count
        }
        else if division
        {
            return divisionList.count
        }
        else
        {
            return judgeList.count
        }
        
       // return ShowListArray.count
        
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if showClass
        {
            let object:NSDictionary = showList.object(at: row) as! NSDictionary
            
            return object.object(forKey: "showname") as! String
            
        }
        else if division
        {
            let object:NSDictionary = divisionList.object(at: row) as! NSDictionary
            
            return object.object(forKey: "name") as! String
        }
        else
        {
            let object:NSDictionary = judgeList.object(at: row) as! NSDictionary
            
            return object.object(forKey: "judgename") as! String
        }
        
        
        
//        let object = ShowListArray.object(at: row) as! NSDictionary
//        return object.object(forKey: "name") as? String
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {

        if showClass
        {
            let object:NSDictionary = showList.object(at: row) as! NSDictionary

            showIndex = row
            
            lblShowName.textColor = UIColor.black
            lblShowName.text = object.object(forKey: "showname") as! String

            J1DefaultJudge = object.object(forKey: "default_judge") as! NSInteger

            ShowId = object.object(forKey: "id") as! String

//            showTable.isHidden = true
//            showClass = false


            judgeList.removeAllObjects()

            let temp:NSArray = object.object(forKey: "judge") as! NSArray


            for object1 in temp
            {
                self.judgeList.add(object1)
            }



            if J1DefaultJudge == 1
            {
                let judgeobject = temp.object(at: 0) as! NSDictionary

                Judge1Id = judgeobject.object(forKey: "judgeid") as! String

                judgeId = Judge1Id

//                judge1View.frame.origin.x = judge2View.frame.origin.x
//                judge2View.isHidden = true
//                lblJudge2.isHidden = true
//                judge1Sep.isHidden = false

                
                lblJudge1.text = "JUDGE #1 : \(judgeobject.object(forKey: "judgename") as! String)"
                
                if judgeList.count > 0
                {
                    let judgeObj : NSDictionary = judgeList.object(at: 1) as! NSDictionary
                    
                    lblJudge2.text = "JUDGE #2 : \(judgeObj.object(forKey: "judgename") as! String)"
                }
               

            }
            else
            {
                
                judge1View.frame.origin.x = judge1LblPrevX
                judge2View.isHidden = false
                lblJudge2.isHidden = false
                judge1Sep.isHidden = true


                let judgeObj : NSDictionary = judgeList.object(at: 0) as! NSDictionary

                lblJudge1.text = "JUDGE #1 : \(judgeObj.object(forKey: "judgename") as! String)"

               if (judgeList.count > 1)
                {
                let judgeObj1 : NSDictionary = judgeList.object(at: 1) as! NSDictionary

                lblJudge2.text = "JUDGE #2 : \(judgeObj1.object(forKey: "judgename") as! String)"
                }
                else
               {
                    judgeId = ((object.object(forKey: "judge") as! NSArray).object(at: 0) as! NSDictionary).object(forKey: "judgeid") as! String
                }

            }






//            ClassListAPI()

        }
        else if division
        {


            let object:NSDictionary = divisionList.object(at: row) as! NSDictionary

            divisionId = object.object(forKey: "id") as! String



//            divisionTable.isHidden = true
//            division = false

            let tempMutDict:NSMutableDictionary = NSMutableDictionary(dictionary: classList.object(at: classJudgeIndex) as! NSDictionary)


            tempMutDict.setValue(divisionId, forKey: "division")

            classList.replaceObject(at: classJudgeIndex, with: tempMutDict)


            let indexPath = NSIndexPath.init(row: classJudgeIndex, section: 0)

            let cell:DataListCell = ClassJudgeTable.cellForRow(at: indexPath as IndexPath) as! DataListCell

            cell.divisionField.text = object.object(forKey: "name") as! String

        }
        else if judge
        {

            let object:NSDictionary = judgeList.object(at: row) as! NSDictionary

            judgeId = object.object(forKey: "judgeid") as! String
            judge_position = String(row)

//            judgeTable.isHidden = true
//            judge = false


            let tempMutDict:NSMutableDictionary = NSMutableDictionary(dictionary: classList.object(at: classJudgeIndex) as! NSDictionary)

            tempMutDict.setValue(judgeId, forKey: "judge")

            classList.replaceObject(at: classJudgeIndex, with: tempMutDict)

            let indexPath = NSIndexPath.init(row: classJudgeIndex, section: 0)

            let cell:DataListCell = ClassJudgeTable.cellForRow(at: indexPath as IndexPath) as! DataListCell

            cell.judgeField.text = object.object(forKey: "judgename") as! String

        }




    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel: UILabel? = (view as? UILabel)
        if pickerLabel == nil {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name: "Roboto-Regular", size: 30)
            pickerLabel?.textAlignment = .center
        }
        
        if showClass        {
            let object:NSDictionary = showList.object(at: row) as! NSDictionary
            
            pickerLabel?.text = object.object(forKey: "showname") as! String
        }
        else if division
        {
            let object:NSDictionary = divisionList.object(at: row) as! NSDictionary
            pickerLabel?.text = object.object(forKey: "name") as! String
        }
        else if judge
        {
            let object:NSDictionary = judgeList.object(at: row) as! NSDictionary
            
            pickerLabel?.text = object.object(forKey: "judgename") as! String
        }
        
        pickerLabel?.textColor = UIColor.black
        
        return pickerLabel!
    }
    
    
    
    @objc func ShowSaveClick()
     {
        ShowView.removeFromSuperview()
        if showClass
        {
             showClass = false
            
            if (lblShowName.text == "Choose Show Name")
            {
                lblShowName.text = "Choose Show Name"
                lblShowName.textColor = UIColor.red
            }
            else
            {
                UserDefaults.standard.setValue(lblShowName.text as! String, forKey: "ShowName")
                UserDefaults.standard.setValue(ShowId, forKey: "ShowID")
                HeaderView.showNameHeader.text = lblShowName.text as! String
                
                let object:NSDictionary = showList.object(at: showIndex) as! NSDictionary
                
                
                let temp:NSArray = object.object(forKey: "judge") as! NSArray
                UserDefaults.standard.set(temp , forKey: "JudgeArray")
                
                UserDefaults.standard.set(object.object(forKey: "default_judge") as! NSInteger ,forKey: "defaultJudge")
                
               ClassListAPI()
            }
        }
     
        
        
    }
    
    // MARK: - cancel click
    @objc func ShowCancelClick()
    {
        
       // ShowName = ""
        showClass = false
        
        if (UserDefaults.standard.object(forKey: "ShowName") != nil && UserDefaults.standard.object(forKey: "ShowName") as! String != "")
        {
            HeaderView.showNameHeader.text = UserDefaults.standard.object(forKey: "ShowName") as! String
            ShowId = UserDefaults.standard.object(forKey: "ShowID") as! String
            lblShowName.text = UserDefaults.standard.object(forKey: "ShowName") as! String
        }
        else
        {
        
            lblShowName.text = "Choose Show Name"
        }
        lblShowName.textColor = UIColor.black
        ShowView.removeFromSuperview()
        
        
    }
    
    @objc func ShowSaveClickDivJud(sender : UIButton)
    {
        ShowView.removeFromSuperview()
        if division
        {
            division = false
            
        }
        else if judge
        {
            judge = false
        }
        
        
        
    }
    
    // MARK: - cancel click
    @objc func ShowCancelClickDivJud(sender : UIButton)
    {
        
        // ShowName = ""
        
        ShowView.removeFromSuperview()
        
        if division
        {
            division = false
            
            let indexPath = NSIndexPath.init(row: classJudgeIndex, section: 0)
            
            let cell:DataListCell = ClassJudgeTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
            
            cell.divisionField.text = ""
            
        }
        else if judge
        {
            judge = false
            
            let indexPath = NSIndexPath.init(row: classJudgeIndex, section: 0)
            
            let cell:DataListCell = ClassJudgeTable.cellForRow(at: indexPath as IndexPath) as! DataListCell
            
            cell.judgeField.text = ""
        }
        
        
        
    }
    
    
    //    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
    //        var pointInTable:CGPoint = (textField.superview?.superview!.convert(textField.frame.origin, to:ClassJudgeTable))!
    //        var contentOffset:CGPoint = ClassJudgeTable.contentOffset
    //        contentOffset.y  = pointInTable.y
    //        if let accessoryView = textField.inputAccessoryView {
    //            contentOffset.y -= accessoryView.frame.size.height
    //        }
    //        ClassJudgeTable.contentOffset = contentOffset
    //        return true;
    //    }
    
    
    
   
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

