//
//  NPFileViewController.swift
//  NAPHA
//
//  Created by Priyanka on 15/02/18.
//  Copyright © 2018 Esolz. All rights reserved.
//

import UIKit

class NPFileViewController: GlobalViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var btnShowSetUp: UIButton!
    @IBOutlet weak var mainscroll1: UIScrollView!
    @IBOutlet weak var mainscroll2: UIScrollView!
    @IBOutlet weak var ShowTable: UITableView!
    
    @IBOutlet weak var txtSearch: UITextField!
    
    
    var ShowListArray = NSMutableArray()
    var search : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        print("width \(self.view.frame.size.width) height \(self.view.frame.size.height)")
        
       SetDesign()
       
    }
    func SetDesign()
    {
        btnShowSetUp.backgroundColor = GlobalViewController.ThemeColor
        btnShowSetUp.layer.cornerRadius = SetButtonRadius(size: 24)
        btnShowSetUp.layer.shadowOffset = CGSize(width:0,height:5)
        // set the radius
        btnShowSetUp.layer.shadowRadius = 10
        // change the color of the shadow (has to be CGColor)
        btnShowSetUp.layer.shadowColor = GlobalViewController.ThemeColor.cgColor
        // display the shadow
        btnShowSetUp.layer.shadowOpacity = 0.8
        btnShowSetUp.layer.shadowPath = UIBezierPath(roundedRect:CGRect(x: 20, y: 5, width: btnShowSetUp.frame.size.width - 40, height:btnShowSetUp.frame.size.height), cornerRadius: 12.0).cgPath
        btnShowSetUp.setTitleColor(UIColor.white, for: .normal)
        btnShowSetUp.titleLabel!.font = UIFont(name:GlobalViewController.RobotoBold, size: 18)
        
        txtSearch.attributedPlaceholder = NSAttributedString(string: "Search show", attributes: [NSAttributedStringKey.foregroundColor: UIColor.black])
        
        
//        if (UserDefaults.standard.object(forKey: "ShowName") != nil && UserDefaults.standard.object(forKey: "ShowName") as! String != "")
//        {
//            HeaderView.showNameHeader.text = UserDefaults.standard.object(forKey: "ShowName") as! String
//
//            txtSearch.text = UserDefaults.standard.object(forKey: "ShowName") as! String
//
//        }
        
        ShowListAPI()
        
    }
    // MARK: - Show List Api Execute
    func ShowListAPI()
    {
        
        DispatchQueue.main.async {
            self.CheckLoader()
        }
        
        let  url_to_request = GlobalViewController.GLOBALAPI + "show/fetchshow"
        
        print("Full URL:",url_to_request)
        
        let urlString:NSURL = NSURL(string: url_to_request)!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: urlString as URL)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        var parameter : String = ""
        
        parameter = "searchvalue=\(txtSearch.text!)"
        
        print("parameter:",parameter)
        request.httpMethod = "POST"
        request.httpBody = parameter.data(using: String.Encoding.utf8)
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            
            
            (data, response, error) in
            
            
            guard let _:Data = data, let _:URLResponse = response, error == nil
                
                else
            {
                print("error")
                let alert = UIAlertController(title: "Error", message:error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                OperationQueue.main.addOperation
                    {
                        self.CheckLoader()
                        self.present(alert, animated: true, completion: nil)
                }
                return
            }
            //    self.statusCode = [(NSHTTPURLResponse*)response statusCode];
            if let httpResponse = response as? HTTPURLResponse
            {
                
                
                print("statusCode \(httpResponse.statusCode)")
                if (httpResponse.statusCode == 200)
                {
                    
                    
                    
                    let JSONData = data
                    do {
                        let JSON = try JSONSerialization.jsonObject(with: JSONData!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                        guard let JSONDictionary: NSDictionary = JSON as? NSDictionary else {
                            print("Not a Dictionary")
                            // put in function
                            
                            print("JSONDictionary \(JSON)")
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                                
                            }
                            return
                        }
                        
                        print("JSONDictionary \(JSONDictionary)")
                        
                        
                        let status : Bool = JSONDictionary.object(forKey: "status") as! Bool
                        
                        
                        if (status)
                        {
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                                let temparray = JSONDictionary.object(forKey: "details") as! NSArray
                                
                                self.ShowListArray = temparray.mutableCopy() as! NSMutableArray
                                
                                
                                if self.ShowListArray.count > 0
                                {
                                    if self.search == false
                                    {
                                       self.mainscroll1.isHidden = true
                                       self.mainscroll2.isHidden = false
                                    }
                                    self.ShowTable.isHidden = false
                                    self.ShowTable.reloadData()
                                    
                                    
                                    
                                    
                                }
                                else
                                {
                                    if self.search == false
                                    {
                                    self.mainscroll1.isHidden = false
                                    self.mainscroll2.isHidden = true
                                    }
                                     self.ShowListArray.removeAllObjects()
                                     self.ShowTable.reloadData()
                                  //  self.ShowTable.isHidden = true
                                }
                                
                                
                            }
                            
                            
                            
                        }
                        else
                        {
                            
                            
                            DispatchQueue.main.async {
                                
                                
                                if self.search == false
                                {
                                self.mainscroll1.isHidden = false
                                self.mainscroll2.isHidden = true
                                    
                                }
                                self.ShowListArray.removeAllObjects()
                                 self.ShowTable.reloadData()
                              //  self.ShowTable.isHidden = true
                            
                                self.CheckLoader()
                                
                                
                            }
                        }
                        
                        
                    }
                    catch let JSONError as NSError {
                        let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                        print("Error---\(String(describing: datastring))")
                        print("Error---\(String(describing: data))")
                        print("Error mesg---\(JSONError)")
                        
                        let alert = UIAlertController(title: "Error", message: JSONError.localizedDescription as String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        OperationQueue.main.addOperation
                            {
                                if self.search == false
                                {
                                    self.mainscroll1.isHidden = false
                                    self.mainscroll2.isHidden = true
                                    
                                }
                                self.ShowListArray.removeAllObjects()
                                 self.ShowTable.reloadData()
                               // self.ShowTable.isHidden = true
                                self.CheckLoader()
                                self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                    
                }
                else if(httpResponse.statusCode == 400)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                           // self.ShowTable.isHidden = true
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else if(httpResponse.statusCode == 500)
                {
                    let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print("Error---\(String(describing: datastring))")
                    
                    
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                          //  self.ShowTable.isHidden = true
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                      //  self.ShowTable.isHidden = true
                        self.CheckLoader()
                    }
                }
                
                
            }
            
            
        })
        
        
        task.resume()
        
        
    }
    // MARK: - table view delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return ShowListArray.count
        
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // create a new cell if needed or reuse an old one
        
        
        let cell:DataListCell = ShowTable.dequeueReusableCell(withIdentifier: "NPEntryCell") as! DataListCell!
        
        
        cell.selectionStyle = .none
        
        
        
        return cell
        
        
    }
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if let cell = cell as? DataListCell
        {
            
            cell.btnClassList.layer.cornerRadius = self.SetButtonRadius(size: 15)
            cell.btnEcard.layer.cornerRadius = self.SetButtonRadius(size: 15)
            
            cell.btnClassList.tag = indexPath.row
            cell.btnEcard.tag = indexPath.row
            
            cell.btnClassList.addTarget(self, action:#selector(NPFileViewController.ClassListClick), for: .touchUpInside)
            cell.btnEcard.addTarget(self, action:#selector(NPFileViewController.ECardClick), for: .touchUpInside)
           
            if ShowListArray.count > 0
            {
                
                let tempDict:NSDictionary = ShowListArray.object(at: indexPath.row) as! NSDictionary
                
                cell.lblShowName.text = (tempDict.object(forKey: "name") as! String)
                cell.lblJudge1.text = (tempDict.object(forKey: "judgeonename") as! String)
                cell.lblJudge2.text = (tempDict.object(forKey: "judgetwoname") as! String)
                
                
            }
            
        }
    }
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell:DataListCell = ShowTable.dequeueReusableCell(withIdentifier: "NPEntryCell1") as! DataListCell!
        
        cell.selectionStyle = .none
        
        return cell
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if ShowListArray.count > 0
        {
            
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        print ("Cell Height:",ShowTable.frame.size.height/10 )
        
        return ShowTable.frame.size.height/11 //(80/768) * self.view.frame.size.height;
    }
    
    
    // MARK: - class list click
    @objc func ClassListClick(sender: UIButton)
    {
        let tempDict:NSDictionary = ShowListArray.object(at:sender.tag) as! NSDictionary
        
        let ClassListPageView = self.storyboard?.instantiateViewController(withIdentifier: "NPClassListViewController") as! NPClassListViewController
        ClassListPageView.ShowId = tempDict.object(forKey: "id") as! String
        ClassListPageView.Judge1 = tempDict.object(forKey: "judgeonename") as! String
        ClassListPageView.Judge2 = tempDict.object(forKey: "judgetwoname") as! String
        ClassListPageView.J1DefaultJudge = NSInteger(tempDict.object(forKey: "default_judge") as! String)!
        ClassListPageView.ShowName = tempDict.object(forKey: "name") as! String
        ClassListPageView.Judge1Id = tempDict.object(forKey: "judgeoneid") as! String
        ClassListPageView.Judge2Id = tempDict.object(forKey: "judgetwoid") as! String
        
        
        
        var tempDict1 = NSMutableDictionary()
        var tempDict2 = NSMutableDictionary()
        let judgeList = NSMutableArray()
        tempDict1.setValue(tempDict.object(forKey: "judgeoneid"), forKey: "judgeid")
        tempDict1.setValue(tempDict.object(forKey: "judgeonename") as! String, forKey: "judgename")
        judgeList.add(tempDict1)
        
        tempDict2.setValue(tempDict.object(forKey: "judgetwoid") , forKey: "judgeid")
        tempDict2.setValue(tempDict.object(forKey: "judgetwoname") as! String, forKey: "judgename")
        judgeList.add(tempDict2)
        
        ClassListPageView.judgeList = judgeList.mutableCopy() as! NSMutableArray
        
        UserDefaults.standard.setValue((tempDict.object(forKey: "name") as! String), forKey: "ShowName")
        UserDefaults.standard.setValue((tempDict.object(forKey: "id") as! String), forKey: "ShowID")
        
        UserDefaults.standard.set(NSInteger(tempDict.object(forKey: "default_judge") as! String)!, forKey: "defaultJudge")
        UserDefaults.standard.set(judgeList.mutableCopy() as! NSArray, forKey: "JudgeArray")
        
        self.navigationController?.pushViewController(ClassListPageView, animated: false)
        
    }
    
    
    // MARK: - E Card click
    @objc func ECardClick(sender: UIButton)
    {
        let tempDict:NSDictionary = ShowListArray.object(at:sender.tag) as! NSDictionary
        
        let EcardPageView = self.storyboard?.instantiateViewController(withIdentifier: "NPJudgeCardViewController") as! NPJudgeCardViewController

        
        UserDefaults.standard.setValue((tempDict.object(forKey: "name") as! String), forKey: "ShowName")
        UserDefaults.standard.setValue((tempDict.object(forKey: "id") as! String), forKey: "ShowID")
        UserDefaults.standard.set(NSInteger(tempDict.object(forKey: "default_judge") as! String)!, forKey: "defaultJudge")
        
        var tempDict1 = NSMutableDictionary()
        var tempDict2 = NSMutableDictionary()
        let judgeList = NSMutableArray()
        tempDict1.setValue(tempDict.object(forKey: "judgeoneid"), forKey: "judgeid")
        tempDict1.setValue(tempDict.object(forKey: "judgeonename") as! String, forKey: "judgename")
        judgeList.add(tempDict1)
        
        tempDict2.setValue(tempDict.object(forKey: "judgetwoid") , forKey: "judgeid")
        tempDict2.setValue(tempDict.object(forKey: "judgetwoname") as! String, forKey: "judgename")
        judgeList.add(tempDict2)
        
        
        UserDefaults.standard.set(judgeList.mutableCopy() as! NSMutableArray, forKey: "JudgeArray")
        self.navigationController?.pushViewController(EcardPageView, animated: false)
    }
     // MARK: - Show Set Up tapped
    @IBAction func ShowSetUpTapped(_ sender: Any) {
        
        let loginPageView = self.storyboard?.instantiateViewController(withIdentifier: "NPShowSetupViewController") as! NPShowSetupViewController
        self.navigationController?.pushViewController(loginPageView, animated: false)
    }
    // MARK: - Search tapped
    @IBAction func SearchTapped(_ sender: Any) {
        
        txtSearch.resignFirstResponder()
        search = true
        ShowListAPI()
       
        
    }
    // MARK: - TextField Delegates
    public func textFieldDidBeginEditing(_ textField: UITextField)
    {
        
        
    }
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool
    {
        return true
    }
    public func textFieldDidEndEditing(_ textField: UITextField)
    {
        
    }
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        return true
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        mainscroll2.setContentOffset(CGPoint (x: 0, y: 0), animated: true)
        textField.endEditing(true)
        
        txtSearch.resignFirstResponder()
        search = true
        ShowListAPI()
        
        return false
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
