//
//  NPFileOthersViewController.swift
//  NAPHA
//
//  Created by admin on 06/03/18.
//  Copyright © 2018 Esolz. All rights reserved.
//

import UIKit

class NPFileOthersViewController: GlobalViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if (UserDefaults.standard.object(forKey: "ShowName") != nil && UserDefaults.standard.object(forKey: "ShowName") as! String != "")
        {
            HeaderView.showNameHeader.text = UserDefaults.standard.object(forKey: "ShowName") as! String
            
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
