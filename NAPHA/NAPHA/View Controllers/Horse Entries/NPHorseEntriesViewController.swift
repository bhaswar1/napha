//
//  NPHorseEntriesViewController.swift
//  NAPHA
//
//  Created by Priyanka on 16/02/18.
//  Copyright © 2018 Esolz. All rights reserved.
//

import UIKit

class NPHorseEntriesViewController: GlobalViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var mainscroll: UIScrollView!
    @IBOutlet weak var HorseEntryTable: UITableView!
    
    @IBOutlet weak var showNameLbl: UILabel!
    @IBOutlet weak var showView: UIView!
    
    var previousTableHeight = 0.0
    var HorseEntryArray = NSMutableArray()
    var HorseEntryNo : String = ""
    var HorseName : String = ""
    var HorseId : String = ""
    var RowIndex : NSInteger = 0, showIndex = 0
    var ShowId : String = ""
    
    var showList = NSMutableArray()
    var showClass = false
    var ShowView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        previousTableHeight = Double(HorseEntryTable.frame.size.height)
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        
        if (UserDefaults.standard.object(forKey: "ShowName") != nil && UserDefaults.standard.object(forKey: "ShowName") as! String != "")
        {
            HeaderView.showNameHeader.text = UserDefaults.standard.object(forKey: "ShowName") as! String
            ShowId = UserDefaults.standard.object(forKey: "ShowID") as! String
            showNameLbl.text = UserDefaults.standard.object(forKey: "ShowName") as! String
            
            HorseListAPI()
        }
        
       // HorseListAPI()
    }
    // MARK: - Horse List Api Execute
    func HorseListAPI()
    {
        
        DispatchQueue.main.async {
            self.CheckLoader()
        }
        
        let  url_to_request = GlobalViewController.GLOBALAPI + "show/horsefetch"
        
        print("Full URL:",url_to_request)
        
        let urlString:NSURL = NSURL(string: url_to_request)!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: urlString as URL)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        var parameter : String = ""
        
      parameter = "entrynumber=&showid=\(ShowId)"
        
        print("parameter:",parameter)
        request.httpMethod = "POST"
        request.httpBody = parameter.data(using: String.Encoding.utf8)
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            
            
            (data, response, error) in
            
            
            guard let _:Data = data, let _:URLResponse = response, error == nil
                
                else
            {
                print("error")
                let alert = UIAlertController(title: "Error", message:error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                OperationQueue.main.addOperation
                    {
                        self.CheckLoader()
                        self.present(alert, animated: true, completion: nil)
                }
                return
            }
            //    self.statusCode = [(NSHTTPURLResponse*)response statusCode];
            if let httpResponse = response as? HTTPURLResponse
            {
                
                
                print("statusCode \(httpResponse.statusCode)")
                if (httpResponse.statusCode == 200)
                {
                    
                    
                    
                    let JSONData = data
                    do {
                        let JSON = try JSONSerialization.jsonObject(with: JSONData!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                        guard let JSONDictionary: NSDictionary = JSON as? NSDictionary else {
                            print("Not a Dictionary")
                            // put in function
                            
                            print("JSONDictionary \(JSON)")
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                                
                            }
                            return
                        }
                        
                        print("JSONDictionary \(JSONDictionary)")
                        
                        
                        let status : Bool = JSONDictionary.object(forKey: "status") as! Bool
                        
                        
                        if (status)
                        {
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                                let temparray = JSONDictionary.object(forKey: "details") as! NSArray
                                
                                self.HorseEntryArray = temparray.mutableCopy() as! NSMutableArray
                                
                                let tempDict = NSMutableDictionary()
                                tempDict.setValue("", forKey: "entrynumber")
                                tempDict.setValue("", forKey: "id")
                                tempDict.setValue("", forKey: "name")
                                
                                self.HorseEntryArray.add(tempDict)
                                
                                
                                
                                if self.HorseEntryArray.count > 0
                                {
                                    
                                    self.HorseEntryTable.isHidden = false
                                    self.HorseEntryTable.reloadData()
                                    
                                    
                                    
                                    
                                }
                                else
                                {
                                  
                                    self.HorseEntryArray.removeAllObjects()
                                    self.HorseEntryTable.reloadData()
                                    //  self.ShowTable.isHidden = true
                                }
                                
                                
                            }
                            
                            
                            
                        }
                        else
                        {
                            
                            
                            DispatchQueue.main.async {
                                
                                
                              
                                self.HorseEntryArray.removeAllObjects()
                                self.HorseEntryTable.reloadData()
                                //  self.ShowTable.isHidden = true
                                
                                self.CheckLoader()
                                
                                
                            }
                        }
                        
                        
                    }
                    catch let JSONError as NSError {
                        let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                        print("Error---\(String(describing: datastring))")
                        print("Error---\(String(describing: data))")
                        print("Error mesg---\(JSONError)")
                        
                        let alert = UIAlertController(title: "Error", message: JSONError.localizedDescription as String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        OperationQueue.main.addOperation
                            {
                               
                                self.HorseEntryArray.removeAllObjects()
                                self.HorseEntryTable.reloadData()
                                // self.ShowTable.isHidden = true
                                self.CheckLoader()
                                self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                    
                }
                else if(httpResponse.statusCode == 400)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            // self.ShowTable.isHidden = true
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else if(httpResponse.statusCode == 500)
                {
                    let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print("Error---\(String(describing: datastring))")
                    
                    
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            //  self.ShowTable.isHidden = true
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        //  self.ShowTable.isHidden = true
                        self.CheckLoader()
                    }
                }
                
                
            }
            
            
        })
        
        
        task.resume()
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - table view delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        if HorseEntryArray.count == 0
        {
            return 1
        }
        else
        {
            return HorseEntryArray.count
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // create a new cell if needed or reuse an old one
        
        
        let cell:DataListCell = HorseEntryTable.dequeueReusableCell(withIdentifier: "NPEntryCell") as! DataListCell!
        
        
        cell.selectionStyle = .none
        
        
        
        return cell
        
        
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return CGFloat(previousTableHeight/10) // HorseEntryTable.frame.size.height/10   //(80/768) * self.view.frame.size.height;
    }
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if let cell = cell as? DataListCell
        {
            
            cell.btnSave.layer.cornerRadius = self.SetButtonRadius(size: 16)
            
            cell.txtHorseEntry.tag = indexPath.row
            cell.txtHorseName.tag = indexPath.row
            
            cell.txtHorseName.delegate = self
            cell.txtHorseEntry.delegate = self
            
            cell.txtHorseEntry.placeholder = nil
            cell.txtHorseName.placeholder = nil
            
//            cell.txtHorseEntry.tag = 0
//            cell.txtHorseName.tag = 1
            
            cell.btnSave.tag = indexPath.row
            cell.btnSave.addTarget(self, action:#selector(NPHorseEntriesViewController.SaveClick), for: .touchUpInside)


            if HorseEntryArray.count > 0
            {
               // print("Index Row:",indexPath.row)
                if indexPath.row == HorseEntryArray.count // last row
                {
                
                }
                else // not a last row
                {
                    let tempDict:NSDictionary = HorseEntryArray.object(at: indexPath.row) as! NSDictionary
                    
                    cell.txtHorseEntry.text = (tempDict.object(forKey: "entrynumber") as! String)
                    cell.txtHorseName.text = (tempDict.object(forKey: "name") as! String)
                }
                



            }
            else
            {
                cell.txtHorseEntry.text = ""
                cell.txtHorseName.text = ""
            }



       }
    }
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell:DataListCell = HorseEntryTable.dequeueReusableCell(withIdentifier: "NPEntryCell1") as! DataListCell!
        
        cell.selectionStyle = .none
        
        return cell.contentView
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    // MARK: - Save click
    @objc func SaveClick(sender: UIButton)
    {
        
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = HorseEntryTable.cellForRow(at: indexPath) as! DataListCell
        
        cell.txtHorseEntry.resignFirstResponder()
        cell.txtHorseName.resignFirstResponder()
        
        
    //    HorseEntryTable.setContentOffset(CGPoint (x: 0, y: 0), animated: true)
        
     //   print("text ---\(String(describing: cell.txtHorseEntry.text))")
        if (showNameLbl.text == "Choose Show Name")
        {
            showNameLbl.text = "Choose Show Name"
            showNameLbl.textColor = UIColor.red
        }
        else if (BlankOrNot(cell.txtHorseEntry.text!).characters.count==0)
        {
            cell.txtHorseEntry.attributedPlaceholder = NSAttributedString(string: "Enter Entry #", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
        }
        else if (BlankOrNot(cell.txtHorseName.text!).characters.count==0)
        {
            cell.txtHorseName.attributedPlaceholder = NSAttributedString(string: "Enter Horse Name", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
        }
        else
        {
            cell.txtHorseEntry.resignFirstResponder()
            cell.txtHorseName.resignFirstResponder()
            
            HorseName = cell.txtHorseName.text!
            HorseEntryNo = cell.txtHorseEntry.text!
            
            // Duplicate Checking
            
            var duplicate = 0
            
            var horseDataId = ""
            
            if HorseEntryArray.count > 1
            {
                var rowCount = 0
                for tempObject in HorseEntryArray
                {
                    
                    let classObject = tempObject as! NSDictionary
                    
                    if (classObject.object(forKey: "id") != nil)
                    {
                        horseDataId = classObject.object(forKey: "id") as! String
                    }
                    
                    if (rowCount != sender.tag && classObject.object(forKey: "entrynumber") as! String == cell.txtHorseEntry.text)
                    {
                        
                        print ("Horse Number :",cell.txtHorseEntry.text as! String)
                        duplicate = duplicate + 1
                        
                        //                        if duplicate > 0
                        //                        {
                        //                            break
                        //                        }
                    }
                    
                    rowCount = rowCount + 1
                }
                
            }
            
            if (duplicate > 0)
            {
                let alert = UIAlertController(title: "Error", message: "Horse Number Already Exist", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
            
            
            
            
            
            // last row - so add
            if sender.tag == HorseEntryArray.count
            {
                HorseId = ""
            }
            else // not a last row - so edit
            {
                let tempDict:NSDictionary = HorseEntryArray.object(at:sender.tag) as! NSDictionary
                
                HorseId = tempDict.object(forKey: "id") as! String
            }
           
            
                HorseSaveEditAPI(RowId:sender.tag, HorseId: HorseId)
            }
           
            
        }
    }
    
    
    // MARK: - Horse Save/Edit Api Execute
    func HorseSaveEditAPI(RowId : NSInteger, HorseId : String)
    {
        
        
        DispatchQueue.main.async {
            self.CheckLoader()
        }
        
        
        let  url_to_request = GlobalViewController.GLOBALAPI + "show/horseentry"
        
        print("Full URL:",url_to_request)
        
        let urlString:NSURL = NSURL(string: url_to_request)!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: urlString as URL)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        let parameter = "entrynumber=\(HorseEntryNo)&name=\(HorseName)&id=\(HorseId)&showid=\(ShowId)"
        
        print("parameter:",parameter)
        request.httpMethod = "POST"
        request.httpBody = parameter.data(using: String.Encoding.utf8)
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            
            
            (data, response, error) in
            
            
            guard let _:Data = data, let _:URLResponse = response, error == nil
                
                else
            {
                print("error")
                let alert = UIAlertController(title: "Error", message:error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                OperationQueue.main.addOperation
                    {
                        self.CheckLoader()
                        self.present(alert, animated: true, completion: nil)
                }
                return
            }
            //    self.statusCode = [(NSHTTPURLResponse*)response statusCode];
            if let httpResponse = response as? HTTPURLResponse
            {
                
                
                print("statusCode \(httpResponse.statusCode)")
                if (httpResponse.statusCode == 200)
                {
                    
                    
                    
                    let JSONData = data
                    do {
                        let JSON = try JSONSerialization.jsonObject(with: JSONData!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                        guard let JSONDictionary: NSDictionary = JSON as? NSDictionary else {
                            print("Not a Dictionary")
                            // put in function
                            
                            print("JSONDictionary \(JSON)")
                            DispatchQueue.main.async {
                                self.CheckLoader()
                            }
                            return
                        }
                        
                        print("JSONDictionary \(JSONDictionary)")
                        
                        
                        let status : Bool = JSONDictionary.object(forKey: "status") as! Bool
                        
                        if (status == true)
                        {
                            
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                                
                                let details:NSDictionary = JSONDictionary.object(forKey: "details") as! NSDictionary
                                
                                if self.HorseEntryArray.count == 0
                                {
                                    let tempDict = NSMutableDictionary()
                                    tempDict.setValue("", forKey: "entrynumber")
                                    tempDict.setValue("", forKey: "id")
                                    tempDict.setValue("", forKey: "name")
                                    
                                    self.HorseEntryArray.add(tempDict)
                                }
                                
                                
                                if RowId == self.HorseEntryArray.count - 1 // last row so add
                                {
                               // self.HorseEntryArray.replaceObject(at: self.HorseEntryArray.count, with: details)
                                    
                                    let tempDict = NSMutableDictionary()
                                    tempDict.setValue("", forKey: "entrynumber")
                                    tempDict.setValue("", forKey: "id")
                                    tempDict.setValue("", forKey: "name")
                                    
                                    self.HorseEntryArray.insert(details, at: self.HorseEntryArray.count-1)
                                    
                                    self.HorseEntryTable.beginUpdates()
                                    self.HorseEntryTable.insertRows(at: [IndexPath(row: self.HorseEntryArray.count-1, section: 0)], with: .none)
                                    self.HorseEntryTable.endUpdates()
                                    
                                    
                                }
                                else // edit
                                {
                                    
                                    self.HorseEntryArray.replaceObject(at: RowId, with: details)
                                    let indexPath = IndexPath(item:RowId, section: 0)
                                    self.HorseEntryTable.reloadRows(at: [indexPath], with: .none)
                                }
                                
                                print("horse entry array--\(self.HorseEntryArray)")
                                
                                
                            }
                            
                            
                        }
                        else
                        {
                            let alert = UIAlertController(title: "Error", message: JSONDictionary.value(forKey: "message") as? String, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            
                            DispatchQueue.main.async {
                                self.present(alert, animated: true, completion: nil)
                                self.CheckLoader()
                                
                                
                            }
                        }
                        
                        
                    }
                    catch let JSONError as NSError {
                        print("\(JSONError)")
                        let alert = UIAlertController(title: "Error", message: JSONError.localizedDescription as String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        OperationQueue.main.addOperation
                            {
                                self.CheckLoader()
                                self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                    
                }
                else if(httpResponse.statusCode == 400)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else if(httpResponse.statusCode == 500)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        self.CheckLoader()
                    }
                }
                
                
            }
            
            
        })
        
        
        task.resume()
        
        
    }
    
    
    // MARK: - TextField Delegates
    public func textFieldDidBeginEditing(_ textField: UITextField)
    {
        textField.placeholder = nil
        
        RowIndex = textField.tag
        
        print("Text Field Tag:",RowIndex,textField.tag)
        
        
//        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.transitionCurlDown, animations: {
//            self.mainscroll.contentOffset.y = (CGFloat(Int(self.HorseEntryTable.frame.size.height/10) * textField.tag))
//        }, completion: nil)
//
        

    }
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool
    {
        return true
    }
    public func textFieldDidEndEditing(_ textField: UITextField)
    {
        
    }
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        let scalars = string.unicodeScalars
        
        let indexPath = IndexPath(row: textField.tag, section: 0)
        let cell = HorseEntryTable.cellForRow(at: indexPath) as! DataListCell
        
        if textField == cell.txtHorseEntry
        {
            if (scalars[scalars.startIndex].value > 46 && scalars[scalars.startIndex].value < 58)  // string == "0" || string == "1" || string == "2" || string == "3" || string == "4" || string == "5" || string == "6" || string == "7" || string == "8" || string == "9"
            {
            var startString = ""
            if (textField.text != nil)
            {
                startString += textField.text!
            }
            startString += string
                let limitNumber = Int(startString)
            
            if (Double(limitNumber!)>=1 && Double(limitNumber!)<=500)
            {
                return true
            }
            else
            {
                return false
            }
                
            }
            else if (scalars[scalars.startIndex].value == 65533)
            {
                return true
            }
            else
            {
                return false
            }
            
        }
        else
        {
            return true
        }
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
    //  HorseEntryTable.setContentOffset(CGPoint (x: 0, y: 0), animated: true)
        textField.endEditing(true)
        
//        HorseEntryTable.isScrollEnabled = true
//        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.transitionCurlUp, animations: {
//            self.mainscroll.contentOffset.y = 0
//        }, completion: nil)
        
        return false
    }
    
    // MARK: - keyboard Will Show
    @objc func keyboardWillShow(notification: NSNotification)
    {
        let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        
        print(" row index \(RowIndex)")
      //  var y = (HorseEntryTable.frame.origin.y + HorseEntryTable.frame.size.height) - (keyboardSize?.height)!
        
        //   print("height brfore \(SendMessageView.frame.size.height)")
        
         HorseEntryTable.frame = CGRect(x: HorseEntryTable.frame.origin.x,y: HorseEntryTable.frame.origin.y , width: HorseEntryTable.frame.size.width,height: (mainscroll.frame.size.height - HorseEntryTable.frame.origin.y) - (keyboardSize?.height)!);
        
        if HorseEntryArray.count != 0
        {
            let index = IndexPath(row:RowIndex, section: 0) // use your index number or Indexpath
            HorseEntryTable.scrollToRow(at: index,at: .bottom, animated: false)
            
          //  HorseEntryTable.isScrollEnabled = false
        }
       
    }
    // MARK: - keyboard Will hide
    @objc func keyboardWillHide(notification: NSNotification)
    {
        let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        
         HorseEntryTable.frame = CGRect(x: HorseEntryTable.frame.origin.x,y: HorseEntryTable.frame.origin.y , width: HorseEntryTable.frame.size.width,height: (mainscroll.frame.size.height - HorseEntryTable.frame.origin.y));
        
        HorseEntryTable.isScrollEnabled = true
    }
    
    
    @IBAction func ShowClick(_ sender: Any)
    {
        showClass = true
        
        if showList.count>0
        {
            ShowView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
            ShowView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            ShowView.center = self.view.center
            self.view.addSubview(ShowView)
            
            
            let typeView = UIView(frame: CGRect(x: 0, y: ShowView.frame.height-250, width: ShowView.frame.width, height: 250))
            typeView.backgroundColor = UIColor.white
            ShowView.addSubview(typeView)
            
            // Create a Picker
            let pickerView = UIPickerView()
            
            pickerView.delegate = self
            pickerView.dataSource = self as! UIPickerViewDataSource
            
            
            pickerView.frame = CGRect(x: 10, y: 50, width: ShowView.frame.width-20, height: 200)
            pickerView.backgroundColor = UIColor.white
            
            
            // Add DataPicker to the view
            typeView.addSubview(pickerView)
            
            let savebtn:UIButton = UIButton(frame: CGRect(x: typeView.frame.size.width-60, y: pickerView.frame.origin.y-50, width: 50, height: 50))
            savebtn.setTitleColor(GlobalViewController.ThemeColor, for: .normal)
            savebtn.setTitle("Save", for: .normal)
            savebtn.addTarget(self, action:#selector(NPCategoryByDivisionEditViewController.ShowSaveClick), for: .touchUpInside)
            //  savebtn.tag = (sender as AnyObject).tag
            typeView.addSubview(savebtn)
            
            let cancelbtn:UIButton = UIButton(frame: CGRect(x: 10, y: pickerView.frame.origin.y-50, width: 80, height: 50))
            cancelbtn.setTitleColor(GlobalViewController.ThemeColor, for: .normal)
            cancelbtn.setTitle("Cancel", for: .normal)
            cancelbtn.addTarget(self, action:#selector(NPCategoryByDivisionEditViewController.ShowCancelClick), for: .touchUpInside)
            //   cancelbtn.tag = (sender as AnyObject).tag
            typeView.addSubview(cancelbtn)
            
            

        }
        else
        {
            classShowList()
        }
        
        
        
        
    }
    
    
    // MARK: - Class List Api Execute
    func classShowList()
    {
        
        DispatchQueue.main.async {
            self.CheckLoader()
        }
        
        let  url_to_request = GlobalViewController.GLOBALAPI + "show/fetchparticularshowjudge"
        
        print("Full URL:",url_to_request)
        
        let urlString:NSURL = NSURL(string: url_to_request)!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: urlString as URL)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        var parameter : String = ""
        
        parameter = "showid="
        
        print("parameter:",parameter)
        request.httpMethod = "POST"
        request.httpBody = parameter.data(using: String.Encoding.utf8)
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            
            
            (data, response, error) in
            
            
            guard let _:Data = data, let _:URLResponse = response, error == nil
                
                else
            {
                print("error")
                let alert = UIAlertController(title: "Error", message:error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                OperationQueue.main.addOperation
                    {
                        self.CheckLoader()
                        self.present(alert, animated: true, completion: nil)
                }
                return
            }
            //    self.statusCode = [(NSHTTPURLResponse*)response statusCode];
            if let httpResponse = response as? HTTPURLResponse
            {
                
                
                print("statusCode \(httpResponse.statusCode)")
                if (httpResponse.statusCode == 200)
                {
                    
                    let JSONData = data
                    do {
                        let JSON = try JSONSerialization.jsonObject(with: JSONData!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                        guard let JSONDictionary: NSDictionary = JSON as? NSDictionary else {
                            print("Not a Dictionary")
                            // put in function
                            
                            print("JSONDictionary \(JSON)")
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                                
                            }
                            return
                        }
                        
                        print("JSONDictionary \(JSONDictionary)")
                        
                        
                        let status : Bool = JSONDictionary.object(forKey: "status") as! Bool
                        
                        
                        if (status)
                        {
                            DispatchQueue.main.async {
                                self.CheckLoader()
                                
                                let temparray = JSONDictionary.object(forKey: "details") as! NSArray
     
                                
                                self.showList = temparray.mutableCopy() as! NSMutableArray
                                
                                if self.showList.count > 0
                                {
                                    self.ShowView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                                    self.ShowView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
                                    self.ShowView.center = self.view.center
                                    self.view.addSubview(self.ShowView)
                                    
                                    let typeView = UIView(frame: CGRect(x: 0, y: self.ShowView.frame.height-250, width: self.ShowView.frame.width, height: 250))
                                    typeView.backgroundColor = UIColor.white
                                    self.ShowView.addSubview(typeView)
                                    
                                    // Create a Picker
                                    let pickerView = UIPickerView()
                                    
                                    pickerView.delegate = self
                                    pickerView.dataSource = self as UIPickerViewDataSource
                                    
                                    
                                    pickerView.frame = CGRect(x: 10, y: 50, width: self.ShowView.frame.width-20, height: 200)
                                    pickerView.backgroundColor = UIColor.white
                                    
                                    
                                    // Add DataPicker to the view
                                    typeView.addSubview(pickerView)
                                    
                                    let savebtn:UIButton = UIButton(frame: CGRect(x: typeView.frame.size.width-60, y: pickerView.frame.origin.y-50, width: 50, height: 50))
                                    savebtn.setTitleColor(GlobalViewController.ThemeColor, for: .normal)
                                    savebtn.setTitle("Save", for: .normal)
                                    savebtn.addTarget(self, action:#selector(NPCategoryByDivisionEditViewController.ShowSaveClick), for: .touchUpInside)
                                    // savebtn.tag = (sender as AnyObject).tag
                                    typeView.addSubview(savebtn)
                                    
                                    let cancelbtn:UIButton = UIButton(frame: CGRect(x: 10, y: pickerView.frame.origin.y-50, width: 80, height: 50))
                                    cancelbtn.setTitleColor(GlobalViewController.ThemeColor, for: .normal)
                                    cancelbtn.setTitle("Cancel", for: .normal)
                                    cancelbtn.addTarget(self, action:#selector(NPCategoryByDivisionEditViewController.ShowCancelClick), for: .touchUpInside)
                                    // cancelbtn.tag = (sender as AnyObject).tag
                                    typeView.addSubview(cancelbtn)
                                    
                                    
                                    
                                    
                                    //            showTable.isHidden = false
                                    //            showTable.reloadData()
                                }
                                else
                                {
                                    
                                    // self.ClassJudgeTable.reloadData()
                                    //  self.ShowTable.isHidden = true
                                }
                                
                                
                            }
                            
                            
                            
                        }
                        else
                        {
                            
                            
                            DispatchQueue.main.async {

                                
                                self.CheckLoader()
                                
                                
                            }
                        }
                        
                        
                    }
                    catch let JSONError as NSError {
                        let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                        print("Error---\(String(describing: datastring))")
                        print("Error---\(String(describing: data))")
                        print("Error mesg---\(JSONError)")
                        
                        let alert = UIAlertController(title: "Error", message: JSONError.localizedDescription as String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        OperationQueue.main.addOperation
                            {
                                
                                // self.ShowTable.isHidden = true
                                self.CheckLoader()
                                self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                    
                }
                else if(httpResponse.statusCode == 400)
                {
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            // self.ShowTable.isHidden = true
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else if(httpResponse.statusCode == 500)
                {
                    let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print("Error---\(String(describing: datastring))")
                    
                    
                    let alert = UIAlertController(title: "Error", message: "Server failed to respond", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation
                        {
                            //  self.ShowTable.isHidden = true
                            self.CheckLoader()
                            self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        //  self.ShowTable.isHidden = true
                        self.CheckLoader()
                    }
                }
                
                
            }
            
            
        })
        
        
        task.resume()
        
        
    }
    
    
    // MARK: - UIpicker Delegates
    // The number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
      
            return showList.count
        
        
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
//        if showClass
//        {
            let object:NSDictionary = showList.object(at: row) as! NSDictionary
            
            return object.object(forKey: "showname") as! String
            
    //    }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
    if showClass
    {
        showIndex = row
        
    let object:NSDictionary = showList.object(at: row) as! NSDictionary
    
    showNameLbl.textColor = UIColor.black
    showNameLbl.text = object.object(forKey: "showname") as! String
    
    
    ShowId = object.object(forKey: "id") as! String
    
    
    
   // let temp:NSArray = object.object(forKey: "judge") as! NSArray
    

    }
    
}
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel: UILabel? = (view as? UILabel)
        if pickerLabel == nil {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name: "Roboto-Regular", size: 30)
            pickerLabel?.textAlignment = .center
        }
        
        if showClass
        {
            let object:NSDictionary = showList.object(at: row) as! NSDictionary
           
            pickerLabel?.text =  object.object(forKey: "showname") as! String
        }
       
        
        pickerLabel?.textColor = UIColor.black
        
        return pickerLabel!
    }
    
    
    @objc func ShowSaveClick()
    {
        ShowView.removeFromSuperview()

            showClass = false
            
            if (showNameLbl.text == "Choose Show Name")
            {
                showNameLbl.text = "Choose Show Name"
                showNameLbl.textColor = UIColor.red
            }
            else
            {
                UserDefaults.standard.setValue(showNameLbl.text as! String, forKey: "ShowName")
                UserDefaults.standard.setValue(ShowId, forKey: "ShowID")
                
                let object:NSDictionary = showList.object(at: showIndex) as! NSDictionary
                
                
                let temp:NSArray = object.object(forKey: "judge") as! NSArray
                UserDefaults.standard.set(temp , forKey: "JudgeArray")
                
                UserDefaults.standard.set(object.object(forKey: "default_judge") as! NSInteger ,forKey: "defaultJudge")
                HeaderView.showNameHeader.text = showNameLbl.text as! String
                HorseListAPI()
            }
        
        
        
    }
    
    // MARK: - cancel click
    @objc func ShowCancelClick()
    {
        showClass = false
        
        if (UserDefaults.standard.object(forKey: "ShowName") != nil && UserDefaults.standard.object(forKey: "ShowName") as! String != "")
        {
            HeaderView.showNameHeader.text = UserDefaults.standard.object(forKey: "ShowName") as! String
            ShowId = UserDefaults.standard.object(forKey: "ShowID") as! String
            showNameLbl.text = UserDefaults.standard.object(forKey: "ShowName") as! String
        }
        else
        {
            // ShowName = ""
            showNameLbl.text = "Choose Show Name"
        }
            showNameLbl.textColor = UIColor.black
        ShowView.removeFromSuperview()
        
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
