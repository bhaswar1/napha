//
//  GlobalViewController.swift
//  NAPHA
//
//  Created by Priyanka on 01/11/17.
//  Copyright © 2017 Esolz. All rights reserved.
//

import UIKit


class GlobalViewController: UIViewController, Show_menu_delegate, Preference_menu_delegate, File_menu_delegate {
   

    @IBOutlet weak var HeaderView: NPHeaderView!
    @IBOutlet weak var ShowListView: NPShowListView!
    @IBOutlet weak var PreferenceListView: NPPreferenceListView!
    @IBOutlet weak var FileListView: NPFileListView!
    
    static let IsipadPro12 = (UIScreen.main.bounds.size.width==1366) ? true : false
    static let IsipadAir = (UIScreen.main.bounds.size.width==1024) ? true : false
    static let Isiphone6Plus = (UIScreen.main.bounds.size.height==736) ? true : false
    
//    static let ThemeColor = UIColor(red:195.0/255.0, green:149.0/255.0, blue:98.0/255.0, alpha:1.0)
    static let ThemeColor = UIColor(red:198.0/255.0, green:150.0/255.0, blue:92.0/255.0, alpha:1.0)
    
    static let RobotoRegular = "Roboto-Regular"
    static let RobotoBold = "Roboto-Bold"
    
    
    static let GLOBALAPI = "https://esolz.co.in/lab3/napha/index.php/"
    static let GLOBALUserImgThumb : String = "https://esolz.co.in/lab3/Roadsensor/images/users/thumb/"
    
    var showmenuindex : NSInteger = 0
    
     var LoaderView: UIView!
    var myActivityIndicator: UIActivityIndicatorView!

    
   
    
    var OverlayView: UIView!
   

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        // Do any additional setup after loading the view.
        
        HeaderView.btnFile.backgroundColor = UIColor.white
        HeaderView.btnFile.setTitleColor(UIColor.black, for: .normal)
        HeaderView.btnFile.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
        HeaderView.btnFile.layer.shadowColor = UIColor.clear.cgColor
        
        HeaderView.btnShow.backgroundColor = UIColor.white
        HeaderView.btnShow.setTitleColor(UIColor.black, for: .normal)
        HeaderView.btnShow.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
        HeaderView.btnShow.layer.shadowColor = UIColor.clear.cgColor
        
        HeaderView.btnPreferences.backgroundColor = UIColor.white
        HeaderView.btnPreferences.setTitleColor(UIColor.black, for: .normal)
        HeaderView.btnPreferences.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
        HeaderView.btnPreferences.layer.shadowColor = UIColor.clear.cgColor
        
        
        if (self.topViewController()?.isKind(of: NPInitialViewController.self))!
        {
            
            print("NPInitialViewController");

            
            HeaderView.btnFile.addTarget(self, action: #selector(GlobalViewController.FileClick), for: .touchUpInside)
            
            HeaderView.btnShow.addTarget(self, action: #selector(GlobalViewController.ShowClick), for: .touchUpInside)
            
            HeaderView.btnPreferences.addTarget(self, action: #selector(GlobalViewController.PreferenceClick), for: .touchUpInside)
            
            //   leftmenuindex = 1
            
        }
        else if (self.topViewController()?.isKind(of: NPFileViewController.self))!
        {

            print("NPFileViewController");
        //    HeaderView.btnFile.isSelected = true
            
            HeaderView.btnFile.backgroundColor = GlobalViewController.ThemeColor
            HeaderView.btnFile.layer.cornerRadius = self.SetButtonRadius(size: 20)
            HeaderView.btnFile.layer.shadowOffset = CGSize(width:0,height:5)
            // set the radius
            HeaderView.btnFile.layer.shadowRadius = 10
            // change the color of the shadow (has to be CGColor)
            HeaderView.btnFile.layer.shadowColor = GlobalViewController.ThemeColor.cgColor
            // display the shadow
            HeaderView.btnFile.layer.shadowOpacity = 0.8
            HeaderView.btnFile.layer.shadowPath = UIBezierPath(roundedRect:CGRect(x: 20, y: 5, width: HeaderView.btnFile.frame.size.width - 40, height:HeaderView.btnFile.frame.size.height), cornerRadius: 12.0).cgPath
            HeaderView.btnFile.setTitleColor(UIColor.white, for: .normal)
            
            HeaderView.btnFile.titleLabel!.font = UIFont(name:GlobalViewController.RobotoBold, size: 18)

            HeaderView.btnFile.addTarget(self, action: #selector(GlobalViewController.FileClick), for: .touchUpInside)
            
            HeaderView.btnShow.addTarget(self, action: #selector(GlobalViewController.ShowClick), for: .touchUpInside)
            
            HeaderView.btnPreferences.addTarget(self, action: #selector(GlobalViewController.PreferenceClick), for: .touchUpInside)

         //   leftmenuindex = 1

        }
        else if (self.topViewController()?.isKind(of: NPFile_DeleteViewController.self))!
        {
            
            print("NPFileDeleteViewController");
            //    HeaderView.btnFile.isSelected = true
            
            HeaderView.btnFile.backgroundColor = GlobalViewController.ThemeColor
            HeaderView.btnFile.layer.cornerRadius = self.SetButtonRadius(size: 20)
            HeaderView.btnFile.layer.shadowOffset = CGSize(width:0,height:5)
            // set the radius
            HeaderView.btnFile.layer.shadowRadius = 10
            // change the color of the shadow (has to be CGColor)
            HeaderView.btnFile.layer.shadowColor = GlobalViewController.ThemeColor.cgColor
            // display the shadow
            HeaderView.btnFile.layer.shadowOpacity = 0.8
            HeaderView.btnFile.layer.shadowPath = UIBezierPath(roundedRect:CGRect(x: 20, y: 5, width: HeaderView.btnFile.frame.size.width - 40, height:HeaderView.btnFile.frame.size.height), cornerRadius: 12.0).cgPath
            HeaderView.btnFile.setTitleColor(UIColor.white, for: .normal)
            
            HeaderView.btnFile.titleLabel!.font = UIFont(name:GlobalViewController.RobotoBold, size: 18)
            
            HeaderView.btnFile.addTarget(self, action: #selector(GlobalViewController.FileClick), for: .touchUpInside)
            
            HeaderView.btnShow.addTarget(self, action: #selector(GlobalViewController.ShowClick), for: .touchUpInside)
            
            HeaderView.btnPreferences.addTarget(self, action: #selector(GlobalViewController.PreferenceClick), for: .touchUpInside)
            
            //   leftmenuindex = 1
            
        }
        else if (self.topViewController()?.isKind(of: NPFilePrintViewController.self))!
        {
            
            print("NPFilePrintViewController");
            //    HeaderView.btnFile.isSelected = true
            
            HeaderView.btnFile.backgroundColor = GlobalViewController.ThemeColor
            HeaderView.btnFile.layer.cornerRadius = self.SetButtonRadius(size: 20)
            HeaderView.btnFile.layer.shadowOffset = CGSize(width:0,height:5)
            // set the radius
            HeaderView.btnFile.layer.shadowRadius = 10
            // change the color of the shadow (has to be CGColor)
            HeaderView.btnFile.layer.shadowColor = GlobalViewController.ThemeColor.cgColor
            // display the shadow
            HeaderView.btnFile.layer.shadowOpacity = 0.8
            HeaderView.btnFile.layer.shadowPath = UIBezierPath(roundedRect:CGRect(x: 20, y: 5, width: HeaderView.btnFile.frame.size.width - 40, height:HeaderView.btnFile.frame.size.height), cornerRadius: 12.0).cgPath
            HeaderView.btnFile.setTitleColor(UIColor.white, for: .normal)
            
            HeaderView.btnFile.titleLabel!.font = UIFont(name:GlobalViewController.RobotoBold, size: 18)
            
            HeaderView.btnFile.addTarget(self, action: #selector(GlobalViewController.FileClick), for: .touchUpInside)
            
            HeaderView.btnShow.addTarget(self, action: #selector(GlobalViewController.ShowClick), for: .touchUpInside)
            
            HeaderView.btnPreferences.addTarget(self, action: #selector(GlobalViewController.PreferenceClick), for: .touchUpInside)
            
            //   leftmenuindex = 1
            
        }
        else if (self.topViewController()?.isKind(of: NPFileDuplicateViewController.self))!
        {
            
            print("NPFileDuplicateViewController");
            //    HeaderView.btnFile.isSelected = true
            
            HeaderView.btnFile.backgroundColor = GlobalViewController.ThemeColor
            HeaderView.btnFile.layer.cornerRadius = self.SetButtonRadius(size: 20)
            HeaderView.btnFile.layer.shadowOffset = CGSize(width:0,height:5)
            // set the radius
            HeaderView.btnFile.layer.shadowRadius = 10
            // change the color of the shadow (has to be CGColor)
            HeaderView.btnFile.layer.shadowColor = GlobalViewController.ThemeColor.cgColor
            // display the shadow
            HeaderView.btnFile.layer.shadowOpacity = 0.8
            HeaderView.btnFile.layer.shadowPath = UIBezierPath(roundedRect:CGRect(x: 20, y: 5, width: HeaderView.btnFile.frame.size.width - 40, height:HeaderView.btnFile.frame.size.height), cornerRadius: 12.0).cgPath
            HeaderView.btnFile.setTitleColor(UIColor.white, for: .normal)
            
            HeaderView.btnFile.titleLabel!.font = UIFont(name:GlobalViewController.RobotoBold, size: 18)
            
            HeaderView.btnFile.addTarget(self, action: #selector(GlobalViewController.FileClick), for: .touchUpInside)
            
            HeaderView.btnShow.addTarget(self, action: #selector(GlobalViewController.ShowClick), for: .touchUpInside)
            
            HeaderView.btnPreferences.addTarget(self, action: #selector(GlobalViewController.PreferenceClick), for: .touchUpInside)
            
            //   leftmenuindex = 1
            
        }
        else if (self.topViewController()?.isKind(of: NPFileOthersViewController.self))!
        {
            
            print("NPFileOthersViewController");
            //    HeaderView.btnFile.isSelected = true
            
            HeaderView.btnFile.backgroundColor = GlobalViewController.ThemeColor
            HeaderView.btnFile.layer.cornerRadius = self.SetButtonRadius(size: 20)
            HeaderView.btnFile.layer.shadowOffset = CGSize(width:0,height:5)
            // set the radius
            HeaderView.btnFile.layer.shadowRadius = 10
            // change the color of the shadow (has to be CGColor)
            HeaderView.btnFile.layer.shadowColor = GlobalViewController.ThemeColor.cgColor
            // display the shadow
            HeaderView.btnFile.layer.shadowOpacity = 0.8
            HeaderView.btnFile.layer.shadowPath = UIBezierPath(roundedRect:CGRect(x: 20, y: 5, width: HeaderView.btnFile.frame.size.width - 40, height:HeaderView.btnFile.frame.size.height), cornerRadius: 12.0).cgPath
            HeaderView.btnFile.setTitleColor(UIColor.white, for: .normal)
            
            HeaderView.btnFile.titleLabel!.font = UIFont(name:GlobalViewController.RobotoBold, size: 18)
            
            HeaderView.btnFile.addTarget(self, action: #selector(GlobalViewController.FileClick), for: .touchUpInside)
            
            HeaderView.btnShow.addTarget(self, action: #selector(GlobalViewController.ShowClick), for: .touchUpInside)
            
            HeaderView.btnPreferences.addTarget(self, action: #selector(GlobalViewController.PreferenceClick), for: .touchUpInside)
            
            //   leftmenuindex = 1
            
        }
        else if (self.topViewController()?.isKind(of: NPShowSetupViewController.self))!
        {
            
            print("NPShowSetupViewController");
            //    HeaderView.btnFile.isSelected = true
            
            HeaderView.btnShow.backgroundColor = GlobalViewController.ThemeColor
            HeaderView.btnShow.layer.cornerRadius = self.SetButtonRadius(size: 20)
            HeaderView.btnShow.layer.shadowOffset = CGSize(width:0,height:5)
            // set the radius
            HeaderView.btnShow.layer.shadowRadius = 10
            // change the color of the shadow (has to be CGColor)
            HeaderView.btnShow.layer.shadowColor = GlobalViewController.ThemeColor.cgColor
            // display the shadow
            HeaderView.btnShow.layer.shadowOpacity = 0.8
            HeaderView.btnShow.layer.shadowPath = UIBezierPath(roundedRect:CGRect(x: 20, y: 5, width: HeaderView.btnShow.frame.size.width - 40, height:HeaderView.btnShow.frame.size.height), cornerRadius: 12.0).cgPath
            HeaderView.btnShow.setTitleColor(UIColor.white, for: .normal)
            
            HeaderView.btnShow.titleLabel!.font = UIFont(name:GlobalViewController.RobotoBold, size: 18)
            
            HeaderView.btnFile.addTarget(self, action: #selector(GlobalViewController.FileClick), for: .touchUpInside)
            
            HeaderView.btnShow.addTarget(self, action: #selector(GlobalViewController.ShowClick), for: .touchUpInside)
            
            HeaderView.btnPreferences.addTarget(self, action: #selector(GlobalViewController.PreferenceClick), for: .touchUpInside)
            
            showmenuindex = 1
            
        }
        else if (self.topViewController()?.isKind(of: NPClassListViewController.self))!
        {
            
            print("NPClassListViewController");
            //    HeaderView.btnFile.isSelected = true
            
            HeaderView.btnShow.backgroundColor = GlobalViewController.ThemeColor
            HeaderView.btnShow.layer.cornerRadius = self.SetButtonRadius(size: 20)
            HeaderView.btnShow.layer.shadowOffset = CGSize(width:0,height:5)
            // set the radius
            HeaderView.btnShow.layer.shadowRadius = 10
            // change the color of the shadow (has to be CGColor)
            HeaderView.btnShow.layer.shadowColor = GlobalViewController.ThemeColor.cgColor
            // display the shadow
            HeaderView.btnShow.layer.shadowOpacity = 0.8
            HeaderView.btnShow.layer.shadowPath = UIBezierPath(roundedRect:CGRect(x: 20, y: 5, width: HeaderView.btnShow.frame.size.width - 40, height:HeaderView.btnShow.frame.size.height), cornerRadius: 12.0).cgPath
            HeaderView.btnShow.setTitleColor(UIColor.white, for: .normal)
            
            HeaderView.btnShow.titleLabel!.font = UIFont(name:GlobalViewController.RobotoBold, size: 18)
            
            HeaderView.btnFile.addTarget(self, action: #selector(GlobalViewController.FileClick), for: .touchUpInside)
            
            HeaderView.btnShow.addTarget(self, action: #selector(GlobalViewController.ShowClick), for: .touchUpInside)
            HeaderView.btnPreferences.addTarget(self, action: #selector(GlobalViewController.PreferenceClick), for: .touchUpInside)
            
            showmenuindex = 2
            
        }
        else if (self.topViewController()?.isKind(of: NPHorseEntriesViewController.self))!
        {
            
            print("NPHorseEntriesViewController");
            //    HeaderView.btnFile.isSelected = true
            
            HeaderView.btnShow.backgroundColor = GlobalViewController.ThemeColor
            HeaderView.btnShow.layer.cornerRadius = self.SetButtonRadius(size: 20)
            HeaderView.btnShow.layer.shadowOffset = CGSize(width:0,height:5)
            // set the radius
            HeaderView.btnShow.layer.shadowRadius = 10
            // change the color of the shadow (has to be CGColor)
            HeaderView.btnShow.layer.shadowColor = GlobalViewController.ThemeColor.cgColor
            // display the shadow
            HeaderView.btnShow.layer.shadowOpacity = 0.8
            HeaderView.btnShow.layer.shadowPath = UIBezierPath(roundedRect:CGRect(x: 20, y: 5, width: HeaderView.btnShow.frame.size.width - 40, height:HeaderView.btnShow.frame.size.height), cornerRadius: 12.0).cgPath
            HeaderView.btnShow.setTitleColor(UIColor.white, for: .normal)
            
            HeaderView.btnShow.titleLabel!.font = UIFont(name:GlobalViewController.RobotoBold, size: 18)
            
            HeaderView.btnFile.addTarget(self, action: #selector(GlobalViewController.FileClick), for: .touchUpInside)
            
            HeaderView.btnShow.addTarget(self, action: #selector(GlobalViewController.ShowClick), for: .touchUpInside)
            
            HeaderView.btnPreferences.addTarget(self, action: #selector(GlobalViewController.PreferenceClick), for: .touchUpInside)
            
            showmenuindex = 3
            
        }
        else if (self.topViewController()?.isKind(of: NPJudgeCardViewController.self))!
        {
            
            print("NPJudgeCardViewController");
            //    HeaderView.btnFile.isSelected = true
            
            HeaderView.btnShow.backgroundColor = GlobalViewController.ThemeColor
            HeaderView.btnShow.layer.cornerRadius = self.SetButtonRadius(size: 20)
            HeaderView.btnShow.layer.shadowOffset = CGSize(width:0,height:5)
            // set the radius
            HeaderView.btnShow.layer.shadowRadius = 10
            // change the color of the shadow (has to be CGColor)
            HeaderView.btnShow.layer.shadowColor = GlobalViewController.ThemeColor.cgColor
            // display the shadow
            HeaderView.btnShow.layer.shadowOpacity = 0.8
            HeaderView.btnShow.layer.shadowPath = UIBezierPath(roundedRect:CGRect(x: 20, y: 5, width: HeaderView.btnShow.frame.size.width - 40, height:HeaderView.btnShow.frame.size.height), cornerRadius: 12.0).cgPath
            HeaderView.btnShow.setTitleColor(UIColor.white, for: .normal)
            
            HeaderView.btnShow.titleLabel!.font = UIFont(name:GlobalViewController.RobotoBold, size: 18)
            
            HeaderView.btnFile.addTarget(self, action: #selector(GlobalViewController.FileClick), for: .touchUpInside)
            
            HeaderView.btnShow.addTarget(self, action: #selector(GlobalViewController.ShowClick), for: .touchUpInside)
            
            HeaderView.btnPreferences.addTarget(self, action: #selector(GlobalViewController.PreferenceClick), for: .touchUpInside)
            
            showmenuindex = 4
            
        }
        else if (self.topViewController()?.isKind(of: NPCategoryByDivisionEditViewController.self))!
        {
            
            print("NPCategoryByDivisionEditViewController");
            //    HeaderView.btnFile.isSelected = true
            
            HeaderView.btnPreferences.backgroundColor = GlobalViewController.ThemeColor
            HeaderView.btnPreferences.layer.cornerRadius = self.SetButtonRadius(size: 20)
            HeaderView.btnPreferences.layer.shadowOffset = CGSize(width:0,height:5)
            // set the radius
            HeaderView.btnPreferences.layer.shadowRadius = 10
            // change the color of the shadow (has to be CGColor)
            HeaderView.btnPreferences.layer.shadowColor = GlobalViewController.ThemeColor.cgColor
            // display the shadow
            HeaderView.btnPreferences.layer.shadowOpacity = 0.8
            HeaderView.btnPreferences.layer.shadowPath = UIBezierPath(roundedRect:CGRect(x: 20, y: 5, width: HeaderView.btnPreferences.frame.size.width - 40, height:HeaderView.btnPreferences.frame.size.height), cornerRadius: 12.0).cgPath
            HeaderView.btnPreferences.setTitleColor(UIColor.white, for: .normal)
            
            HeaderView.btnPreferences.titleLabel!.font = UIFont(name:GlobalViewController.RobotoBold, size: 18)
            
            HeaderView.btnFile.addTarget(self, action: #selector(GlobalViewController.FileClick), for: .touchUpInside)
            
            HeaderView.btnShow.addTarget(self, action: #selector(GlobalViewController.ShowClick), for: .touchUpInside)
            
            HeaderView.btnPreferences.addTarget(self, action: #selector(GlobalViewController.PreferenceClick), for: .touchUpInside)
            
            //   leftmenuindex = 1
            
        }
       
       
      
    }
    // MARK: - File click
    @objc func FileClick()
    {
        
        if HeaderView.btnFile.isSelected == false
        {
            HeaderView.btnShow.isSelected = false
            HeaderView.btnPreferences.isSelected = false
            
//            HeaderView.btnPreferences.backgroundColor = UIColor.clear
//            HeaderView.btnPreferences.layer.shadowColor = UIColor.clear.cgColor
//            HeaderView.btnShow.backgroundColor = UIColor.clear
//            HeaderView.btnShow.layer.shadowColor = UIColor.clear.cgColor
//
//            HeaderView.btnShow.setTitleColor(UIColor.black, for: .normal)
//            HeaderView.btnShow.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
//
//            HeaderView.btnPreferences.setTitleColor(UIColor.black, for: .normal)
//            HeaderView.btnPreferences.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
            
            ShowListView.isHidden = true
            PreferenceListView.isHidden = true
            
            if (self.topViewController()?.isKind(of: NPFileViewController.self))! || (self.topViewController()?.isKind(of: NPFile_DeleteViewController.self))! || (self.topViewController()?.isKind(of: NPFilePrintViewController.self))! || (self.topViewController()?.isKind(of: NPFileDuplicateViewController.self))! || (self.topViewController()?.isKind(of: NPFileOthersViewController.self))!
            {
                HeaderView.btnPreferences.backgroundColor = UIColor.clear
                HeaderView.btnPreferences.layer.shadowColor = UIColor.clear.cgColor
                
                HeaderView.btnPreferences.setTitleColor(UIColor.black, for: .normal)
                HeaderView.btnPreferences.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
                
                HeaderView.btnShow.backgroundColor = UIColor.clear
                HeaderView.btnShow.layer.shadowColor = UIColor.clear.cgColor
                
                HeaderView.btnShow.setTitleColor(UIColor.black, for: .normal)
                HeaderView.btnShow.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
                
            }
            else if (self.topViewController()?.isKind(of: NPCategoryByDivisionEditViewController.self))!
            {
               
                HeaderView.btnShow.backgroundColor = UIColor.clear
                HeaderView.btnShow.layer.shadowColor = UIColor.clear.cgColor
                
                HeaderView.btnShow.setTitleColor(UIColor.black, for: .normal)
                HeaderView.btnShow.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
                
            }
            else if (self.topViewController()?.isKind(of: NPShowSetupViewController.self))! || (self.topViewController()?.isKind(of: NPHorseEntriesViewController.self))! || (self.topViewController()?.isKind(of: NPClassListViewController.self))! || (self.topViewController()?.isKind(of: NPJudgeCardViewController.self))!
            {
                HeaderView.btnPreferences.backgroundColor = UIColor.clear
                HeaderView.btnPreferences.layer.shadowColor = UIColor.clear.cgColor
                
                HeaderView.btnPreferences.setTitleColor(UIColor.black, for: .normal)
                HeaderView.btnPreferences.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
                
            }
            else if (self.topViewController()?.isKind(of: NPInitialViewController.self))!
            {
                HeaderView.btnPreferences.backgroundColor = UIColor.clear
                HeaderView.btnPreferences.layer.shadowColor = UIColor.clear.cgColor
                
                HeaderView.btnPreferences.setTitleColor(UIColor.black, for: .normal)
                HeaderView.btnPreferences.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
                
                HeaderView.btnShow.backgroundColor = UIColor.clear
                HeaderView.btnShow.layer.shadowColor = UIColor.clear.cgColor
                
                HeaderView.btnShow.setTitleColor(UIColor.black, for: .normal)
                HeaderView.btnShow.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
            }
            
            
            
            
            HeaderView.btnFile.isSelected = true
            HeaderView.btnShow.isSelected = false
            
            FileListView.File_delegate = self
            
            FileListView.frame = CGRect(x: (HeaderView.btnFile.frame.origin.x + HeaderView.btnFile.frame.size.width/2) - FileListView.frame.size.width/1.6, y: HeaderView.btnFile.frame.origin.y+HeaderView.btnFile.frame.size.height+15, width: FileListView.frame.size.width, height: FileListView.frame.size.height)
            
            HeaderView.btnFile.backgroundColor = GlobalViewController.ThemeColor
            HeaderView.btnFile.layer.cornerRadius = self.SetButtonRadius(size: 20)
            HeaderView.btnFile.layer.shadowOffset = CGSize(width:0,height:5)
            // set the radius
            HeaderView.btnFile.layer.shadowRadius = 10
            // change the color of the shadow (has to be CGColor)
            HeaderView.btnFile.layer.shadowColor = GlobalViewController.ThemeColor.cgColor
            // display the shadow
            HeaderView.btnFile.layer.shadowOpacity = 0.8
            HeaderView.btnFile.layer.shadowPath = UIBezierPath(roundedRect:CGRect(x: 20, y: 5, width: HeaderView.btnFile.frame.size.width - 40, height:HeaderView.btnFile.frame.size.height), cornerRadius: 12.0).cgPath
            HeaderView.btnFile.setTitleColor(UIColor.white, for: .normal)
            HeaderView.btnFile.titleLabel!.font = UIFont(name:GlobalViewController.RobotoBold, size: 18)
            
            // selected row color changed to bold
            if(self.showmenuindex > 0)
            {
                // selected row color black color
                let indexPath = IndexPath(row: self.showmenuindex - 1, section: 0)
                let cell = self.PreferenceListView.PrefTable.cellForRow(at: indexPath)
                cell?.textLabel?.font = UIFont(name:GlobalViewController.RobotoBold, size: 18)
            }
            
            FileListView.isHidden = false
        }
        else if HeaderView.btnFile.isSelected == true
        {
            HeaderView.btnFile.isSelected = false
            
            if (self.topViewController()?.isKind(of: NPFileViewController.self))! || (self.topViewController()?.isKind(of: NPFilePrintViewController.self))! || (self.topViewController()?.isKind(of: NPFileDuplicateViewController.self))! || (self.topViewController()?.isKind(of: NPFile_DeleteViewController.self))! || (self.topViewController()?.isKind(of: NPFileOthersViewController.self))!
            {
            }
            else
            {
                
                
                
                HeaderView.btnFile.backgroundColor = UIColor.white
                HeaderView.btnFile.setTitleColor(UIColor.black, for: .normal)
                HeaderView.btnFile.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
                HeaderView.btnFile.layer.shadowColor = UIColor.clear.cgColor
            }
            
            
            
            FileListView.isHidden = true
        }
        
        
        
//        let loginPageView = self.storyboard?.instantiateViewController(withIdentifier: "NPFileViewController") as! NPFileViewController
//        self.navigationController?.pushViewController(loginPageView, animated: false)
    }
    // MARK: - Show click
    @objc func ShowClick()
    {
        if HeaderView.btnShow.isSelected == false
        {
            HeaderView.btnPreferences.isSelected = false
            HeaderView.btnFile.isSelected = false
            
//            HeaderView.btnFile.backgroundColor = UIColor.clear
//            HeaderView.btnFile.layer.shadowColor = UIColor.clear.cgColor
//            HeaderView.btnPreferences.backgroundColor = UIColor.clear
//            HeaderView.btnPreferences.layer.shadowColor = UIColor.clear.cgColor
//
//            HeaderView.btnFile.setTitleColor(UIColor.black, for: .normal)
//            HeaderView.btnFile.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
//
//            HeaderView.btnPreferences.setTitleColor(UIColor.black, for: .normal)
//            HeaderView.btnPreferences.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
            
            PreferenceListView.isHidden = true
            FileListView.isHidden = true
            
            HeaderView.btnShow.isSelected = true
            HeaderView.btnPreferences.isSelected = false
            
            
            if (self.topViewController()?.isKind(of: NPFileViewController.self))! || (self.topViewController()?.isKind(of: NPFile_DeleteViewController.self))! || (self.topViewController()?.isKind(of: NPFilePrintViewController.self))! || (self.topViewController()?.isKind(of: NPFileDuplicateViewController.self))! || (self.topViewController()?.isKind(of: NPFileOthersViewController.self))!
            {
                HeaderView.btnPreferences.backgroundColor = UIColor.clear
                HeaderView.btnPreferences.layer.shadowColor = UIColor.clear.cgColor
                
                HeaderView.btnPreferences.setTitleColor(UIColor.black, for: .normal)
                HeaderView.btnPreferences.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
                
            }
            else if (self.topViewController()?.isKind(of: NPCategoryByDivisionEditViewController.self))!
            {
                
                HeaderView.btnFile.backgroundColor = UIColor.clear
                HeaderView.btnFile.layer.shadowColor = UIColor.clear.cgColor
                
                HeaderView.btnFile.setTitleColor(UIColor.black, for: .normal)
                HeaderView.btnFile.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
                
            }
            else if (self.topViewController()?.isKind(of: NPShowSetupViewController.self))! || (self.topViewController()?.isKind(of: NPHorseEntriesViewController.self))! || (self.topViewController()?.isKind(of: NPClassListViewController.self))! || (self.topViewController()?.isKind(of: NPJudgeCardViewController.self))!
            {
                HeaderView.btnPreferences.backgroundColor = UIColor.clear
                HeaderView.btnPreferences.layer.shadowColor = UIColor.clear.cgColor
                
                HeaderView.btnPreferences.setTitleColor(UIColor.black, for: .normal)
                HeaderView.btnPreferences.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
                
                HeaderView.btnFile.backgroundColor = UIColor.clear
                HeaderView.btnFile.layer.shadowColor = UIColor.clear.cgColor
                
                HeaderView.btnFile.setTitleColor(UIColor.black, for: .normal)
                HeaderView.btnFile.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
                
            }
            else if (self.topViewController()?.isKind(of: NPInitialViewController.self))!
            {
                HeaderView.btnFile.backgroundColor = UIColor.clear
                HeaderView.btnFile.layer.shadowColor = UIColor.clear.cgColor
                
                HeaderView.btnFile.setTitleColor(UIColor.black, for: .normal)
                HeaderView.btnFile.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
                
                HeaderView.btnPreferences.backgroundColor = UIColor.clear
                HeaderView.btnPreferences.layer.shadowColor = UIColor.clear.cgColor
                
                HeaderView.btnPreferences.setTitleColor(UIColor.black, for: .normal)
                HeaderView.btnPreferences.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
            }
            
            
            ShowListView.Show_delegate = self
            
             ShowListView.frame = CGRect(x: (HeaderView.btnShow.frame.origin.x + HeaderView.btnShow.frame.size.width/2) - ShowListView.frame.size.width/2, y: ShowListView.frame.origin.y, width: ShowListView.frame.size.width, height: ShowListView.frame.size.height)
            
            HeaderView.btnShow.backgroundColor = GlobalViewController.ThemeColor
            HeaderView.btnShow.layer.cornerRadius = self.SetButtonRadius(size: 20)
            HeaderView.btnShow.layer.shadowOffset = CGSize(width:0,height:5)
            // set the radius
            HeaderView.btnShow.layer.shadowRadius = 10
            // change the color of the shadow (has to be CGColor)
            HeaderView.btnShow.layer.shadowColor = GlobalViewController.ThemeColor.cgColor
            // display the shadow
            HeaderView.btnShow.layer.shadowOpacity = 0.8
            HeaderView.btnShow.layer.shadowPath = UIBezierPath(roundedRect:CGRect(x: 20, y: 5, width: HeaderView.btnShow.frame.size.width - 40, height:HeaderView.btnFile.frame.size.height), cornerRadius: 12.0).cgPath
            HeaderView.btnShow.setTitleColor(UIColor.white, for: .normal)
            HeaderView.btnShow.titleLabel!.font = UIFont(name:GlobalViewController.RobotoBold, size: 18)
            
            // selected row color changed to bold
            if(self.showmenuindex > 0)
            {
                // selected row color black color
                let indexPath = IndexPath(row: self.showmenuindex - 1, section: 0)
                let cell = self.ShowListView.ListTable.cellForRow(at: indexPath)
                cell?.textLabel?.font = UIFont(name:GlobalViewController.RobotoBold, size: 18)
            }
            
            ShowListView.isHidden = false
        }
        else if HeaderView.btnShow.isSelected == true
        {
            HeaderView.btnShow.isSelected = false
            
            if (self.topViewController()?.isKind(of: NPShowSetupViewController.self))! || (self.topViewController()?.isKind(of: NPHorseEntriesViewController.self))! || (self.topViewController()?.isKind(of: NPClassListViewController.self))! || (self.topViewController()?.isKind(of: NPJudgeCardViewController.self))!
            {
            }
            else
            {
                HeaderView.btnShow.backgroundColor = UIColor.white
                HeaderView.btnShow.setTitleColor(UIColor.black, for: .normal)
                HeaderView.btnShow.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
                HeaderView.btnShow.layer.shadowColor = UIColor.clear.cgColor
            }
           
            
            
            ShowListView.isHidden = true
        }
    }
    // MARK: - preference click
    @objc func PreferenceClick()
    {
        
            if HeaderView.btnPreferences.isSelected == false
            {
                HeaderView.btnShow.isSelected = false
                HeaderView.btnFile.isSelected = false
                
//                HeaderView.btnFile.backgroundColor = UIColor.clear
//                HeaderView.btnFile.layer.shadowColor = UIColor.clear.cgColor
//
//                HeaderView.btnShow.backgroundColor = UIColor.clear
//                HeaderView.btnShow.layer.shadowColor = UIColor.clear.cgColor
//
//                HeaderView.btnShow.setTitleColor(UIColor.black, for: .normal)
//                HeaderView.btnShow.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
//                
//                HeaderView.btnFile.setTitleColor(UIColor.black, for: .normal)
//                HeaderView.btnFile.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
                
                ShowListView.isHidden = true
                FileListView.isHidden = true
                

                if (self.topViewController()?.isKind(of: NPFileViewController.self))! || (self.topViewController()?.isKind(of: NPFile_DeleteViewController.self))! || (self.topViewController()?.isKind(of: NPFilePrintViewController.self))! || (self.topViewController()?.isKind(of: NPFileDuplicateViewController.self))! || (self.topViewController()?.isKind(of: NPFileOthersViewController.self))!
                {
                    HeaderView.btnShow.backgroundColor = UIColor.clear
                    HeaderView.btnShow.layer.shadowColor = UIColor.clear.cgColor
                    
                    HeaderView.btnShow.setTitleColor(UIColor.black, for: .normal)
                    HeaderView.btnShow.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
                    
                }
                else if (self.topViewController()?.isKind(of: NPCategoryByDivisionEditViewController.self))!
                {
                    
                    HeaderView.btnFile.backgroundColor = UIColor.clear
                    HeaderView.btnFile.layer.shadowColor = UIColor.clear.cgColor
                    
                    HeaderView.btnFile.setTitleColor(UIColor.black, for: .normal)
                    HeaderView.btnFile.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
                    
                    HeaderView.btnShow.backgroundColor = UIColor.clear
                    HeaderView.btnShow.layer.shadowColor = UIColor.clear.cgColor
                    
                    HeaderView.btnShow.setTitleColor(UIColor.black, for: .normal)
                    HeaderView.btnShow.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
                    
                }
                else if (self.topViewController()?.isKind(of: NPShowSetupViewController.self))! || (self.topViewController()?.isKind(of: NPHorseEntriesViewController.self))! || (self.topViewController()?.isKind(of: NPClassListViewController.self))! || (self.topViewController()?.isKind(of: NPJudgeCardViewController.self))!
                {
                    
                    HeaderView.btnFile.backgroundColor = UIColor.clear
                    HeaderView.btnFile.layer.shadowColor = UIColor.clear.cgColor
                    
                    HeaderView.btnFile.setTitleColor(UIColor.black, for: .normal)
                    HeaderView.btnFile.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
                    
                }
                else if (self.topViewController()?.isKind(of: NPInitialViewController.self))!
                {
                    HeaderView.btnFile.backgroundColor = UIColor.clear
                    HeaderView.btnFile.layer.shadowColor = UIColor.clear.cgColor
                    
                    HeaderView.btnFile.setTitleColor(UIColor.black, for: .normal)
                    HeaderView.btnFile.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
                    
                    HeaderView.btnShow.backgroundColor = UIColor.clear
                    HeaderView.btnShow.layer.shadowColor = UIColor.clear.cgColor
                    
                    HeaderView.btnShow.setTitleColor(UIColor.black, for: .normal)
                    HeaderView.btnShow.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
                }
                
                
                
                HeaderView.btnPreferences.isSelected = true
                HeaderView.btnShow.isSelected = false
                
                PreferenceListView.Preference_delegate = self
                
                PreferenceListView.frame = CGRect(x: (HeaderView.btnPreferences.frame.origin.x + HeaderView.btnPreferences.frame.size.width/2) - PreferenceListView.frame.size.width/1.6, y: HeaderView.btnPreferences.frame.origin.y+HeaderView.btnPreferences.frame.size.height+15, width: PreferenceListView.frame.size.width, height: PreferenceListView.frame.size.height)
                
                HeaderView.btnPreferences.backgroundColor = GlobalViewController.ThemeColor
                HeaderView.btnPreferences.layer.cornerRadius = self.SetButtonRadius(size: 20)
                HeaderView.btnPreferences.layer.shadowOffset = CGSize(width:0,height:5)
                // set the radius
                HeaderView.btnPreferences.layer.shadowRadius = 10
                // change the color of the shadow (has to be CGColor)
                HeaderView.btnPreferences.layer.shadowColor = GlobalViewController.ThemeColor.cgColor
                // display the shadow
                HeaderView.btnPreferences.layer.shadowOpacity = 0.8
                HeaderView.btnPreferences.layer.shadowPath = UIBezierPath(roundedRect:CGRect(x: 20, y: 5, width: HeaderView.btnPreferences.frame.size.width - 40, height:HeaderView.btnFile.frame.size.height), cornerRadius: 12.0).cgPath
                HeaderView.btnPreferences.setTitleColor(UIColor.white, for: .normal)
                HeaderView.btnPreferences.titleLabel!.font = UIFont(name:GlobalViewController.RobotoBold, size: 18)
                
                // selected row color changed to bold
                if(self.showmenuindex > 0)
                {
                    // selected row color black color
                    let indexPath = IndexPath(row: self.showmenuindex - 1, section: 0)
                    let cell = self.PreferenceListView.PrefTable.cellForRow(at: indexPath)
                    cell?.textLabel?.font = UIFont(name:GlobalViewController.RobotoBold, size: 18)
                }
                
                PreferenceListView.isHidden = false
            }
            else if HeaderView.btnPreferences.isSelected == true
            {
                HeaderView.btnPreferences.isSelected = false
                
                if (self.topViewController()?.isKind(of: NPCategoryByDivisionEditViewController.self))!
                {
                    
                }
                else
                {
                
                
                    HeaderView.btnPreferences.backgroundColor = UIColor.white
                    HeaderView.btnPreferences.setTitleColor(UIColor.black, for: .normal)
                    HeaderView.btnPreferences.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
                    HeaderView.btnPreferences.layer.shadowColor = UIColor.clear.cgColor
                }
                
                
                
                PreferenceListView.isHidden = true
        }
        
        
        
//        let loginPageView = self.storyboard?.instantiateViewController(withIdentifier: "NPCategoryByDivisionEditViewController") as! NPCategoryByDivisionEditViewController
//        self.navigationController?.pushViewController(loginPageView, animated: false)
    }
    
    
    
    // MARK: - File menu delegate
    func file_action_method(sender: NSInteger?) {
        
        if (sender==0)
        {
            
            FileListView.isHidden = true
            HeaderView.btnFile.isSelected = false
            
            let FileView = self.storyboard?.instantiateViewController(withIdentifier: "NPFileViewController") as! NPFileViewController
            self.navigationController?.pushViewController(FileView, animated: false)
            
        }
        else if (sender == 1)
       {
            FileListView.isHidden = true
            HeaderView.btnFile.isSelected = false
        
            let FileView = self.storyboard?.instantiateViewController(withIdentifier: "NPFilePrintViewController") as! NPFilePrintViewController
            self.navigationController?.pushViewController(FileView, animated: false)
        
        }
        else if (sender == 2)
        {
         
            FileListView.isHidden = true
            HeaderView.btnFile.isSelected = false
            
            let FileView = self.storyboard?.instantiateViewController(withIdentifier: "NPFileDuplicateViewController") as! NPFileDuplicateViewController
            self.navigationController?.pushViewController(FileView, animated: false)
        }
        else if (sender == 3)
        {
            FileListView.isHidden = true
            HeaderView.btnFile.isSelected = false
            
            let FileView = self.storyboard?.instantiateViewController(withIdentifier: "NPFileDeleteViewController") as! NPFile_DeleteViewController
            self.navigationController?.pushViewController(FileView, animated: false)
            
        }
        else if (sender == 4)
        {
            
            FileListView.isHidden = true
            HeaderView.btnFile.isSelected = false
            
            UIControl().sendAction(#selector(NSXPCConnection.suspend),
                                   to: UIApplication.shared, for: nil)
            
//            let FileView = self.storyboard?.instantiateViewController(withIdentifier: "NPFileOthersViewController") as! NPFileOthersViewController
//            self.navigationController?.pushViewController(FileView, animated: false)
            
        }
        
        
    }
    
    // MARK: - Preference menu delegate
    func pref_action_method(sender: NSInteger?) {
        
        if (sender==0)
        {
//            if (self.topViewController()?.isKind(of: NPCategoryByDivisionEditViewController.self))!
//            {
//                PreferenceListView.isHidden = true
//                HeaderView.btnPreferences.isSelected = false
//            }
//            else
//            {
            
            PreferenceListView.isHidden = true
           HeaderView.btnPreferences.isSelected = false
            
                if (UserDefaults.standard.object(forKey: "ShowName") != nil && UserDefaults.standard.object(forKey: "ShowName") as! String != "")
                {
                    
                    let alert = UIAlertController(title: nil, message: "Do you want to Leave \(UserDefaults.standard.object(forKey: "ShowName") as! String)?", preferredStyle: UIAlertControllerStyle.alert)
                    
                    let yesAction = UIAlertAction.init(title: "Yes", style: UIAlertActionStyle.default, handler: { _ in
                        print("New Entry")
                        
                        UserDefaults.standard.set("", forKey: "ShowName")
                        UserDefaults.standard.set("", forKey: "ShowID")
                        
                        let categoryDivEditView = self.storyboard?.instantiateViewController(withIdentifier: "NPCategoryByDivisionEditViewController") as! NPCategoryByDivisionEditViewController
                    
                       categoryDivEditView.comingFrom = "General Preference";
                        self.navigationController?.pushViewController(categoryDivEditView, animated: false)
                        
                        
                    })
                    
                    let noAction = UIAlertAction.init(title: "No", style: UIAlertActionStyle.default, handler: { _ in
                        print("Fetch Entry")
                        
                        self.ShowListView.isHidden = true
                        self.HeaderView.btnShow.isSelected = false
                        
                        if (self.topViewController()?.isKind(of: NPCategoryByDivisionEditViewController.self))!
                        {
                            
                        }
                        else
                        {
                            self.HeaderView.btnPreferences.backgroundColor = UIColor.white
                            
                            self.self.HeaderView.btnPreferences.setTitleColor(UIColor.black, for: .normal)
                            self.HeaderView.btnPreferences.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
                            self.HeaderView.btnPreferences.layer.shadowColor = UIColor.clear.cgColor
                        }
                        
                    })
                    
                    alert.addAction(noAction)
                    alert.addAction(yesAction)
                    self.present(alert, animated: true, completion: nil)
                    
                    
                }
                else
                {
                        let categoryDivEditView = self.storyboard?.instantiateViewController(withIdentifier: "NPCategoryByDivisionEditViewController") as! NPCategoryByDivisionEditViewController
                    
                        categoryDivEditView.comingFrom = "General Preference";
                    self.navigationController?.pushViewController(categoryDivEditView, animated: false)
                    
                }
                
            
            
            
//                let categoryDivEditView = self.storyboard?.instantiateViewController(withIdentifier: "NPCategoryByDivisionEditViewController") as! NPCategoryByDivisionEditViewController
//
//                categoryDivEditView.comingFrom = "General Preference"; self.navigationController?.pushViewController(categoryDivEditView, animated: false)
            
           // }
        }
        else if (sender == 1)
        {
//            if (self.topViewController()?.isKind(of: NPCategoryByDivisionEditViewController.self))!
//            {
//                PreferenceListView.isHidden = true
//                HeaderView.btnPreferences.isSelected = false
//            }
//            else
//            {
            
            PreferenceListView.isHidden = true
            HeaderView.btnPreferences.isSelected = false
            
                let categoryDivEditView = self.storyboard?.instantiateViewController(withIdentifier: "NPCategoryByDivisionEditViewController") as! NPCategoryByDivisionEditViewController
                self.navigationController?.pushViewController(categoryDivEditView, animated: false)
                
          //  }
        }
       
        
    }
    
    
    // MARK: - Show menu delegate
    func action_method(sender: NSInteger?) {
        
        if (sender==0)
        {
            if (self.topViewController()?.isKind(of: NPShowSetupViewController.self))!
            {
                
                ShowListView.isHidden = true
                HeaderView.btnShow.isSelected = false
                
               


            }
            else
            {
                if (UserDefaults.standard.object(forKey: "ShowName") != nil && UserDefaults.standard.object(forKey: "ShowName") as! String != "")
                {
                
                let alert = UIAlertController(title: nil, message: "Do you want to Leave \(UserDefaults.standard.object(forKey: "ShowName") as! String)?", preferredStyle: UIAlertControllerStyle.alert)
                
                let yesAction = UIAlertAction.init(title: "Yes", style: UIAlertActionStyle.default, handler: { _ in
                    print("New Entry")
                    
                    UserDefaults.standard.set("", forKey: "ShowName")
                    UserDefaults.standard.set("", forKey: "ShowID")
                    
                    let loginPageView = self.storyboard?.instantiateViewController(withIdentifier: "NPShowSetupViewController") as! NPShowSetupViewController
                    self.navigationController?.pushViewController(loginPageView, animated: false)
                    
                    
                })
                
                let noAction = UIAlertAction.init(title: "No", style: UIAlertActionStyle.default, handler: { _ in
                    print("Fetch Entry")
                    
                    self.ShowListView.isHidden = true
                    self.HeaderView.btnShow.isSelected = false
                    
                     if (self.topViewController()?.isKind(of: NPShowSetupViewController.self))!
                     {
                    
                    }
                    else
                     {
                        self.HeaderView.btnShow.backgroundColor = UIColor.white
                        
                        self.self.HeaderView.btnShow.setTitleColor(UIColor.black, for: .normal)
                        self.HeaderView.btnShow.titleLabel!.font = UIFont(name:GlobalViewController.RobotoRegular, size: 18)
                        self.HeaderView.btnShow.layer.shadowColor = UIColor.clear.cgColor
                    }
                    
                })
                
                alert.addAction(noAction)
                alert.addAction(yesAction)
                self.present(alert, animated: true, completion: nil)
                
                
            }
            else
                {
                    let loginPageView = self.storyboard?.instantiateViewController(withIdentifier: "NPShowSetupViewController") as! NPShowSetupViewController
                    self.navigationController?.pushViewController(loginPageView, animated: false)
                    
                }
                
            }
        }
        else if (sender == 1)
        {
            if (self.topViewController()?.isKind(of: NPClassListViewController.self))!
            {
                ShowListView.isHidden = true
                HeaderView.btnShow.isSelected = false
            }
            else
            {
                
                let classListView = self.storyboard?.instantiateViewController(withIdentifier: "NPClassListViewController") as! NPClassListViewController
                self.navigationController?.pushViewController(classListView, animated: false)
                
            }
        }
        else if (sender==2)
        {
            if (self.topViewController()?.isKind(of: NPHorseEntriesViewController.self))!
            {
                
                ShowListView.isHidden = true
                HeaderView.btnShow.isSelected = false
                
                
            }
            else
            {
                let loginPageView = self.storyboard?.instantiateViewController(withIdentifier: "NPHorseEntriesViewController") as! NPHorseEntriesViewController
                self.navigationController?.pushViewController(loginPageView, animated: false)
            }
        }
        else if (sender == 3)
        {
            if (self.topViewController()?.isKind(of: NPJudgeCardViewController.self))!
            {
                ShowListView.isHidden = true
                HeaderView.btnShow.isSelected = false
            }
            else
            {
                
                let classListView = self.storyboard?.instantiateViewController(withIdentifier: "NPJudgeCardViewController") as! NPJudgeCardViewController
                self.navigationController?.pushViewController(classListView, animated: false)
                
            }
        }
        
    }
    // MARK: - text field left padding
    func txtPadding(textfield : UITextField)
    {
        let paddingView = UIView(frame:CGRect(x: 0, y: 0, width: 30, height: 50))
        //        if(GlobalViewController.Isiphone5)
        //        {
        //            paddingView.frame = CGRect(x: 0, y: 0, width: 38, height: 30)
        //        }
        
        textfield.leftView=paddingView;
        textfield.leftViewMode = UITextFieldViewMode.always
    }
    // MARK: - string blank check after removing space
    func BlankOrNot(_ value:  String) -> String
    {
        
        
        let whitespaceSet = CharacterSet.whitespaces
        
        return value.trimmingCharacters(in: whitespaceSet);
    }
    // MARK: - loader view
    func CheckLoader()
    {
       // if(self.view.subviews.contains(LoaderView))
        if((LoaderView) != nil && self.view.subviews.contains(LoaderView))
        {
             DispatchQueue.main.async {
               
                self.myActivityIndicator.removeFromSuperview()
                self.LoaderView.removeFromSuperview()
                
                self.view.isUserInteractionEnabled=true;
            }
            
        }
        else
        {
           // LoaderView.removeFromSuperview()
            self.view.isUserInteractionEnabled=false;
            
            LoaderView=UIView(frame: CGRect(x:0,y:0,width:self.view.frame.size.width, height:self.view.frame.size.height))
            LoaderView.backgroundColor=UIColor.black.withAlphaComponent(0.3)
            self.view.addSubview(LoaderView)
            
             myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
            
            // Position Activity Indicator in the center of the main view
            myActivityIndicator.center = self.view.center
            // If needed, you can prevent Acivity Indicator from hiding when stopAnimating() is called
            myActivityIndicator.hidesWhenStopped = false
            myActivityIndicator.startAnimating()
            self.view.addSubview(myActivityIndicator)
        }
    }
    
    // MARK: - current view controller
    func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
   
    // MARK: - change button radius according to screen size
    func SetButtonRadius(size : CGFloat) -> CGFloat
    {
      //  var size1 = 20.0
        var size1 = size
        
        if(GlobalViewController.IsipadPro12)
        {
             size1 += 4.0
        }
//        else if(GlobalViewController.Isiphone6)
//        {
//            size1 += 1.0
//        }
//        else if(GlobalViewController.Isiphone6Plus)
//        {
//            size1 += 4.0
//        }
        
        
        return CGFloat(size1)
    }
   
    // MARK: - change font size according to screen size
    func Get_fontSize(size: Float) -> Float
    {
        var size1 = size
        //        if(GlobalViewController.Isiphone5)
        //        {
        //            size1 += 1.0
        //        }
//        if(GlobalViewController.Isiphone6)
//        {
//            size1 += 1.0
//        }
//        else if(GlobalViewController.Isiphone6Plus)
//        {
//            size1 += 2.0
//        }
        
        return size1
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
