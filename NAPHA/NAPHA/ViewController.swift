//
//  ViewController.swift
//  NAPHA
//
//  Created by Priyanka on 14/02/18.
//  Copyright © 2018 Esolz. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
         self.perform(#selector(goHome), with:nil, afterDelay: 1)
    }

    @objc func goHome()
    {
        let InitialPageView = self.storyboard?.instantiateViewController(withIdentifier: "NPInitialViewController") as! NPInitialViewController
        self.navigationController?.pushViewController(InitialPageView, animated: false)
        
        
//        let FilePageView = self.storyboard?.instantiateViewController(withIdentifier: "NPFileViewController") as! NPFileViewController
//        self.navigationController?.pushViewController(FilePageView, animated: false)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

