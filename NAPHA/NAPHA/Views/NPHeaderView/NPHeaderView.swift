//
//  NPHeaderView.swift
//  NAPHA
//
//  Created by Priyanka on 15/02/18.
//  Copyright © 2018 Esolz. All rights reserved.
//

import UIKit

class NPHeaderView: UIView {

    @IBOutlet var view: UIView!
    
    @IBOutlet weak var btnFile: UIButton!
    @IBOutlet weak var btnShow: UIButton!
    @IBOutlet weak var btnPreferences: UIButton!
    @IBOutlet weak var showNameHeader: UILabel!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        Bundle.main.loadNibNamed("NPHeaderView", owner: self, options: nil)
        self.addSubview(self.view);    // adding the top level view to the view hierarchy
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
