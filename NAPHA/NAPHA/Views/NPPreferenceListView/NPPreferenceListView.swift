//
//  NPPreferenceListView.swift
//  NAPHA
//
//  Created by admin on 28/02/18.
//  Copyright © 2018 Esolz. All rights reserved.
//

import UIKit
protocol Preference_menu_delegate: class {
    func pref_action_method(sender: NSInteger?)
}

class NPPreferenceListView: UIView, UITableViewDelegate, UITableViewDataSource
{
    
    @IBOutlet var View: UIView!
    @IBOutlet weak var PrefTable: UITableView!
    
    weak var Preference_delegate: Preference_menu_delegate?
    
    var ListArray : NSMutableArray = ["General Preferences", "Show Preferences"]
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        Bundle.main.loadNibNamed("NPPreferenceListView", owner: self, options: nil)
        self.addSubview(self.View);    // adding the top level view to the view hierarchy
        
        
        PrefTable.backgroundColor = UIColor(red:198.0/255.0, green:150.0/255.0, blue:92.0/255.0, alpha:1.0)
    }
    // MARK: - Tableview delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return ListArray.count;
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Table view cells are reused and should be dequeued using a cell identifier.
        
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
        
        cell.textLabel?.text = ListArray[indexPath.row] as? String
        cell.textLabel?.textColor = UIColor.white
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.font = UIFont(name:"Roboto-Regular", size: 18)
        
        cell.selectionStyle = .none
        
        cell.backgroundColor = UIColor.clear
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        Preference_delegate?.pref_action_method(sender: indexPath.row)
        
        
        
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
